const express = require('express'); // for api framework
const cors = require('cors'); // for api framework
const app = express();
var session = require('express-session');
var multer = require('multer')
const cwd = process.cwd();
const env = process.env;

const fs = require('fs');
const util = require('util');
const morgan = require('morgan'); // for log status
const bodyParser = require('body-parser'); // for accept the all form data i.e get, post, on, put

app.use(session({
    resave: false,
    saveUninitialized: true,
    secret: 'waioz'
}));
const mongoose = require('mongoose'); // for access the models
mongoose.set('useCreateIndex', true); // mongoose default config settings
mongoose.set('useUnifiedTopology', true);
mongoose.connect(env.MONGO_DB_URL, { useNewUrlParser: true }); // mention database name


const adminRoutes = require('./api/routes/admin');
const userRoutes = require('./api/routes/user');

const fileUpload = require('express-fileupload');
app.use(fileUpload());
//const commonHelper = require('./api/helpers/commonHelper');
app.use(express.static(cwd + '/public'));
app.use(morgan('dev'));


app.use(bodyParser.urlencoded({ extended: true, limit: '10mb' }));
app.use(bodyParser.json({ limit: '10mb' }));
app.use((req, res, next) => {
    if (req.method === 'OPTIONS') {
        res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, PATCH, DELETE');
        return res.status(200).json({});
    }
    next();
});

const waitFor = (ms) => new Promise(r => setTimeout(r, ms));
async function asyncForEach(array, callback) {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array);
    }
}

app.use((req, res, next) => {
    res.apiResponse = (status, message, data = null) => {
        res.send({
            status,
            message,
            data
        })
    }
    next()
})

app.use(async(req, res, next) => {
    const url = req.url;
    const uriArray = url.split('/');
    var commonData = {
        restUrl: env.APP_URL + '/api/',
        baseUrl: env.APP_URL + '/'
    }
    const processText = (text) => {
        return text.replace('{{commonData}}', JSON.stringify(commonData))
    }
    if (uriArray[1] == "images" || uriArray[1] == "uploads" || uriArray[1] == "assets") {
        const pathImage = decodeURI(uriArray[uriArray.length - 1]);
        const extension = pathImage.split('.').pop();
        const contentType = 'image/' + extension;
        const file = "." + decodeURI(url);
        fileToLoad = fs.readFileSync(file);
        res.writeHead(200, { 'Content-Type': contentType });
        res.end(fileToLoad, 'binary');
    } else if (uriArray[1] !== 'api') {
        const readFile = util.promisify(fs.readFile)
        try {
            var text = await readFile(cwd + '/public/angular_app/carlease/index.html', 'utf8')
            text = processText(text)
            return res.send(text)
        } catch (error) {
            return res.send(error.message)
        }
    } else {
        /* Api Response Middleware */
        if (uriArray[2] != 'auth') {
            res.apiResponse = (status, message, data = null) => {
                res.send({
                    status,
                    message,
                    data
                })
            }
            if (!req.body.params) {
                return res.apiResponse(false, "Params is required")
            }
            var params = req.body.params
            if ((typeof params).toLowerCase() !== 'object') {
                try {
                    params = JSON.parse(params)
                } catch (e) {
                    return res.apiResponse(false, "Params is not a valid JSON")
                }
                if ((typeof params).toLowerCase() !== 'object') {
                    return res.apiResponse(false, "Params is not a valid JSON")
                }
            }
            req.bodyParams = params
            next()
        }
    }
});


app.use('/api/admin', adminRoutes);
app.use('/api/users', userRoutes);

app.use((error, req, res, next) => {
    res.status(error.status || 200).json({
        error: {
            message: error.message
        }
    });
});

module.exports = app;