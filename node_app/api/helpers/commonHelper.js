const env = process.env;
exports.siteName = ()=>{
    return env.APP_NAME;
}
exports.getBaseurl = () =>{
    return env.APP_URL;
}
exports.siteUrl = () =>{
    return env.HTTP_HOST+":"+env.HTTP_PORT
}
exports.siteBaseUrl = () =>{
    return "http://localhost:8077";
}