const adminUser = require('../models/admin_user');
const Car = require('../models/car');
const User = require('../models/user');
const CarsPhoto = require('../models/car_image');
const CarImageCategory = require('../models/car_image_category');
const CarMake = require('../models/car_make');
const Reservation = require('../models/reservation');
const Notification = require('../models/notification');
const commonHelper = require('../helpers/commonHelper');
const moment = require("moment-timezone");
const mongoose = require('mongoose');
const _ = require("lodash")
var fs = require('fs');
exports.get_admin_users = (req, res, next) => {
    adminUser.find().exec().then(result => {
        res.status(200).json(result);
    }).catch(err => {
        res.status(200).json(err);
    });
}
exports.get_admin_notifications = async(req, res, next) => {
    var requests = req.bodyParams;
    var page = requests.page || 1;
    var query = {};
    if (requests.start_date != '' && requests.end_date != '') {
        query['updatedAt'] = { $gte: moment(requests.start_date).format('YYYY-MM-DD'), $lte: moment(requests.end_date).format('YYYY-MM-DD') + ' 23:59:59' };
    }
    const options = {
        sort: { updatedAt: -1 },
        page: page,
        populate: 'user_by_details reservation_details car_details'
    };
    Notification.paginate(query, options, function(err, notification) {
        return res.apiResponse(true, "Success", { notification })
    });
}
exports.first_admin = (req, res, next) => {
    const admin_details = {
        _id: new mongoose.Types.ObjectId(),
        username: 'car_lease',
        email: 'admin@carlease.com',
        password: 'carlease'
    }
    const admin = new adminUser(admin_details);
    admin.save().then(result => {
        res.status(200).json({
            message: "added",
            admin_details: result
        });
    }).catch(err => {
        res.status(200).json(err);
    });
}
exports.destroyAll = async(req, res, next) => {
    adminUser.remove({}, function(err) {
        if (err) {
            console.log(err)
        } else {
            res.end('success');
        }
    });
}
exports.checkLogin = async(req, res, next) => {
    var requests = req.bodyParams;
    var data = new Object;
    var response = new Object;
    var admin_detail = await adminUser.findOne({ email: requests.email });
    if (admin_detail === null) {
        return res.status(200).send({ success: false, message: "Invalid Credentials.", data: data });
    } else {
        var admin_array = new Object;
        admin_array.users = admin_detail;
        if (admin_detail.comparePassword(requests.password))
            return res.status(200).send({ success: true, message: "Logged In", data: admin_array });
        else
            return res.status(200).send({ success: false, message: "Invalid Password", data: data });
    }
}

exports.get_cars = async(req, res, next) => {
    var requests = req.bodyParams;
    var page = requests.page || 1
    const options = {
        page: page,
        populate: 'make_title'
    };
    Car.paginate({ 'is_enable': true }, options, function(err, car) {
        return res.apiResponse(true, "Success", { car })
    });
}
exports.get_latest_cars = async(req, res, next) => {
    var requests = req.bodyParams;
    var page = requests.page || 1
    const options = {
        populate: 'make_title',
        sort: { createdAT: -1 }
    };
    Car.find({ 'is_enable': true, 'status': true }, function(err, car) {
        return res.apiResponse(true, "Success", { car })
    }).populate('make_title photo_id').sort({ createdAT: -1 });
}

// image category

exports.get_car_image_category = async(req, res, next) => {
    var requests = req.bodyParams;
    var page = requests.page || 1
    const options = {
        page: page,
        limit: 10
    };
    CarImageCategory.paginate({}, options, function(err, car) {
        return res.apiResponse(true, "Success", { car })
    });
}

exports.get_image_category_detail = async(req, res, next) => {
    var requests = req.bodyParams;
    var admin_image_category = [];
    var admin_image_category = await CarImageCategory.findOne({ _id: requests.id });
    if (admin_image_category) {
        return res.apiResponse(true, "Image Category retrieved successfully", { admin_image_category })
    } else {
        return res.apiResponse(false, "Record Missing", { data })
    }
}
exports.delete_image_category = async(req, res, next) => {
    var requests = req.bodyParams;
    var admin_image_category = await CarImageCategory.findById(requests.id, function(err, imagecategoryDetails) {
        imagecategoryDetails.remove();
    });
    return res.apiResponse(true, "Success", { admin_image_category })
}
exports.update_image_category = async(req, res, next) => {
    var requests = req.bodyParams;
    if (req.bodyParams.type) {
        var admin_image_category = await CarImageCategory.findById(requests.id, function(err, imagecategoryDetails) {
            imagecategoryDetails.name = requests.name;
            imagecategoryDetails.status = requests.status;
            imagecategoryDetails.save();
        });
        return res.apiResponse(true, "Category Updated Successfully", { admin_image_category })
    } else {
        var admin_image_category = new CarImageCategory(requests);
        admin_image_category.save();
        return res.apiResponse(true, "Category Added Successfully", { admin_image_category })
    }
}

// make

exports.get_car_make = async(req, res, next) => {
    var requests = req.bodyParams;
    var page = requests.page || 1
    const options = {
        page: page,
        limit: 10
    };
    CarMake.paginate({ 'is_enable': 'true' }, options, function(err, car) {
        return res.apiResponse(true, "Success", { car })
    });
}

exports.get_active_make = async(req, res, next) => {
    var requests = req.bodyParams;
    CarMake.find({ 'status': 'active', 'is_enable': 'true' }).exec().then(active_make => {
        var new_obj = active_make.filter(function(item) {
            return item.name != null && item.name != '';
        });
        make_group = _.map(new_obj, 'name');
        return res.apiResponse(true, "Success", { active_make, make_group })

    }).catch(err => {
        return res.apiResponse(false, "No makes found", {})
    });
}

exports.delete_car_make = async(req, res, next) => {
    var requests = req.bodyParams;
    CarMake.findOneAndUpdate({ "_id": requests.id }, { "$set": { 'is_enable': false } }, { new: true }).exec(function(err, admin_image_category) {
        return res.apiResponse(true, "Deleted Successfully", { admin_image_category })
    });
    // var admin_image_category = await CarMake.findById(requests.id, function (err, imagecategoryDetails) {
    //     imagecategoryDetails.remove();
    // });
    // return res.apiResponse(true, "Success", { admin_image_category })
}

exports.get_make_detail = async(req, res, next) => {
    var requests = req.bodyParams;
    var admin_image_category = [];
    var admin_image_category = await CarMake.findOne({ _id: requests.id });
    if (admin_image_category) {
        return res.apiResponse(true, "Image Category retrieved successfully", { admin_image_category })
    } else {
        return res.apiResponse(false, "Record Missing", { data })
    }
}


exports.update_make = (req, res, next) => {
    if (req.bodyParams.type) {
        if (req.files == null) {
            var requests = req.body;
            var data = new Object;
            CarMake.findById(requests.id, function(err, makeDetails) {
                makeDetails.name = requests.name;
                makeDetails.status = requests.status;
                makeDetails.is_enable = requests.is_enable;
                makeDetails.save();
            });
        } else {
            const media = req.files.media;
            const fileName = Date.now() + media.name;
            media.mv('uploads/make/' + fileName, function(err) {
                var requests = req.body;
                requests.photo = fileName;
                var data = new Object;
                CarMake.findById(requests.id, function(err, makeDetails) {
                    makeDetails.name = requests.name;
                    makeDetails.status = requests.status;
                    makeDetails.photo = requests.photo;
                    makeDetails.is_enable = requests.is_enable;
                    makeDetails.save();
                });
            });
        }
        return res.apiResponse(true, "Successfully make updated", { data })
    } else {
        if (req.files == null) {
            var requests = req.body;
            requests.is_enable = true;
            var admin_category = new CarMake(requests);
            admin_category.save();
            var data = new Object;
        } else {
            const media = req.files.media;
            const fileName = Date.now() + media.name;
            media.mv('uploads/make/' + fileName, function(err) {
                var requests = req.body;
                requests.photo = fileName;
                var admin_category = new CarMake(requests);
                admin_category.save();
                var data = new Object;
            });
        }
        return res.apiResponse(true, "Successfully make added", { data })
    }
}

exports.get_car_detail = async(req, res, next) => {
    var requests = req.bodyParams;
    var admin_car = [];
    var admin_car = await Car.findOne({ _id: requests.id }).populate('make_title');
    var photo_details = await CarsPhoto.find({ car_id: requests.id });
    if (admin_car) {
        return res.apiResponse(true, "Car retrieved successfully", { admin_car, photo_details })
    } else {
        return res.apiResponse(false, "Record Missing", { data })
    }
}

exports.add_car = async(req, res, next) => {
    var requests = req.bodyParams;
    if (requests.type == 'add') {
        var admin_car = new Car(requests);
        admin_car.save();
        return res.apiResponse(true, "Successfully car added", { admin_car })
    } else {
        let getcarPhotos = await CarsPhoto.find({ 'car_id': requests.carid });
        if (getcarPhotos.length == 0) {
            requests.status = false;
        } else {
            requests.status = true;
        }
        Car.findOneAndUpdate({ "_id": requests.carid }, { "$set": requests }, { new: true }).exec(function(err, admin_car) {
            return res.apiResponse(true, "Updated Successfully", { admin_car })
        });
    }
}

exports.upload_car_photo = async(req, res, next) => {
    const car_id = req.body.car_id;
    let file;

    if (req.files.media != null) {
        var obj = {};
        var link = [];
        var result = new Object;
        obj.success = true;
        obj.message = "File uploaded succesfully";

        if (req.files.media.length == undefined)
            imageLength = 1;
        else
            imageLength = req.files.media.length;

        for (var fileIndex = 0; fileIndex < imageLength; fileIndex++) {

            if (imageLength == 1) {
                file = req.files.media;
            } else {
                file = req.files.media[fileIndex];
            }

            const fileName = Date.now() + car_id
            var getCarPhotos = await CarsPhoto.find({ car_id: car_id });

            await (
                new Promise(function(resolve) {
                    file.mv('uploads/cars/' + fileName, function(err) {
                        link.push(commonHelper.getBaseurl() + '/uploads/cars/' + fileName);
                        resolve(link)
                    })

                    result.name = fileName;
                    result.car_id = car_id;
                    result.image_category = 0;

                    var listphoto = new CarsPhoto(result);

                    listphoto.save();
                })
            );
        }
        await Car.findOneAndUpdate({ "_id": car_id }, { "$set": { "status": true } }, { new: true }).exec(function(err, admin_car) {});
        var getCarPhotos = await CarsPhoto.find({ car_id: car_id });
        obj.car_photos = getCarPhotos;
        obj.link = Object.assign({}, link);
        return res.apiResponse(true, "Updated Successfully", obj)
    } else {

    }
}
exports.delete_photo = async(req, res, next) => {
    var requests = req.bodyParams;
    var data = new Object;
    var response = new Object;
    CarsPhoto.findOne({ _id: requests.id }).deleteOne({}, async function(err) {
        if (err) {
            data.err = err;
            return res.apiResponse(false, "Something Wrong", data)
        } else {
            let getcarPhotos = await CarsPhoto.find({ 'car_id': requests.car_id });
            if (getcarPhotos.length == 0) {
                await Car.findOneAndUpdate({ "_id": requests.car_id }, { "$set": { "status": false } }, { new: true }).exec(function(err, admin_car) {});
            } else {
                await Car.findOneAndUpdate({ "_id": requests.car_id }, { "$set": { "status": true } }, { new: true }).exec(function(err, admin_car) {});
            }
            fs.stat('./uploads/cars/' + requests.name, function(err, stats) {
                if (err) {}

                fs.unlink('./uploads/cars/' + requests.name, function(err) {});
            });
            return res.apiResponse(true, "Photo deleted successfully", data)
        }
    });
}

exports.get_car_photo = async(req, res, next) => {
    var requests = req.bodyParams;
    var photoDetails = await CarsPhoto.find({ car_id: requests.car_id });
    var imagecatgoryDetails = await CarImageCategory.find({ status: 'active' });
    return res.apiResponse(true, "Photo retrieved successfully", { photoDetails, imagecatgoryDetails })
}

exports.update_catgory = async(req, res, next) => {
    var requests = req.bodyParams;
    CarsPhoto.findOneAndUpdate({ "_id": requests.id }, { "$set": requests }, { new: true }).exec(function(err, car_photos) {
        return res.apiResponse(true, "Category updated successfully", { car_photos })
    });
}

exports.delete_car = async(req, res, next) => {
    var requests = req.bodyParams;
    var admin_car = await Car.findById(requests.id, async function(err, carDetails) {
        carDetails.remove();
        var car_photo = await CarsPhoto.find({ car_id: requests.id });
        if (car_photo.length > 0) {
            for (let index = 0; index < car_photo.length; index++) {
                const element = car_photo[index].name;
                fs.stat('./uploads/cars/' + element, function(err, stats) {
                    if (err) {}

                    fs.unlink('./uploads/cars/' + element, function(err) {});
                });
            }
            CarsPhoto.find({ car_id: requests.id }).deleteMany({}, async function(err) {})
        }
    });
    return res.apiResponse(true, "Car Deleted Successfully", { admin_car })
}
exports.get_dashboard_result = async(req, res, next) => {
    var requests = req.bodyParams;
    const user_count = await User.find({ 'status': { $ne: false } }).count();
    const active_user_count = await User.find({ 'is_active': true, 'status': { $ne: false } }).count();
    const in_active_user_count = await User.find({ 'is_active': { $ne: true }, 'status': { $ne: false } }).count();
    const car_count = await Car.find({ 'is_enable': { $ne: false } }).count();
    const total_reservation = await Reservation.find({});
    var total_amount = _.sumBy(total_reservation, function(o) { return o.total_amount; })
    const partial_reservation_count = await Reservation.find({}).distinct('token').count();

    var yearly_car = []
    for (var i = 0; i < 12; ++i) {
        var start = moment().startOf('month').month(i);
        var end = moment().endOf('month').month(i);
        var car_month_count = await Car.find({ 'is_enable': { $ne: false }, createdAt: { $gte: start, $lt: end } }).count();
        yearly_car.push(car_month_count);
    }
    return res.apiResponse(true, "Data retrived Successfully", { user_count, active_user_count, in_active_user_count, car_count, total_amount, partial_reservation_count, yearly_car })
}