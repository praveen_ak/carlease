const adminUser = require('../models/admin_user');
const User = require('../models/user');
const Car = require('../models/car');
const Reservation = require('../models/reservation');
const Notification = require('../models/notification');
const commonHelper = require('../helpers/commonHelper');
const moment_timezone = require("moment-timezone");
const moment = require('moment');
const mongoose = require('mongoose');
const _ = require("lodash")
var fs = require('fs');
var stripe = require('stripe')('sk_test_LuA7GlKcKljsYvwp4FbRRx4S');
var CronJob = require('cron').CronJob;
var nodemailer = require('nodemailer');

new CronJob('0 0 0 * * *', async function() {
    const current_day = moment().tz('Asia/Kolkata').format("L");
    const current_day_timestamp = moment(current_day).format("X");    
    const reservation = await Reservation.find({'nextdate': current_day_timestamp}).populate('user_details');
    if(reservation.length > 0) {
        var requests = {};
        for (let index = 0; index < reservation.length; index++) {
            if(reservation[index].no_of_times_paid <= reservation[index].terms) {
                const month_price = parseFloat(reservation[index].month_price);
                const next_payment_day = moment(reservation[index].nextdate, 'X').format('L');            
                const next_payment_timestamp = moment(next_payment_day).add(1, 'M').format("X");
                
                var charge=stripe.charges.create({  // stripe payment start
                    amount:reservation[index].month_price * 100,
                    currency:reservation[index].currency,
                    source:reservation[index].token
                    }, async (err,charge)=>{
                    if(err){
                        var get_admin = await adminUser.find();
                        var newNotification = new Notification({ user_to: get_admin[0].id, user_by: reservation[index].user_id, is_read:0, reservation_id: reservation[index].id, car_id: reservation[index].car_details.id, message: err.raw.message + " User's Email:" +reservation[index].user_details.email });
                        await newNotification.save();
                    }
                    else {
                        if(charge.status == 'succeeded') {
                            requests.token = reservation[index].token;
                            requests.amount = reservation[index].amount;
                            requests.total_amount = reservation[index].total_amount;
                            requests.month_price = reservation[index].month_price;
                            requests.currency = reservation[index].currency;
                            requests.startdate = reservation[index].startdate;
                            requests.enddate = reservation[index].enddate;
                            requests.terms = reservation[index].terms;
                            requests.car_details = reservation[index].car_details;
                            requests.user_id = reservation[index].user_id;
                            if(reservation[index].booking_additional_price) {
                                requests.booking_additional_price = reservation[index].booking_additional_price;
                            }
                            requests.charge_id = charge.id;
                            requests.status = 'accepted';
                            requests.nextdate = next_payment_timestamp;
                            requests.no_of_times_paid = reservation[index].no_of_times_paid + 1;
                            var booking = new Reservation(requests);
                            var confirm_booking = await booking.save();
                            var get_admin = await adminUser.find();
                            var newNotification = new Notification({ user_to: get_admin[0].id, user_by: reservation[index].user_id, is_read:0, reservation_id: confirm_booking.id, car_id: reservation[index].car_details.id, message: "Hi, I am interested in leasing your car." });
                            await newNotification.save();
                            var user_details = User.findOne({'_id':reservation[index].user_id});

                            var mail_content="";
                            mail_content+='<div style="max-width:600px;margin:0 auto;background: #fff;border-radius: 5px;">';
                            mail_content+='<h2 style="display:block;text-transform: uppercase;font-weight: bold;padding: 1em 0;background: #ebf0f3;text-align: center;justify-content: center;margin-bottom: 0;">Payment Receipt</h2>';
                            mail_content+='<table style="width: 100%;border: 1px solid #ebebeb;border-top: 0;">';
                            mail_content+='<thead>';
                            mail_content+='<tr><td style="text-transform: uppercase; padding: 1em; border-right: 1px solid #ebebeb; font-weight: bold; border-bottom: 1px solid #ebebeb;;">Amount Paid</td><td style="text-transform: uppercase; padding: 1em; border-right: 1px solid #ebebeb; font-weight: bold; border-bottom: 1px solid #ebebeb;;">Date Paid</td><td style="text-transform: uppercase; padding: 1em; font-weight: bold; border-bottom: 1px solid #ebebeb;;">Next month Price date</td></tr>';
                            mail_content+='</thead>';
                            mail_content+='<tbody>';
                            if((reservation[index].terms - 1) == reservation[index].no_of_times_paid) {
                                mail_content+='<tr><td style="text-transform: uppercase; border-right: 1px solid #ebebeb; padding: 1em;">'+reservation[index].month_price+'</td><td style="text-transform: uppercase; border-right: 1px solid #ebebeb; padding: 1em;">'+moment(confirm_booking.createdAt).format('LL')+'</td><td style="text-transform: uppercase; padding: 1em;">Nil</td></tr>';
                            }
                            else {
                                mail_content+='<tr><td style="text-transform: uppercase; border-right: 1px solid #ebebeb; padding: 1em;">'+reservation[index].month_price+'</td><td style="text-transform: uppercase; border-right: 1px solid #ebebeb; padding: 1em;">'+moment(confirm_booking.createdAt).format('LL')+'</td><td style="text-transform: uppercase; padding: 1em;">'+moment.unix(next_payment_timestamp).format('LL')+'</td></tr>';
                            }
                            mail_content+='</tbody>';
                            mail_content+='</table>';
                            mail_content+='</div>';
                            // create reusable transporter object using the default SMTP transport
                            var transporter = nodemailer.createTransport({
                                service: 'gmail',
                                auth: {
                                user: 'waioztechnology@gmail.com',
                                pass: 'fcjobwjjwnfqaeod'
                                }
                            });

                            // setup email data with unicode symbols
                            let mailOptions = {
                                from: 'waioztechnology@gmail.com', 
                                to: user_details.email, // list of receivers
                                subject: 'Car Lease Payment Receipt', // Subject line
                                html: mail_content // html body
                            };

                            // send mail with defined transport object
                            transporter.sendMail(mailOptions, (error, info) => {
                                if (error) {
                                    return false;
                                }
                                else
                                {
                                    return true;
                                }
                            });

                        }
                        else {
                            var get_admin = await adminUser.find();
                            var newNotification = new Notification({ user_to: get_admin[0].id, user_by: reservation[index].user_id, is_read:0, reservation_id: reservation[index].id, car_id: reservation[index].car_details.id, message: "User Payments has been failed. Contact to User's Email:" +reservation[index].user_details.email });
                            await newNotification.save();
                        }            
                    }
                });
            }
        }
    }
}, null, true, 'Asia/Kolkata');

exports.register = async(req, res, next) => {
    var requests=req.body;
    var user=await User.findOne({email:requests.email});
    if(user) {   
        return res.apiResponse(false, "Email already exists", { user })
    }
    else {
        const lmedia = req.files.licence_media;
        const fileName_licence =  Date.now()+'_licence_'+lmedia.name;
        const nmedia = req.files.national_media;
        const fileName_national =  Date.now()+'_national_'+nmedia.name;
        lmedia.mv('uploads/'+fileName_licence, function(err) {
            nmedia.mv('uploads/'+fileName_national, function(err) {
                requests.status = true;
                requests.is_active = false;
                requests.timezone=moment_timezone.tz.guess();
                requests.licence_photo = fileName_licence;
                requests.national_photo = fileName_national;
                var user = new User(requests);
                user.save();
                return res.apiResponse(true, "Registered successfully", { user })
            });
        });        
    }
}
exports.login = async(req, res, next) => {
    var requests = req.bodyParams;
    var user = await User.findOne({ email : requests.email }); 
    if (user === null) {
        return res.apiResponse(false, "Email does not exists", { user })
    }
    else 
    { 
        if (user.comparePassword(requests.password))  {
            if(user.status == true) {
                return res.apiResponse(true, "Logged In Successfully", { user })
            }
            else {
                return res.apiResponse(false, "Your account has been deleted", { user })
            }
        }
        else {
            return res.apiResponse(false, "Invalid Password", { user })
        }
    }
}
exports.get_users = async(req, res, next) => {
    var requests = req.bodyParams;
    var page = requests.page || 1
    const options = {
        page: page
    };
    User.paginate({'status': true}, options, function (err, user) {
        return res.apiResponse(true, "Success", { user })
    });
}
exports.get_reservations = async(req, res, next) => {
    var requests = req.bodyParams;
    var page = requests.page || 1;
    var query = {};
    const options = {
        page: page,
        populate: 'user_details'
    };
    var all_reservation = await Reservation.find().populate('user_details');
    var total_users = _.uniqBy(all_reservation, 'user_id');
    
    if(requests.user_id != '') {
        query['user_id'] = requests.user_id
    }
    Reservation.paginate(query, options, function (err, reservation) {
        return res.apiResponse(true, "Success", { reservation, total_users })
    });
}

exports.get_user_reservation = async(req, res, next) => {
    var requests = req.bodyParams;
    var page = requests.page || 1
    const options = {
        page: page
    };
    Reservation.paginate({'user_id': requests.user_id}, options, function (err, reservation) {
        return res.apiResponse(true, "Success", { reservation })
    });
}

exports.update_user = async(req, res, next) => {
    var requests = req.body;
    if(requests.email) {
        var user=await User.findOne({email:requests.email, _id: {$ne: requests.id}});
    }    
    if(user) {   
        return res.apiResponse(false, "Email already exists", { user })
    }
    else {
        let fileName_licence = ''
        if(req.files && req.files.licence_media) {
            const lmedia = req.files.licence_media;
            fileName_licence =  Date.now()+'_licence_'+lmedia.name;
            lmedia.mv('uploads/'+fileName_licence, function(err) {
            })
        }
        let fileName_national = '';
        if(req.files && req.files.national_media) {
            const nmedia = req.files.national_media;
            fileName_national =  Date.now()+'_national_'+nmedia.name;
            nmedia.mv('uploads/'+fileName_national, function(err) {
            })
        }
        if(fileName_licence != '') {
            requests.licence_photo = fileName_licence;
        }
        if(fileName_national != '') {
            requests.national_photo = fileName_national;
        }
        await User.findOneAndUpdate({ "_id": requests.id }, { "$set": requests }, { new: true }).exec(function (err, user) {
            console.log(err);
            return res.apiResponse(true, "Updated Successfully", {user})
        });
    }
}

exports.price_calculate = async(req, res, next) => {
    var requests = req.bodyParams;
    var is_user_active = await User.findOne({'_id':requests.user_id,'is_active': true});
    if(!is_user_active) {
        return res.apiResponse(false,"Contact admin to enable your account", {})
    }
    var no_lease_query = await Reservation.find({'startdate':{$lte:requests.startdate},'enddate':{$gte:requests.startdate},'car_details.id': requests.id});
    if(no_lease_query.length > 0) {
        return res.apiResponse(false,"Car already booked", {})
    }
    if(requests.term == 1) {
        var term = requests.term + ' month'
    }
    else {
        var term = requests.term + ' months'
    }
    await Car.findOne({'_id': requests.id,price: {$elemMatch: { 'duration' :term}}}, function(err, price_details) {
        if(err) {
            return res.apiResponse(false,"Something wrong", {price_details})
        }
        else {
            return res.apiResponse(true,"Calculated Successfully", {price_details})
        }
    }).select('price')
}
exports.get_search_results = async(req, res, next) => {
    var requests = req.bodyParams;
    var page = requests.page || 1;
    var query = {};
    query['is_enable'] = true;
    query['status'] = true;
    if(requests.make) {
        query['make_title'] = requests.make;
    }
    if(requests.model) {
        query['model'] = requests.model;
    }
    if(requests.transmission) {
        query['transmission_type'] = requests.transmission;
    }
    if(requests.term && requests.term == '1') {
        var term = requests.term + ' month';
        query['price'] = { $elemMatch: { 'duration' :term, price: {$gte: requests.min_price,$lte: requests.max_price}}}
    }
    else if (requests.term && requests.term != '1' && requests.term != 'all') {
        var term = requests.term + ' months';
        query['price'] = { $elemMatch: { 'duration' :term, price: {$gte: requests.min_price,$lte: requests.max_price}}}
    }
    const options = {
    	populate: ['photo_id','make_title'],
        page: page,
        limit: 12
    };
    console.log(query);
    Car.paginate(query,options, function(err, car_details) {
        return res.apiResponse(true,"Car Retrived Successfully", {car_details});
    }); 
}
exports.get_max_price = async(req, res, next) => {
    var response = new Object;
    var max_price=await Car.find({'is_enable':true,'status':true}).sort({'price.price':-1}).limit(1).select('price');
    max_price = _.orderBy(max_price[0].price, ({ price }) => price || '', ['desc']);
    return res.apiResponse(true,"Price", {max_price});
}
exports.get_model_by_make = async(req, res, next) => {
    var requests = req.bodyParams;
    if(requests.make) {
        var get_models = await Car.find({'is_enable': true, 'status': true, 'make_title':requests.make});
        get_models = _.map(get_models, 'model');
        return res.apiResponse(true,"Model", {get_models});
    }
    else {
        return res.apiResponse(false,"Model", {});
    }
}

// payment

exports.payment_success = async(req, res, next) => {
    var requests = req.bodyParams;
    requests.month_price = parseFloat(requests.month_price);
    // console.log(requests.month_price); return
    // stripe.products.create({
    //     name: requests.car_details.model,
    //     type: 'service',
    //     },
    //     function(err, product) {
    //         stripe.plans.create(
    //             {
    //                 amount: requests.month_price,
    //                 currency: 'usd',
    //                 interval: 'month',
    //                 product: {name: product.name},
    //             },
    //             function(err, plan) {
    //                 var plan_id = plan.id;
    //                 stripe.customers.create({
    //                     source: requests.token,
    //                     email: requests.user_email
    //                   }).then(customer => {
    //                     stripe.subscriptions.create({
    //                       customer: customer.id,
    //                     //   current_period_start: requests.startdate,
    //                     //   current_period_end: requests.enddate,
    //                       items: [
    //                         {
    //                           plan: plan_id
    //                         }
    //                       ]
    //                     });
    //                 });
    //             }
    //         );
    //     }
    // );
    // return;
    var current_day = moment().tz('Asia/Kolkata').format("L");
    var current_day_timestamp = moment(current_day).format("X"); 
    var next_payment_day = moment(requests.startdate, 'X').format('L');
    var next_payment_timestamp = moment(next_payment_day).add(1, 'M').format("X");
    
    var charge=stripe.charges.create({  // stripe payment start
        amount:requests.month_price * 100,
        currency:requests.currency,
        source:requests.token
        }, async (err,charge)=>{
        if(err){
            return res.apiResponse(false,err.raw.message, {});
        }
        else {
            if(charge.status == 'succeeded') {
                requests.charge_id = charge.id;
                requests.status = 'accepted';
                requests.nextdate = next_payment_timestamp;
                requests.no_of_times_paid = 1;
                var booking = new Reservation(requests);
                var confirm_booking = await booking.save();
                var get_admin = await adminUser.find();
                var newNotification = new Notification({ user_to: get_admin[0].id, user_by: requests.user_id, is_read:0, router_path: 'reservation/'+confirm_booking.id, reservation_id: confirm_booking.id, car_id: requests.car_details.id, message: "Hi, I am interested in leasing your car." });
                await newNotification.save();
                return res.apiResponse(true,"Payment done", {});
            }
            else {
                return res.apiResponse(false,"Payment failed", {});
            }            
        }
    });    
}