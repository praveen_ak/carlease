// grab the things we need
const mongoose = require('mongoose');
const moment = require("moment");
const crypto = require("crypto");
var uniqueValidator = require('mongoose-unique-validator');
var mongoosePaginate = require('mongoose-paginate-v2');
const commonHelper = require('../helpers/commonHelper');

//create schemaOptions
var schemaOptions = {
    toObject: {
        virtuals: true
    },
    toJSON: {
        virtuals: true
    },
    timestamps: true
};


/**
 * User schema
 */
const userSchema = mongoose.Schema({
    fname: {
        type: String,
        trim: true
    },
    lname: {
        type: String,
        trim: true
    },
    email: {
        type: String,
    },
    password: {
        type: String
    },
    phone_number: String,
    licence_photo: String,
    national_photo: String,
    profile_picture: String,
    salt: {
        type: String,
    },
    timezone: String,
    is_active: {
        type: Boolean,
        default: false
    },
    status: {
        type: Boolean,
        default: true
    },
    /* For reset password */
    resetPasswordToken: String,
    resetPasswordExpires: Date
}, schemaOptions);
// on every save, add the date
userSchema.pre('save', function(next) {
    // get the current date
    if (this.password) {
        this.salt = Buffer.from(crypto.randomBytes(16).toString('base64'), 'base64');
        this.password = this.hashPassword(this.password);
    }
    next();
});

/**
 * Create instance method for hashing a password
 */
userSchema.methods.hashPassword = function(password) {
    if (this.salt && password) {
        return crypto.pbkdf2Sync(password, this.salt, 10000, 64, 'sha512').toString('base64');
    }
    return password;
};

/**
 * Create instance method for authenticating user
 */
userSchema.methods.comparePassword = function(password) {
    return this.password === this.hashPassword(password);
};
userSchema.virtual('fullName').get(function() {
    let fullName = this.fname ? this.fname + ' ' : '';
    fullName += this.lname ? this.lname : '';
    return fullName;
});
userSchema.virtual('licencephoto').get(function() {
    let photo = this.licence_photo;
    if (photo != "") {
        return commonHelper.getBaseurl() + "/uploads/" + this.licence_photo;
    } else {
        return commonHelper.getBaseurl() + "/uploads/no_profile_img.png";
    }
});
userSchema.virtual('nationalphoto').get(function() {
    let photo = this.national_photo;
    if (photo != "") {
        return commonHelper.getBaseurl() + "/uploads/" + this.national_photo;
    } else {
        return commonHelper.getBaseurl() + "/uploads/no_profile_img.png";
    }
});
userSchema.virtual('original_profile').get(function() {
    if (this.profile_picture) {
        return commonHelper.getBaseurl() + "/uploads/" + this.profile_picture;
    } else {
        return commonHelper.getBaseurl() + "/uploads/no_profile_img.png";
    }
});

userSchema.plugin(mongoosePaginate);
module.exports = mongoose.model('User', userSchema);