// grab the things we need
const mongoose = require('mongoose');
const moment = require("moment");
const crypto = require("crypto");
var mongoosePaginate = require('mongoose-paginate-v2');
const commonHelper = require('../helpers/commonHelper');

//create schemaOptions
var schemaOptions = {
    toObject: {
      virtuals: true,
      transform: function (doc, ret) {
        delete ret.__v;
      }
    }
    ,toJSON: {
      virtuals: true,
      transform: function (doc, ret) {
        delete ret.__v;
      }
    },
    timestamps: true
  };


/**
 * Item schema
 */
const carimageSchema = mongoose.Schema({
    image_category: String,
    car_id: String,
    name: String,
}, schemaOptions);

carimageSchema.virtual('car_photo').get(function () {
    return commonHelper.getBaseurl()+"/uploads/cars/"+this.name;
});
  
carimageSchema.plugin(mongoosePaginate);
module.exports = mongoose.model('CarImage',carimageSchema);