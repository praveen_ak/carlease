// grab the things we need
const mongoose = require('mongoose');
const moment = require("moment");
const crypto = require("crypto");
var mongoosePaginate = require('mongoose-paginate-v2');
const commonHelper = require('../helpers/commonHelper');

//create schemaOptions
var schemaOptions = {
    toObject: {
      virtuals: true,
      transform: function (doc, ret) {
        delete ret.__v;
      }
    }
    ,toJSON: {
      virtuals: true,
      transform: function (doc, ret) {
        delete ret.__v;
      }
    },
    timestamps: true
  };


/**
 * Item schema
 */
const carimagecategorySchema = mongoose.Schema({
    name: String,
    status: String
}, schemaOptions);

carimagecategorySchema.plugin(mongoosePaginate);
module.exports = mongoose.model('CarImageCategory',carimagecategorySchema);