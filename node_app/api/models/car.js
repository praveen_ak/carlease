// grab the things we need
const mongoose = require('mongoose');
const moment = require("moment");
const crypto = require("crypto");
var mongoosePaginate = require('mongoose-paginate-v2');
const commonHelper = require('../helpers/commonHelper');

//create schemaOptions
var schemaOptions = {
    toObject: {
      virtuals: true,
      transform: function (doc, ret) {
        delete ret.__v;
      }
    }
    ,toJSON: {
      virtuals: true,
      transform: function (doc, ret) {
        delete ret.__v;
      }
    },
    timestamps: true
  };


/**
 * Item schema
 */
const carSchema = mongoose.Schema({
    make_title: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'CarMake'
    },
    model: String,
    transmission_type: String,
    fuel_type: String,
    body_type: String,
    seat: Number,
    engine_capacity: Number,
    bhp: Number,
    mileage: Number,
    fuel_capacity: Number,
    price: [{
        duration: String, 
        price: Number
    }],
    extra_price: [{
        color_code: String, 
        price: Number
    }],
    status: Boolean,
    is_enable: Boolean
}, schemaOptions);

carSchema.virtual('photo_id', {
  ref : 'CarImage',      // fetch from ListingsPhoto model
  localField : '_id',
  foreignField : 'car_id',
  justOne : true
})
carSchema.virtual('photo_all', {
  ref : 'CarImage',      // fetch from ListingsPhoto model
  localField : '_id',
  foreignField : 'list_id'
})
carSchema.plugin(mongoosePaginate);
module.exports = mongoose.model('Car',carSchema);