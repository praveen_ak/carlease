// grab the things we need
const mongoose = require('mongoose');
const moment = require("moment");
const crypto = require("crypto");
var mongoosePaginate = require('mongoose-paginate-v2');
const commonHelper = require('../helpers/commonHelper');

//create schemaOptions
var schemaOptions = {
    toObject: {
      virtuals: true,
      transform: function (doc, ret) {
        delete ret.__v;
      }
    }
    ,toJSON: {
      virtuals: true,
      transform: function (doc, ret) {
        delete ret.__v;
      }
    },
    timestamps: true
  };


/**
 * Item schema
 */
const carmakeSchema = mongoose.Schema({
    name: String,
    photo: String,
    status: String,
    is_enable: String
}, schemaOptions);

carmakeSchema.virtual('makephoto').get(function () {
    let photo=this.photo;
    if(photo!="")
    {
      return commonHelper.getBaseurl()+"/uploads/make/"+this.photo;
    }
    else
    { 
      return commonHelper.getBaseurl()+"/images/no_profile_img.png";
    }
});

carmakeSchema.plugin(mongoosePaginate);
module.exports = mongoose.model('CarMake',carmakeSchema);