// grab the things we need
const mongoose = require('mongoose');
const moment = require("moment");
const crypto = require("crypto");
var uniqueValidator = require('mongoose-unique-validator');

//create schemaOptions
var schemaOptions = {
  toObject: {
    virtuals: true
  }
  ,toJSON: {
    virtuals: true
  },
  timestamps: true
};


/**
* User schema
*/
const adminuserSchema = mongoose.Schema({
  role: Number,
  username: {
    type: String,
    trim: true
  },
  email: {
    type: String,
  },
  password: {
    type: String
  },
  salt: {
    type: String,
  },
  /* For reset password */
  resetPasswordToken: String,
  resetPasswordExpires: Date
}, schemaOptions);
// on every save, add the date
adminuserSchema.pre('save', function(next) {
// get the current date
if (this.password) {
  this.salt = Buffer.from(crypto.randomBytes(16).toString('base64'), 'base64');
  this.password = this.hashPassword(this.password);
}
next();
});

/**
* Create instance method for hashing a password
*/
adminuserSchema.methods.hashPassword = function (password) {
if (this.salt && password) {
	return crypto.pbkdf2Sync(password, this.salt, 10000, 64, 'sha512').toString('base64');
}
return password;
};

/**
* Create instance method for authenticating user
*/
adminuserSchema.methods.comparePassword = function (password) {
return this.password === this.hashPassword(password);
};

adminuserSchema.plugin(uniqueValidator);
module.exports = mongoose.model('adminUser',adminuserSchema);