// grab the things we need
const mongoose = require('mongoose');
const moment = require("moment");
const commonHelper = require('../helpers/commonHelper');
mongoose.set('useFindAndModify', false);
var mongoosePaginate = require('mongoose-paginate-v2');
//create schemaOptions
var schemaOptions = {
    toObject: {
      virtuals: true
    }
    ,toJSON: {
      virtuals: true
    },
    timestamps: true
  };


/**
 * User schema
 */
const notificationSchema = mongoose.Schema({
    user_by: String,
    user_to: String,
    reservation_id: String,
    message: String,
    is_read: Number,
    car_id: String,
}, schemaOptions);

notificationSchema.virtual('user_by_details', {
  ref : 'User',      // fetch from user model
  localField : 'user_by',
  foreignField : '_id',
  justOne : true
})

notificationSchema.virtual('car_details', {
  ref : 'Car',      // fetch from listings model
  localField : 'car_id',
  foreignField : '_id',
  justOne : true
})

notificationSchema.virtual('reservation_details', {
  ref : 'Reservation',      // fetch from reservation model
  localField : 'reservation_id',
  foreignField : '_id',
  justOne : true
})

notificationSchema.plugin(mongoosePaginate);
module.exports = mongoose.model('Notifications',notificationSchema);