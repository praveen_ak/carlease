// grab the things we need
const mongoose = require('mongoose');
const moment = require("moment");
mongoose.set('useFindAndModify', false);
var mongoosePaginate = require('mongoose-paginate-v2');
//create schemaOptions
var schemaOptions = {
    toObject: {
      virtuals: true
    }
    ,toJSON: {
      virtuals: true
    },
    timestamps: true
  };

const reservationSchema = mongoose.Schema({
    token: String,
    charge_id: String,
    amount: Number,
    total_amount: Number,
    month_price: Number,
    currency: String,
    startdate: Number,
    nextdate: Number,
    enddate: Number,
    terms: Number,
    no_of_times_paid : Number,
    car_details: Object,
    user_id: String,
    booking_additional_price: Object,
    status: String
}, schemaOptions);

reservationSchema.virtual('user_details', {
  ref : 'User',      // fetch from user model
  localField : 'user_id',
  foreignField : '_id',
  justOne : true
})

reservationSchema.plugin(mongoosePaginate);
mongoose.set('useCreateIndex', true);
module.exports = mongoose.model('Reservation',reservationSchema);