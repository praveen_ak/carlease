
const express = require('express');
const router = express.Router();
const User = require('../models/user');

const UserController = require('../controllers/user');
const mongoose = require('mongoose');
router.post('/user_register',UserController.register);
router.post('/user_login', UserController.login);
router.post('/get_users', UserController.get_users);
router.post('/update_user', UserController.update_user);
router.post('/price_calculate', UserController.price_calculate);
router.post('/get_search_results', UserController.get_search_results);
router.post('/get_max_price', UserController.get_max_price);
router.post('/get_model_by_make', UserController.get_model_by_make)

router.post('/payment_success', UserController.payment_success);
router.post('/get_reservations', UserController.get_reservations);
router.post('/get_user_reservation', UserController.get_user_reservation);
module.exports = router;