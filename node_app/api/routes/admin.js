// grab the things we need
const express = require('express');
const router = express.Router();
const adminUser = require('../models/admin_user');

const AdminController = require('../controllers/admin_user');
const mongoose = require('mongoose');
router.post('/first_admin',AdminController.first_admin);
router.post('/check_login',AdminController.checkLogin);

router.post('/get_cars', AdminController.get_cars);
router.post('/get_car_image_category', AdminController.get_car_image_category);
router.post('/get_image_category_detail', AdminController.get_image_category_detail);
router.post('/update_image_category', AdminController.update_image_category);
router.post('/delete_image_category', AdminController.delete_image_category);

router.post('/get_car_make', AdminController.get_car_make);
router.post('/get_active_make', AdminController.get_active_make);
router.post('/delete_car_make', AdminController.delete_car_make);
router.post('/update_make', AdminController.update_make);
router.post('/get_make_detail', AdminController.get_make_detail);

router.post('/add_car', AdminController.add_car);
router.post('/upload_car_photo', AdminController.upload_car_photo)
router.post('/delete_photo', AdminController.delete_photo);
router.post('/get_car_photo', AdminController.get_car_photo);
router.post('/update_catgory', AdminController.update_catgory);
router.post('/get_car_detail', AdminController.get_car_detail);
router.post('/delete_car', AdminController.delete_car);
router.post('/get_latest_cars', AdminController.get_latest_cars);

router.post('/get_admin_notifications', AdminController.get_admin_notifications);
router.post('/get_dashboard_result', AdminController.get_dashboard_result);
module.exports = router;