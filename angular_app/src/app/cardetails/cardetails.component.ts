import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RestService } from '../rest.service';
import { MatSnackBar, MatDialog, MatDialogConfig } from '@angular/material';
import { CommonService } from '../common.service';
import { MatCalendar, DateAdapter } from '@angular/material';
import * as _moment from 'moment';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';
import { StripeService, Elements, Element as StripeElement, ElementsOptions } from "ngx-stripe";
import { MatProgressButtonOptions } from 'mat-progress-buttons';
const moment = _moment;
import * as _ from "lodash";
import * as Coloraze from "coloraze";
const coloraze = new Coloraze();

@Component({
  selector: 'app-cardetails',
  templateUrl: './cardetails.component.html',
  styleUrls: ['./cardetails.component.css']
})
export class CardetailsComponent<D> implements OnInit {
	btnOpts: MatProgressButtonOptions = {
		active: false,
		text: 'Confirm and Pay',
		spinnerSize: 18,
		raised: true,
		stroked: false,
		spinnerColor: 'accent',
		fullWidth: false,
		disabled: false,
		mode: 'indeterminate',
		type: 'submit'
	};
  car_details;
  photo_details;
  extra_color_price;
  total_extra_color_price = 0;
	minDate: D = this._dateAdapter.today();
	min = this._dateAdapter.today();
  displayedColumns: string[] = ['color_name', 'price'];
  startdate;
  enddate;
  terms;
  terms_array=[];
  is_color: Boolean = true;
  car_color;
  sub_total_price = [];
  total_price;
  is_loading: Boolean = false;
	elements: Elements;
	card: StripeElement;
	stripeTest: FormGroup;
	elementsOptions: ElementsOptions = {
		locale: 'en'
	};
  constructor(private dialog: MatDialog,public _dateAdapter: DateAdapter<D>,public router: Router, public rest: RestService, public snackBar: MatSnackBar, public commonservice: CommonService,public _fb: FormBuilder,public stripeService: StripeService) { }

  ngOnInit() {
    let params = {
      'id': this.commonservice.getsecond_index().split('?')[0]
    }
    let data = JSON.stringify(params);
    this.rest.post('get_car_detail', data).subscribe((response) => {
      if(response.status) {
        this.car_details = response.data.admin_car;
        if(this.car_details.price) {
          for (let index = 0; index < this.car_details.price.length; index++) {
            if(this.car_details.price[index].price) {
              const element = this.car_details.price[index].price;
              var term_value = this.car_details.price[index].duration.split(" ");
              this.terms_array.push({'duration': this.car_details.price[index].duration, 'price': this.car_details.price[index].price, "term": term_value[0]});
            }
          }
        }
        this.photo_details  = response.data.photo_details;
        this.extra_color_price = response.data.admin_car.extra_price
      }
    })
  }

  term_change(event) {
    if(this.startdate) {
      this.enddate = moment(this.startdate).add(event.value, 'months')['_d'];
      this.enddate = moment(this.enddate).format('l');
    }
    this.price_calculate('');
  }

  start_date() {
    this.enddate = moment(this.startdate).add(this.terms, 'months')['_d'];
    this.enddate = moment(this.enddate).format('l');
    this.price_calculate('');
  }

  is_avail_color(color) {
    if(!color) {
      this.car_color = {};
      this.price_calculate('');
    }
  }

  color_change(color) {
    this.car_color = color;
    this.price_calculate('');
  }

  price_calculate(templateRef) {
    if(!this.commonservice.user) {
      this.snackBar.open("Car can't booking without login", "", {
        panelClass: 'orange-text',
        duration: 2000
      })
      return
    }
    else if(this.terms == undefined) {
      this.snackBar.open("Choose your term", "", {
        panelClass: 'orange-text',
        duration: 2000
      })
      return
    }
    else if(this.startdate == undefined) {
      this.snackBar.open("Choose start date", "", {
        panelClass: 'orange-text',
        duration: 2000
      })
      return
    }
    let params = {
      'term': this.terms,
      'startdate': moment(this.startdate).format('X'),
      'user_id': this.commonservice.user.id,
      'id': this.commonservice.getsecond_index().split('?')[0]
    }
    this.is_loading = true;
    let data = JSON.stringify(params);
    this.rest.post('price_calculate', data).subscribe((response) => {
      this.is_loading = false;
      if(response.status) {
        let month;
        if(this.terms == 1) {
          month = this.terms + ' month';
        }
        else {
          month = this.terms + ' months';
        }
        let entry = {'duration':month}
        if(this.car_color && !_.isEmpty(this.car_color)) {
          this.total_extra_color_price = this.car_color.price * this.terms;
        }
        else {
          this.total_extra_color_price = 0;
        }
        
        this.sub_total_price = _.filter(response.data.price_details.price, _.matches(entry));
        this.total_price = this.sub_total_price[0].price + this.total_extra_color_price;
        if(templateRef != '') {
          let dialogRef = this.dialog.open(templateRef, {
            panelClass: ['w-100', 'mw-500']
          });
          this.stripeTest = this._fb.group({
            name: ['', [Validators.required]]
          });
          this.stripeService.elements(this.elementsOptions).subscribe(elements => {
            this.elements = elements;
            dialogRef.afterClosed().subscribe(result => {
              this.card = undefined;
            });
            // Only mount the element the first time
            if (!this.card) {
              this.card = this.elements.create('card', {
                iconStyle: "solid",
                hidePostalCode: true,
                style: {
                  base: {
                    iconColor: '#666EE8',
                    color: '#212529',
                    lineHeight: '40px',
                    fontWeight: 300,
                    fontSmoothing: "antialiased",
                    '::placeholder': {
                      color: '#9E9E9E'
                    },
                  },
                  invalid: {
                    iconColor: "#eb1c26",
                    color: "#eb1c26"
                  }
                }
              });
              this.card.mount('#card-element');
            }
          });
        }
      }
      else {
        this.snackBar.open(response.message, '', {
          panelClass: 'orange-text',
          duration: 2000
        })
      }
    })
  }
  buy_car() {
		this.btnOpts.active = true;
		this.btnOpts.text = "Payment processing...";
		this.stripeService.createToken(this.card,{}).subscribe(obj => {
			if (obj.token) {
				var pay_params = {
					'token': obj.token.id,
					'amount': this.sub_total_price[0].price,
          'total_amount': this.total_price,
          'month_price': (this.total_price / this.terms).toFixed(2),
					'currency': 'USD',
					'startdate': moment(this.startdate).format('X'),
					'enddate': moment(this.enddate).format('X'),
					'terms': this.terms,
					'car_details': {
						'id': this.commonservice.getsecond_index().split('?')[0],
						'make': this.car_details.make_title.name,
            'model': this.car_details.model,
            'transmission_type': this.car_details.transmission_type,
						'body_type': this.car_details.body_type,
            'fuel_type': this.car_details.fuel_type,
            'seat': this.car_details.seat,
            'engine_cc': this.car_details.engine_capacity,
            'bhp': this.car_details.bhp,
            'mileage': this.car_details.mileage,
            'fuel_capacity': this.car_details.fuel_capacity,
            'photo': this.photo_details[0].car_photo,
					},
          'user_id': this.commonservice.user.id,
          'user_email': this.commonservice.user.email,
					'booking_additional_price': this.car_color
				}
        let pay_data = JSON.stringify(pay_params);
				this.rest.post('payment_success',pay_data).subscribe((response) => {
					if(response.status) {
						this.btnOpts.active = false;
						this.btnOpts.text = "Confirm and Pay";
						const snackBarRef = this.snackBar.open(response.message,'',{
							duration: 2000,
							panelClass: 'green-text'
						})
					}
					else {
						this.btnOpts.active = false;
						this.btnOpts.text = "Confirm and Pay";
						this.snackBar.open(response.message,'',{
							duration: 2000,
							panelClass: 'orange-text'
						})
          }
          this.dialog.closeAll();
				},
				(err)=>{
					this.btnOpts.active = false;
					this.btnOpts.text = "Confirm and Pay";
					this.snackBar.open(err.message.message,'',{
						duration: 2000,
						panelClass: 'orange-text'
					})
				})
			} else {
				this.btnOpts.active = false;
				this.btnOpts.text = "Confirm and Pay";
				this.snackBar.open(obj.error.message,'',{
					duration: 2000,
					panelClass: 'orange-text'
				})
			}
		});
  }
  get_color_name(color_code) {
    console.log(color_code);
    return coloraze.name(color_code)
  }

}
