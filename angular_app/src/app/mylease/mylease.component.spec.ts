import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyleaseComponent } from './mylease.component';

describe('MyleaseComponent', () => {
  let component: MyleaseComponent;
  let fixture: ComponentFixture<MyleaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyleaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyleaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
