import { Component, OnInit } from '@angular/core';
import { CommonService } from '../common.service';
import { RestService } from '../rest.service';

@Component({
  selector: 'app-mylease',
  templateUrl: './mylease.component.html',
  styleUrls: ['./mylease.component.css']
})
export class MyleaseComponent implements OnInit {
  leases;
	page = 1;
	current = 1;
	totalDocs= 0;
	totalPages= 0;
	startPage=1;
	endPage=1;
	pages:any;
  constructor(public commonservice: CommonService, public rest: RestService) { }

  ngOnInit() {
    this.get_leases(this.page);
  }
  get_leases(page) {
    let params = {
      'user_id': this.commonservice.user.id
    }
    let data = JSON.stringify(params);
    this.rest.post('get_user_reservation',data).subscribe((response) => {
      if(response.status) {
        this.leases = response.data.reservation.docs;
        this.current = response.data.reservation.page;
        this.totalDocs = response.data.reservation.totalDocs;
        this.totalPages = response.data.reservation.totalPages;
        if (this.totalPages <= 5) {
          this.startPage = 1;
          this.endPage = this.totalPages;
        }
        else {
          if (this.current <= 3) {
            this.startPage = 1;
            this.endPage = 5;
          }
          else if (this.current + 2 >= this.totalPages) {
            this.startPage = this.totalPages - 4;
            this.endPage = this.totalPages;
          }
          else {
            this.startPage = this.current - 2;
            this.endPage = this.current + 2;
          }
        }
        if (this.pages && this.pages.length > 1) {
          this.pages = Array.from(Array((this.endPage + 1) - this.startPage).keys()).map(i => this.startPage + i);
        }
      }
    })
  }

}
