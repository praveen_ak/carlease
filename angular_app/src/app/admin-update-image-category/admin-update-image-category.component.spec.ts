import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminUpdateImageCategoryComponent } from './admin-update-image-category.component';

describe('AdminUpdateImageCategoryComponent', () => {
  let component: AdminUpdateImageCategoryComponent;
  let fixture: ComponentFixture<AdminUpdateImageCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminUpdateImageCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminUpdateImageCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
