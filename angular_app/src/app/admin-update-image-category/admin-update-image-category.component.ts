import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {MatSnackBar} from '@angular/material';
import { RestService } from '../rest.service';
import { CommonService } from '../common.service'
import { MatProgressButtonOptions } from 'mat-progress-buttons';

@Component({
  selector: 'app-admin-update-image-category',
  templateUrl: './admin-update-image-category.component.html',
  styleUrls: ['./admin-update-image-category.component.css']
})
export class AdminUpdateImageCategoryComponent implements OnInit {
  btnOpts: MatProgressButtonOptions = {
		active: false,
		text: 'Submit',
		spinnerSize: 18,
		raised: true,
		stroked: false,
		spinnerColor: 'accent',
		fullWidth: false,
		disabled: false,
		mode: 'indeterminate',
	};
  name="";
  status="active";
  id="";
  returnUrl: string;
  page_title: String;
  showForm:Boolean = true;
  thispageLoad:Boolean = false;
  thisEditpage:Boolean = false;

  constructor(public rest:RestService,public router: Router,public snackBar: MatSnackBar,public commonservice: CommonService) { }

  ngOnInit() {
    this.returnUrl = 'admin/image_category';
    this.page_title = "Add Image Category";
    if(this.commonservice.getthird_index()=="edit")
    {
      this.page_title = "Update Image Category";
      this.thisEditpage=true;
      this.thispageLoad=true;
      let params={
        'id' : this.commonservice.urlFourthSegment
      };
      let data=JSON.stringify(params);
      this.rest.post('get_image_category_detail',data).subscribe((response) => {
        if(response.status)
        {
          this.showForm=true;
          this.name=response.data.admin_image_category.name;
          this.status=response.data.admin_image_category.status;
          this.id=response.data.admin_image_category.id;
        }
        else
        {
          this.showForm=false;
        }
        this.thispageLoad=false;
      });
    }
  }
  update_image_category()
  {
  	if(this.name=="")
  	{
  	  this.snackBar.open('Enter a image category name','',{
  	    duration: 2000,
  	    panelClass: ['orange-text']
  	  });
  	}
  	else
  	{
			this.btnOpts.active = true;
			this.btnOpts.text = "Saving...";
      let params={
        'id' : this.id,
        'name': this.name,
        'status': this.status,
        'type': this.thisEditpage
      };
      let data=JSON.stringify(params);
   
  		this.rest.post('add_image_category',data).subscribe((response) => {
  			if(response.status)
  			{
          this.btnOpts.active = false;
          this.btnOpts.text = "Submit";
  				this.snackBar.open(response.message,'',{
  					duration: 2000,
  					panelClass: ['green-text']
  				});
  				this.router.navigateByUrl(this.returnUrl);
  			}
  			else
  			{
  				this.snackBar.open(response.message,'',{
  					duration: 2000,
  					panelClass: ['orange-text']
  				});
  			}
  		});
  	} 
  }

}
