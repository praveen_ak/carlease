import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { BrowserXhr } from '@angular/http';
import { HttpClient, HttpClientModule, HttpClientJsonpModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {CustExtBrowserXhr} from './cust-ext-browser-xhr';
import { CommonService } from './common.service';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { MatFormFieldModule, MatSnackBarModule, MatDialogModule, MatSidenavModule, MatIconModule, MatToolbarModule, MatListModule, MatSelectModule, MatInputModule, MatButtonModule, MatProgressSpinnerModule, MatBottomSheetModule, MatTableModule, MatAutocompleteModule, MatStepperModule, MatRadioModule, MatSlideToggleModule, MatDatepickerModule, MatNativeDateModule } from '@angular/material';
import { MatProgressButtonsModule } from 'mat-progress-buttons';
import { NgxFileDropModule } from 'ngx-file-drop';
import { ColorSketchModule } from 'ngx-color/sketch';
import { Ng2ImgMaxModule } from 'ng2-img-max';
import { CarouselModule } from 'ngx-owl-carousel-o';
import {StickyModule} from 'ng2-sticky-kit';
import { Ng5SliderModule } from 'ng5-slider';
import { NgxStripeModule } from 'ngx-stripe';
import { InternationalPhoneNumberModule } from 'ngx-international-phone-number';

import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

import { AdminLoginComponent } from './admin-login/admin-login.component';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { AdminSidebarComponent } from './admin-sidebar/admin-sidebar.component';
import { AdminCarComponent } from './admin-car/admin-car.component';
import { AdminCarUpdateComponent } from './admin-car-update/admin-car-update.component';
import { AdminImageCategoryComponent } from './admin-image-category/admin-image-category.component';
import { AdminUpdateImageCategoryComponent } from './admin-update-image-category/admin-update-image-category.component';
import { AdminMakeComponent } from './admin-make/admin-make.component';
import { AdminMakeUpdateComponent } from './admin-make-update/admin-make-update.component';
import { HomepageComponent } from './homepage/homepage.component';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UserheaderComponent } from './userheader/userheader.component';
import { AdminUsersComponent } from './admin-users/admin-users.component';
import { CardetailsComponent } from './cardetails/cardetails.component';
import { SearchComponent } from './search/search.component';
import { EditprofileComponent } from './editprofile/editprofile.component';
import { MyleaseComponent } from './mylease/mylease.component';
import { AdminNotificationComponent } from './admin-notification/admin-notification.component';
import { AdminBookingComponent } from './admin-booking/admin-booking.component';

@NgModule({
  declarations: [
    AppComponent,
    AdminLoginComponent,
    AdminDashboardComponent,
    AdminSidebarComponent,
    AdminCarComponent,
    AdminCarUpdateComponent,
    AdminImageCategoryComponent,
    AdminUpdateImageCategoryComponent,
    AdminMakeComponent,
    AdminMakeUpdateComponent,
    HomepageComponent,
    SignupComponent,
    LoginComponent,
    DashboardComponent,
    UserheaderComponent,
    AdminUsersComponent,
    CardetailsComponent,
    SearchComponent,
    EditprofileComponent,
    MyleaseComponent,
    AdminNotificationComponent,
    AdminBookingComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    HttpClientJsonpModule,
    BrowserAnimationsModule,
    MDBBootstrapModule.forRoot(),
    MatFormFieldModule,
    MatSnackBarModule,
    MatDialogModule,
    MatSidenavModule,
    MatIconModule,
    MatToolbarModule,
    MatToolbarModule,
    MatListModule,
    MatSelectModule,
    MatInputModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    MatBottomSheetModule,
    MatTableModule,
    MatAutocompleteModule,
    MatStepperModule,
    FormsModule,
    ReactiveFormsModule,
    MatProgressButtonsModule,
    NgxFileDropModule,
    ColorSketchModule,
    Ng2ImgMaxModule,
    MatRadioModule,
    MatSlideToggleModule,
    CarouselModule,
    MatDatepickerModule,
    MatNativeDateModule,
    StickyModule,
    Ng5SliderModule,
    NgxStripeModule.forRoot('pk_test_VLhpypvPGvKYl2Q72RwOFo5d'),
    InternationalPhoneNumberModule,
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
      }
    })
  ],
  schemas: [ NO_ERRORS_SCHEMA ],
  providers: [
    CommonService,
    {provide: BrowserXhr,useFactory: CustExtBrowserXhr}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}