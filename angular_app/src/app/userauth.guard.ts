import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserauthGuard implements CanActivate {
  constructor(private router : Router){}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    let url: string = state.url;  
    return this.userLogin(url);
  }
  userLogin(url) : boolean {
    let logStatus=this.isUserLogged();
    if(!logStatus){
        this.router.navigate(['login']);
        return false;
    }
    else if(logStatus){
        return true;
    }
  }
  public isUserLogged(): boolean {
      let status = false;
      if( localStorage.getItem('isUserLogged') == "true"){ 
        status = true;
      }
      else{
        status = false;
      }
      return status;
  }
}
