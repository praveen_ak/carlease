import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  sharedData: any;
  user_login:any;
  user:any;
  urlFirstSegment: any;
  urlSecondSegment: any;
  urlThirdSegment: any;
  urlFourthSegment: any;
  urlFifthSegment: any;
  commonData: Object;
  admin_login: any;
  getfirst_index() {
    return this.urlFirstSegment;
  }
  getsecond_index() {
    return this.urlSecondSegment;
  }
  getthird_index() {
    return this.urlThirdSegment;
  }
  getfourth_index() {
    return this.urlFourthSegment;
  }
  getfifth_index() {
    return this.urlFifthSegment;
  }
  constructor() {
    var commonData = {}
    const commonDataElement = document.getElementById('commonData')
    if (commonDataElement && commonDataElement.innerHTML) {
      commonData = JSON.parse(commonDataElement.innerHTML)
    }
    this.commonData = commonData
  }
}