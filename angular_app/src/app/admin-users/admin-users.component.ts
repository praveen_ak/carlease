import { Component, OnInit } from '@angular/core';
import { RestService } from '../rest.service';
import { Router } from '@angular/router';
import { MatSnackBar, MatBottomSheet, MatBottomSheetRef, MatDialog } from '@angular/material';

@Component({
	selector: 'app-admin-users',
	templateUrl: './admin-users.component.html',
	styleUrls: ['./admin-users.component.css']
})
export class AdminUsersComponent implements OnInit {

	displayedColumns: string[] = ['name', 'email', 'phone_number','licence', 'national', 'active', 'action'];
	public data = [];
	spinner_color = 'warn';
	spinner_mode = 'indeterminate';
	spinner_value = 20;
	page = 1;
	current = 1;
	totalDocs = 0;
	totalPages = 0;
	startPage = 1;
	endPage = 1;
	deleteId = "";
	delete_btn_loading: Boolean = false;
	pages: any;
	preview_img;

	constructor(public rest: RestService, private bottomSheet: MatBottomSheet, public router: Router, public snackBar: MatSnackBar, public dialog: MatDialog) { }

	deleteConfirmation(templateRef, id) {
		this.deleteId = id;
		let dialogRef = this.bottomSheet.open(templateRef, {
			panelClass: 'delete-confirmation-width'
			// data: { name: this.name, animal: this.animal }
		});
	}
	closeConfirmation() {
		this.bottomSheet.dismiss();
	}
	deleteUser(user_status) {
		this.delete_btn_loading = true;
		const uploadData = new FormData();
		uploadData.append('id', this.deleteId);
		uploadData.append('status', user_status);
		let data = uploadData;
		let params = {
		}
		this.rest.upload('update_user', data, params).subscribe((response) => {
			if (response.status) {
				this.snackBar.open(response.message, '', {
					duration: 2000,
					panelClass: 'green-text'
				})
			}
			else {
				this.snackBar.open(response.message, '', {
					duration: 2000,
					panelClass: 'orange-text'
				})
			}
			this.delete_btn_loading = false;
			this.deleteId = "";
			this.get_users(this.page);
			this.bottomSheet.dismiss();
		})
	}

	get_users(page) {
		let params = {
			'page': page
		};
		this.rest.post('get_users', JSON.stringify(params)).subscribe((response) => {
			this.data = response.data.user.docs;
			this.current = response.data.user.page;
			this.totalDocs = response.data.user.totalDocs;
			this.totalPages = response.data.user.totalPages;
			if (this.totalPages <= 5) {
				this.startPage = 1;
				this.endPage = this.totalPages;
			}
			else {
				if (this.current <= 3) {
					this.startPage = 1;
					this.endPage = 5;
				}
				else if (this.current + 2 >= this.totalPages) {
					this.startPage = this.totalPages - 4;
					this.endPage = this.totalPages;
				}
				else {
					this.startPage = this.current - 2;
					this.endPage = this.current + 2;
				}
			}
			this.pages = Array.from(Array((this.endPage + 1) - this.startPage).keys()).map(i => this.startPage + i);
		});
	}
	ngOnInit() {
		this.data = [];
		this.get_users(this.page);
	}
	change_active(user_id, status) {
		const uploadData = new FormData();
		uploadData.append('id', user_id);
		uploadData.append('is_active', status);
		let data = uploadData;
		let params = {
		}
		this.rest.upload('update_user', data, params).subscribe((response) => {
			if (response.status) {
				this.snackBar.open(response.message, '', {
					duration: 2000,
					panelClass: 'green-text'
				})
			}
			else {
				this.snackBar.open(response.message, '', {
					duration: 2000,
					panelClass: 'orange-text'
				})
			}
		})
	}
	showImage(template,image) {
		this.preview_img = image;
		let dialog_ref = this.dialog.open(template, {
			panelClass: ''
		})
	}

}
