import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { MatProgressButtonOptions } from 'mat-progress-buttons';
import { MatStepper } from '@angular/material/stepper';
import { RestService } from '../rest.service';
import { CommonService } from '../common.service';
import { NgxFileDropEntry, FileSystemFileEntry, FileSystemDirectoryEntry } from 'ngx-file-drop';
import { ColorEvent } from 'ngx-color';
import { MatSnackBar } from '@angular/material';
import { Ng2ImgMaxService } from 'ng2-img-max';
import { Router } from '@angular/router';
import * as Coloraze from "coloraze";
const coloraze = new Coloraze();
var extra_price = [];
@Component({
  selector: 'app-admin-car-update',
  templateUrl: './admin-car-update.component.html',
  styleUrls: ['./admin-car-update.component.css']
})
export class AdminCarUpdateComponent implements OnInit {
  btnOpts: MatProgressButtonOptions = {
    active: false,
    text: 'Next',
    spinnerSize: 18,
    raised: true,
    stroked: false,
    spinnerColor: 'accent',
    fullWidth: false,
    disabled: false,
    mode: 'indeterminate',
  };
  btnExtraOpts: MatProgressButtonOptions = {
    active: false,
    text: 'Add',
    spinnerSize: 18,
    raised: true,
    stroked: false,
    spinnerColor: 'accent',
    fullWidth: false,
    disabled: false,
    mode: 'indeterminate',
  };
  status = "active";
  id = "";
  returnUrl: string;
  page_title: String;
  thispageLoad: Boolean = false;
  thisEditpage: Boolean = false;
  myControl = new FormControl();
  make: string[];
  active_make;
  filter_make: Observable<string[]>;
  state = '#000000';
  car_form_value = {};
  subscription_price = [];
  subscription_names = [];
  subscription_prices_name_value = [];
  before_carimages = [];
  carImages = [];
  car_images;
  categories;
  loadingStatus: Boolean;
  color_price;
  public extra_color_price = [];

	spinner_color = 'warn';
	spinner_mode = 'indeterminate';
  spinner_value = 20;
  
  displayedColumns: string[] = ['color_name', 'price', 'action'];
  constructor(public router: Router ,public rest: RestService, public commonservice: CommonService, public ng2ImgMax: Ng2ImgMaxService, public snackBar: MatSnackBar) { }
  @ViewChild('stepper') private myStepper: MatStepper;

  ngOnInit() {
    this.returnUrl = 'admin/make';
    this.page_title = "Add your Car";
    this.getCarPhoto();
    if(this.commonservice.getthird_index()=="edit")
    {
      this.page_title = "Update your Car";
      this.thisEditpage=true;
      this.thispageLoad=true;
      let params={
        'id' : this.commonservice.urlFourthSegment
      };
      let data=JSON.stringify(params);
      this.rest.post('get_car_detail',data).subscribe((response) => {
        if(response.status)
        {
          this.car_form_value['make'] = response.data.admin_car.make_title.name;
          this.car_form_value['model'] = response.data.admin_car.model;
          this.car_form_value['transmission_type'] = response.data.admin_car.transmission_type;
          this.car_form_value['fuel_type'] = response.data.admin_car.fuel_type;
          this.car_form_value['body_type'] = response.data.admin_car.body_type;
          this.car_form_value['seat'] = response.data.admin_car.seat;
          this.car_form_value['engine_cc'] = response.data.admin_car.engine_capacity;
          this.car_form_value['bhp'] = response.data.admin_car.bhp;
          this.car_form_value['kilometer'] = response.data.admin_car.mileage;
          this.car_form_value['fuel_capacity'] = response.data.admin_car.fuel_capacity;
          this.extra_color_price = response.data.admin_car.extra_price
          for (var i = 0; i < response.data.admin_car.price.length; i++) {
            this.subscription_price[i] = response.data.admin_car.price[i].price;
          }
          this.status=response.data.admin_car.status;
          this.id=response.data.admin_car.id;
          this.getCarPhoto();
        }
        else {
        }
        this.thispageLoad=false;
      });
    }
    this.rest.post('get_active_make', {}).subscribe((response) => {
      if (response.status) {
        this.active_make = response.data.active_make;
        this.make = response.data.make_group;
        this.filter_make = this.myControl.valueChanges.pipe(
          startWith(''),
          map(value => this._filter(value))
        );
      }
      this.subscription_names = [
        {
          id: 1,
          name: '1 month'
        },
        {
          id: 2,
          name: '3 months'
        },
        {
          id: 3,
          name: '6 months'
        },
        {
          id: 4,
          name: '12 months'
        },
        {
          id: 5,
          name: '18 months'
        },
      ]
    })
  }
  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.make.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
  }

	showcategory(id) {
    let element = document.getElementById(id) as HTMLElement;
    element.classList.contains('active') ? 
    element.classList.remove('active') : 
    element.classList.add('active')
  }

	assignCatgory(category, image_id) {
		let params = {
			"image_category": category.name,
			"id": image_id
		}
		let data = JSON.stringify(params);
		this.rest.post('update_catgory',data).subscribe((response) => {
			if(response.success) {
				this.snackBar.open(response.message,'',{
					duration: 2000,
					panelClass: ['green-text']
				});
			}
			else {				
				this.snackBar.open(response.message,'',{
					duration: 2000,
					panelClass: ['orange-text']
				});
			}
		})
	}

	getCarPhoto() {	
		let params = {
			'car_id': this.id
		}
		let data = JSON.stringify(params);
		this.rest.post('get_car_photo',data).subscribe((response) => {
			this.car_images = response.data.photoDetails;
			this.categories = response.data.imagecatgoryDetails;
		})
  }
  
  goBack() {
    this.myStepper.previous();
  }

  goForward(type) {
    var is_error;
    var msg;
    if (type == 'general') {
      var make_id = this.searchMake(this.car_form_value['make'], this.active_make);
      if (this.car_form_value['make'] == undefined || this.car_form_value['make'] == '' || make_id == undefined) {
        is_error = true;
        msg = "Pick one make"
      }
      else if (this.car_form_value['model'] == undefined || this.car_form_value['model'] == '') {
        is_error = true;
        msg = "Enter model name"
      }
      else if (this.car_form_value['transmission_type'] == undefined || this.car_form_value['transmission_type'] == '') {
        is_error = true;
        msg = "Pick one transmission type"
      }
      else if (this.car_form_value['fuel_type'] == undefined || this.car_form_value['fuel_type'] == '') {
        is_error = true;
        msg = "Pick one fuel type"
      }
      else if (this.car_form_value['body_type'] == undefined || this.car_form_value['body_type'] == '') {
        is_error = true;
        msg = "Pick one body type"
      }
      else if (this.car_form_value['seat'] == undefined || this.car_form_value['seat'] == '') {
        is_error = true;
        msg = "Enter the seating value"
      }
      else if (this.car_form_value['engine_cc'] == undefined || this.car_form_value['engine_cc'] == '') {
        is_error = true;
        msg = "Enter the Engine CC"
      }
      else if (this.car_form_value['bhp'] == undefined || this.car_form_value['bhp'] == '') {
        is_error = true;
        msg = "Enter the BHP"
      }
      else if (this.car_form_value['kilometer'] == undefined || this.car_form_value['kilometer'] == '') {
        is_error = true;
        msg = "Enter the kilometer"
      }
      else if (this.car_form_value['fuel_capacity'] == undefined || this.car_form_value['fuel_capacity'] == '') {
        is_error = true;
        msg = "Enter the fuel capcity"
      }

      if (is_error) {
        this.snackBar.open(msg, '', {
          panelClass: 'orange-text',
          duration: 2000
        })
      }
      else {
        this.btnOpts.active = true;
        this.btnOpts.text = 'Saving...';       
        var itype = 'add';
        if (this.id != '') {
          itype = 'edit';
        }
        let params = {
          'type': itype,
          'make_title': make_id,
          'model': this.car_form_value['model'],
          'transmission_type': this.car_form_value['transmission_type'],
          'fuel_type': this.car_form_value['fuel_type'],
          'body_type': this.car_form_value['body_type'],
          'seat': this.car_form_value['seat'],
          'engine_capacity': this.car_form_value['engine_cc'],
          'bhp': this.car_form_value['bhp'],
          'mileage': this.car_form_value['kilometer'],
          'fuel_capacity': this.car_form_value['fuel_capacity'],
          'is_enable': true,
          'carid': this.id
        }
        let data = JSON.stringify(params);
        this.rest.post('add_car', data).subscribe((response) => {
          this.btnOpts.active = false;
          this.btnOpts.text = 'Next';
          if (response.status) {
            this.id = response.data.admin_car.id;
            this.snackBar.open(response.message, '', {
              panelClass: 'green-text',
              duration: 2000
            })
            this.myStepper.next();
          }
          else {
            this.snackBar.open(response.message, '', {
              panelClass: 'orange-text',
              duration: 2000
            })
          }
        })
      }
    }
    else if (type == 'pricing') {
      this.subscription_prices_name_value = [];
      for (var k = 0; k < this.subscription_names.length; k++) {
        this.subscription_prices_name_value.push({ 'duration': this.subscription_names[k].name, 'price': this.subscription_price[k] });
      }
      for (var k = 0; k < this.subscription_names.length; k++) {
        if (this.subscription_price[k] == '' || this.subscription_price[k] == undefined) {
          is_error = true;
          msg = "Enter the price";
        }
        else {
          is_error = false;
          break
        }
      }
      if (is_error) {
        this.snackBar.open(msg, '', {
          panelClass: 'orange-text',
          duration: 2000
        })
      }
      else {
        this.btnOpts.active = true;
        this.btnOpts.text = 'Saving...';
        if (this.id != '') {
          itype = 'edit';
        }
        let params = {
          'price': this.subscription_prices_name_value,
          'type': itype,
          'carid': this.id
        }
        let data = JSON.stringify(params);
        this.rest.post('add_car', data).subscribe((response) => {
          this.btnOpts.active = false;
          this.btnOpts.text = 'Next';
          if (response.status) {
            this.myStepper.next();
            this.snackBar.open(response.message, '', {
              panelClass: 'green-text',
              duration: 2000
            })
          }
          else {
            this.snackBar.open(response.message, '', {
              panelClass: 'orange-text',
              duration: 2000
            })
          }
        })
      }
    }
    else if(type == 'photo') {
      if(this.car_images.length == 0) {
        this.snackBar.open("Upload minimum one image", '', {
          panelClass: 'orange-text',
          duration: 2000
        })
      }
      else {        
        this.myStepper.next();
      }
    }
    else if(type == 'finish') {
      this.router.navigateByUrl('/admin/car');
    }
  }
  searchMake(nameKey, myArray) {
    for (var i = 0; i < myArray.length; i++) {
      if (myArray[i].name === nameKey) {
        return myArray[i].id;
      }
    }
  }
  public files: NgxFileDropEntry[] = [];

  isFileAllowed(fileName: string) {
    let isFileAllowed = false;
    let not_upload = false;
    const allowedFiles = ['.png', '.jpeg', '.jpg'];
    const regex = /(?:\.([^.]+))?$/;
    const extension = regex.exec(fileName);
    if (undefined !== extension && null !== extension) {
      for (const ext of allowedFiles) {
        if (ext === extension[0]) {
          isFileAllowed = true;
        }
      }
    }
    else {
      isFileAllowed = false;
    }
    if (isFileAllowed == false) {
      not_upload = true;
    }
    else {
      not_upload = false;
    }
    return not_upload;
  }
  public dropped(files: NgxFileDropEntry[]) {
    this.files = files;
    for (var droppedFileIndex = 0; droppedFileIndex < this.files.length; droppedFileIndex++) {
      const droppedFile = this.files[droppedFileIndex];
      if (this.isFileAllowed(droppedFile.fileEntry.name)) {
        this.snackBar.open("Photo error occured", '', {
          duration: 2000,
          panelClass: ['orange-text']
        });
        return false;
      }
    }
    for (const droppedFile of files) {

      // Is it a file?
      if (droppedFile.fileEntry.isFile) {
        const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
        fileEntry.file((file: File) => {
          //console.log(droppedFile.relativePath, file);
          var reader = new FileReader();
          reader.readAsDataURL(file);
          reader.onload = () => {
            document.getElementById('upload_photos').classList.add('disabled');
            this.before_carimages.push({ 'car_photo': reader.result })
            this.loadingStatus = true;
          };
          this.ng2ImgMax.resizeImage(file, 1500, 500).subscribe(
            result => {
              var carimage = new File([result], result.name);
              this.carImages.push(carimage);
              if (files.length == this.carImages.length) {
                this.uploadphoto(this.carImages);
              }
            },
            error => {
              this.loadingStatus = false;
              document.getElementById('upload_photos').classList.remove('disabled');
              this.snackBar.open("Photo error occured", '', {
                duration: 2000,
                panelClass: ['orange-text']
              });
            });
        });
      } else {
        const fileEntry = droppedFile.fileEntry as FileSystemDirectoryEntry;
        console.log(droppedFile.relativePath, fileEntry);
      }
    }
  }

  uploadphoto(carImages) {
    const formData: any = new FormData();
    for (let i = 0; i < carImages.length; i++) {
      formData.append("media", carImages[i]);
    }
    formData.append("car_id", this.id);
    this.rest.upload('upload_car_photo', formData, {}).subscribe((response) => {
      document.getElementById('upload_photos').classList.remove('disabled');
      if (response.status) {
        this.before_carimages = [];
        this.carImages = [];
        this.loadingStatus = false;
        this.car_images = response.data.car_photos;
        this.snackBar.open(response.message, '', {
          duration: 2000,
          panelClass: ['green-text']
        });
      }
      else {
        this.snackBar.open("Photo not Uploaded", '', {
          duration: 2000,
          panelClass: ['orange-text']
        });
      }
    })
  }

  delete_photo(details) {
		let params = {
			"id": details._id,
			"name": details.name,
			"car_id": this.id
		}
		let data = JSON.stringify(params);
		this.rest.post('delete_photo',data).subscribe((response) => {
			if(response.status) {
				this.getCarPhoto();
				this.snackBar.open(response.message,'',{
					duration: 2000,
					panelClass: ['green-text']
				});
			}
			else {				
				this.snackBar.open(response.message,'',{
					duration: 2000,
					panelClass: ['orange-text']
				});
			}
		})
  }

  changeComplete(event) {
    this.state = event.color.hex;
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46) {
      return false;
    }
    return true;
  }

  add_color_price() {
    if (this.color_price == '' || this.color_price == undefined) {
      this.snackBar.open("Enter price", "", {
        panelClass: 'orange-text',
        duration: 2000
      })
    }
    else {
      this.btnExtraOpts.active = true;
      this.btnExtraOpts.text = 'Saving...';
      let model = { 
        'color_code': this.state, 
        'price': this.color_price
      }
      this.extra_color_price.push(model);
      let params = { 
        'extra_price': this.extra_color_price,
        'type': 'edit',
        'carid': this.id
      }
      let data = JSON.stringify(params);
      this.rest.post('add_car', data).subscribe((response) => {
        this.btnExtraOpts.active = false;
        this.btnExtraOpts.text = 'Add';
        if(response.status) {
          this.extra_color_price = [...this.extra_color_price];
          this.state = '#000000';
          this.color_price = '';
          this.snackBar.open(response.message,'',{
            duration: 2000,
            panelClass: ['green-text']
          });
        }
        else {
          this.snackBar.open(response.message,'',{
            duration: 2000,
            panelClass: ['orange-text']
          });
        }
      })
    }
  }

  delete_color_price(element) {
    var index = this.extra_color_price.indexOf(element);
    if (index > -1) {
      this.extra_color_price.splice(index, 1);
      let params = { 
        'extra_price': this.extra_color_price,
        'type': 'edit',
        'carid': this.id
      }
      let data = JSON.stringify(params);
      this.rest.post('add_car', data).subscribe((response) => {
        if(response.status) {
          this.extra_color_price = [...this.extra_color_price];
          this.snackBar.open(response.message,'',{
            duration: 2000,
            panelClass: ['green-text']
          });
        }
        else {
          this.snackBar.open(response.message,'',{
            duration: 2000,
            panelClass: ['orange-text']
          });
        }
      })
    }
  }
  get_color_name(color_code) {
    return coloraze.name(color_code)
  }

}
