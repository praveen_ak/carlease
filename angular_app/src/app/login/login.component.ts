import { Component, OnInit } from '@angular/core';
import { MatProgressButtonOptions } from 'mat-progress-buttons';
import { RestService } from '../rest.service';
import { CommonService } from '../common.service';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  btnOpts: MatProgressButtonOptions = {
    active: false,
    text: 'Submit',
    spinnerSize: 18,
    raised: true,
    stroked: false,
    spinnerColor: 'accent',
    fullWidth: false,
    disabled: false,
    mode: 'indeterminate',
  };
  formValues = {};
  pwd_hide = true;
  userLoggedin;

  constructor(public rest: RestService, public commonservice: CommonService, public snackBar: MatSnackBar, public router: Router) { }

  ngOnInit() {
    this.userLoggedin = this.commonservice.user_login;
		if(this.userLoggedin == 'true') {
			this.router.navigateByUrl('dashboard');
		}
  }
  validateEmail(email) {
    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }
  loginSubmit() {
    if (this.formValues['email'] == undefined || this.formValues['email'] == '') {
      this.snackBar.open('Enter a valid email', '', {
        duration: 2000,
        panelClass: ['orange-text']
      });
    }
    else if (!this.validateEmail(this.formValues['email'])) {
      this.snackBar.open('Invalid email', '', {
        duration: 2000,
        panelClass: ['orange-text']
      });
    }
    else if (this.formValues['pwd'] == undefined || this.formValues['pwd'] == '') {
      this.snackBar.open('Enter Password', '', {
        duration: 2000,
        panelClass: ['orange-text']
      });
    }
    else {
      let params = {
        'email': this.formValues['email'],
        'password': this.formValues['pwd']
      };
      let data = JSON.stringify(params);
      this.btnOpts.active = true;
      this.rest.post('user_login', data).subscribe((response) => {
        if (response.status) {
          this.snackBar.open(response.message, '', {
            duration: 2000,
            panelClass: ['green-text']
          });
          this.commonservice.user_login = 'true';
          this.commonservice.user = response.data.user;
          localStorage.setItem('users', JSON.stringify(response.data.user));
          localStorage.setItem('isUserLogged', 'true');
          this.router.navigateByUrl('dashboard');
        }
        else {
          this.snackBar.open(response.message, '', {
            duration: 2000,
            panelClass: ['orange-text']
          });
        }
        this.btnOpts.active = false;
      })
    }
  }

}
