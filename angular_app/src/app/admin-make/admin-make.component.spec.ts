import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminMakeComponent } from './admin-make.component';

describe('AdminMakeComponent', () => {
  let component: AdminMakeComponent;
  let fixture: ComponentFixture<AdminMakeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminMakeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminMakeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
