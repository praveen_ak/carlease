import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminImageCategoryComponent } from './admin-image-category.component';

describe('AdminImageCategoryComponent', () => {
  let component: AdminImageCategoryComponent;
  let fixture: ComponentFixture<AdminImageCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminImageCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminImageCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
