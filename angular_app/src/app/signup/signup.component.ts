import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MatProgressButtonOptions } from 'mat-progress-buttons';
import { RestService } from '../rest.service';
import { CommonService } from '../common.service';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  btnOpts: MatProgressButtonOptions = {
    active: false,
    text: 'Submit',
    spinnerSize: 18,
    raised: true,
    stroked: false,
    spinnerColor: 'accent',
    fullWidth: false,
    disabled: false,
    mode: 'indeterminate',
  };
  selected_licence_File;
  imageLicenceSrc;
  selected_national_File;
  imageNationalSrc;
  formValues = {};
  pwd_hide = true;
  cpwd_hide = true;
  userLoggedin;
  constructor(public rest: RestService, public commonservice: CommonService, public snackBar: MatSnackBar, public router: Router) { }
  @ViewChild("phoneNumber") phoneNumber: any;

  ngOnInit() {
    this.userLoggedin = this.commonservice.user_login;
		if(this.userLoggedin == 'true') {
			this.router.navigateByUrl('dashboard');
		}
  }
	makeLicenceUpload(event) {
		this.selected_licence_File = event.target.files[0];
    if (this.isFileAllowed(this.selected_licence_File.name)) {
      this.selected_licence_File = undefined;
      this.snackBar.open("Photo error occured", '', {
        duration: 2000,
        panelClass: ['orange-text']
      });
      return false;
    }
    else {
      if (event.target.files && event.target.files[0]) {
          const file = event.target.files[0];

          const reader = new FileReader();
          reader.onload = e => this.imageLicenceSrc = reader.result;

          reader.readAsDataURL(file);
      }
    }
  }
	makeNationalUpload(event) {
    this.selected_national_File = event.target.files[0];
    if (this.isFileAllowed(this.selected_national_File.name)) {
      this.selected_national_File = undefined;
      this.snackBar.open("Photo error occured", '', {
        duration: 2000,
        panelClass: ['orange-text']
      });
      return false;
    }
    else {
	    if (event.target.files && event.target.files[0]) {
	        const file = event.target.files[0];

	        const reader = new FileReader();
	        reader.onload = e => this.imageNationalSrc = reader.result;

	        reader.readAsDataURL(file);
      }
    }
  }

  isFileAllowed(fileName: string) {
    let isFileAllowed = false;
    let not_upload = false;
    const allowedFiles = ['.png', '.jpeg', '.jpg'];
    const regex = /(?:\.([^.]+))?$/;
    const extension = regex.exec(fileName);
    if (undefined !== extension && null !== extension) {
      for (const ext of allowedFiles) {
        if (ext === extension[0]) {
          isFileAllowed = true;
        }
      }
    }
    else {
      isFileAllowed = false;
    }
    if (isFileAllowed == false) {
      not_upload = true;
    }
    else {
      not_upload = false;
    }
    return not_upload;
  }
  validateEmail(email) {
    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }
  registerSubmit() {
    if (this.formValues['fname'] == undefined || this.formValues['fname'] == '') {
      this.snackBar.open('Enter First Name', '', {
        panelClass: "orange-text",
        duration: 2000,
      });
    }
    else if (this.formValues['lname'] == undefined || this.formValues['lname'] == '') {
      this.snackBar.open('Enter Last Name', '', {
        panelClass: "orange-text",
        duration: 2000,
      });
    }
    else if (this.formValues['email'] == undefined || this.formValues['email'] == '') {
      this.snackBar.open('Enter a valid email', '', {
        panelClass: "orange-text",
        duration: 2000,
      });
    }
    else if (!this.validateEmail(this.formValues['email'])) {
      this.snackBar.open('Invalid email', '', {
        panelClass: "orange-text",
        duration: 2000,
      });
    }
    else if(!this.phoneNumber.valid) {
      this.snackBar.open('Invalid Phone number', '', {
        panelClass: "orange-text",
        duration: 2000,
      });
    }
  	else if(this.selected_licence_File==undefined)
  	{
  	  this.snackBar.open('Upload licence image','',{
  	    duration: 2000,
  	    panelClass: ['orange-text']
  	  });
  	}
  	else if(this.selected_national_File==undefined)
  	{
  	  this.snackBar.open('Upload National ID image','',{
  	    duration: 2000,
  	    panelClass: ['orange-text']
  	  });
  	}
    else if (this.formValues['pwd'] == undefined || this.formValues['pwd'] == '') {
      this.snackBar.open('Enter Password', '', {
        panelClass: "orange-text",
        duration: 2000,
      });
    }
    else if (this.formValues['cpwd'] == undefined || this.formValues['cpwd'] == '') {
      this.snackBar.open('Enter Confirm Password', '', {
        panelClass: "orange-text",
        duration: 2000,
      });
    }
    else if (this.formValues['cpwd'] != this.formValues['pwd']) {
      this.snackBar.open('Password is mismatch', '', {
        panelClass: "orange-text",
        duration: 2000,
      });
    }
    else {
      this.btnOpts.active = true;
      this.btnOpts.text = "Saving...";
      const uploadData = new FormData();
      uploadData.append('licence_media', this.selected_licence_File, this.selected_licence_File.name);
      uploadData.append('national_media', this.selected_national_File, this.selected_national_File.name);
      uploadData.append('fname', this.formValues['fname']);
      uploadData.append('lname', this.formValues['lname']);
      uploadData.append('email', this.formValues['email']);
      uploadData.append('phone_number', this.formValues['phone_number']);
      uploadData.append('password', this.formValues['pwd']);
      let data=uploadData;
      let params = {
      }
      this.rest.upload('user_register',data,params).subscribe((response) => {
        if (response.status) {
          this.snackBar.open(response.message, '', {
            panelClass: "green-text",
            duration: 2000,
          });
          this.commonservice.user_login = 'true';
          this.commonservice.user = response.data.user;
          localStorage.setItem('users', JSON.stringify(response.data.user));
          localStorage.setItem('isUserLogged', 'true');
          this.router.navigateByUrl('dashboard');
        }
        else {
          this.snackBar.open(response.message, '', {
            panelClass: "orange-text",
            duration: 2000,
          });
        }
        this.btnOpts.active = false;
        this.btnOpts.text = "Submit";
      })
    }
  }

}
