import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AdminauthGuard implements CanActivate {
  constructor(private router : Router){}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    let url: string = state.url;  
    return this.adminLogin(url);
  }
  adminLogin(url) : boolean{
    let logStatus=this.isAdminLogged();
    if(!logStatus){
        this.router.navigate(['admin/login']);
        return false;
    }
    else if(logStatus){
        return true;
    }
  }
  public isAdminLogged(): boolean{
      let status = false;
      if( localStorage.getItem('isAdminLogged') == "true"){ 
        status = true;
      }
      else{
        status = false;
      }
      return status;
  }
}
