import { Component, OnInit } from '@angular/core';
import { MatProgressButtonOptions } from 'mat-progress-buttons';
import { RestService } from '../rest.service';
import { CommonService } from '../common.service';
import { MatSnackBar } from '@angular/material';
var licence_media;
var national_media;
@Component({
  selector: 'app-editprofile',
  templateUrl: './editprofile.component.html',
  styleUrls: ['./editprofile.component.css']
})
export class EditprofileComponent implements OnInit {
	btnOpts: MatProgressButtonOptions = {
		active: false,
		text: 'Submit',
		spinnerSize: 18,
		raised: true,
		stroked: false,
		spinnerColor: 'accent',
		fullWidth: false,
		disabled: false,
		mode: 'indeterminate',
		type: 'submit'
	};
  user_session;
  constructor(public rest: RestService, public commonservice: CommonService, public snackBar: MatSnackBar) { }

  ngOnInit() {
    this.user_session = this.commonservice.user;
    console.log(this.commonservice.user);
  }
	makeLicenceUpload(event) {
    licence_media = event.target.files[0];
    if (this.isFileAllowed(event.target.files[0].name)) {
      this.snackBar.open("Photo error occured", '', {
        duration: 2000,
        panelClass: ['orange-text']
      });
      return false;
    }
    else {
      if (event.target.files && event.target.files[0]) {
          const file = event.target.files[0];

          const reader = new FileReader();
          reader.onload = e => this.user_session.licencephoto = reader.result;

          reader.readAsDataURL(file);
      }
    }
  }
	makeNationalUpload(event) {
    national_media = event.target.files[0];
    if (this.isFileAllowed(event.target.files[0].name)) {
      this.snackBar.open("Photo error occured", '', {
        duration: 2000,
        panelClass: ['orange-text']
      });
      return false;
    }
    else {
	    if (event.target.files && event.target.files[0]) {
	        const file = event.target.files[0];

	        const reader = new FileReader();
	        reader.onload = e => this.user_session.nationalphoto = reader.result;

	        reader.readAsDataURL(file);
      }
    }
  }

  isFileAllowed(fileName: string) {
    let isFileAllowed = false;
    let not_upload = false;
    const allowedFiles = ['.png', '.jpeg', '.jpg'];
    const regex = /(?:\.([^.]+))?$/;
    const extension = regex.exec(fileName);
    if (undefined !== extension && null !== extension) {
      for (const ext of allowedFiles) {
        if (ext === extension[0]) {
          isFileAllowed = true;
        }
      }
    }
    else {
      isFileAllowed = false;
    }
    if (isFileAllowed == false) {
      not_upload = true;
    }
    else {
      not_upload = false;
    }
    return not_upload;
  }
  validateEmail(email) {
    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }
  update_user() {
    if (this.user_session.fname == '') {
      this.snackBar.open('Enter First Name', '', {
        panelClass: "orange-text",
        duration: 2000,
      });
    }
    else if (this.user_session.lname == '') {
      this.snackBar.open('Enter Last Name', '', {
        panelClass: "orange-text",
        duration: 2000,
      });
    }
    else if (this.user_session.email == '') {
      this.snackBar.open('Enter a valid email', '', {
        panelClass: "orange-text",
        duration: 2000,
      });
    }
    else if (!this.validateEmail(this.user_session.email)) {
      this.snackBar.open('Invalid email', '', {
        panelClass: "orange-text",
        duration: 2000,
      });
    }
    else {
      this.btnOpts.active = true;
      this.btnOpts.text = "Saving...";
      const uploadData = new FormData();
      if(licence_media) {
        uploadData.append('licence_media', licence_media, licence_media.name);
      }
      if(national_media) {
        uploadData.append('national_media', national_media, national_media.name);
      }
      uploadData.append('id', this.user_session.id);
      uploadData.append('fname', this.user_session.fname);
      uploadData.append('lname', this.user_session.lname);
      uploadData.append('email', this.user_session.email);
      let data=uploadData;
      let params = {
      }
      this.rest.upload('update_user',data,params).subscribe((response) => {
        if (response.status) {
          this.snackBar.open(response.message, '', {
            panelClass: "green-text",
            duration: 2000,
          });
          localStorage.setItem('users', JSON.stringify(response.data.user));
          this.commonservice.user = response.data.user;
        }
        else {
          this.snackBar.open(response.message, '', {
            panelClass: "orange-text",
            duration: 2000,
          });
        }
        this.btnOpts.active = false;
        this.btnOpts.text = "Submit";
      })
    }
  }

}
