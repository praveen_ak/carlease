import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import { CommonService } from './common.service';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
  })
}
@Injectable({
  providedIn: 'root'
})
export class RestService {
  urls: Object = {
    admin_login: 'admin/check_login',
    get_cars: 'admin/get_cars',
    get_car_image_category: 'admin/get_car_image_category',
    get_image_category_detail: 'admin/get_image_category_detail',
    add_image_category: 'admin/update_image_category',
    delete_image_category: 'admin/delete_image_category',
    get_car_make: 'admin/get_car_make',
    delete_car_make: 'admin/delete_car_make',
    add_make: 'admin/update_make',
    get_make_detail: 'admin/get_make_detail',
    get_active_make: 'admin/get_active_make',
    add_car: 'admin/add_car',
    upload_car_photo: 'admin/upload_car_photo',
    delete_photo: 'admin/delete_photo',
    get_car_photo: 'admin/get_car_photo',
    update_catgory: 'admin/update_catgory',
    get_car_detail: 'admin/get_car_detail',
    delete_car: 'admin/delete_car',
    user_register: 'users/user_register',
    user_login: 'users/user_login',
    get_users: 'users/get_users',
    update_user: 'users/update_user',
    get_latest_cars: 'admin/get_latest_cars',
    price_calculate: 'users/price_calculate',
    get_search_results: 'users/get_search_results',
    get_max_price: 'users/get_max_price',
    get_model_by_make: 'users/get_model_by_make',
    payment_success: 'users/payment_success',
    get_user_reservation: 'users/get_user_reservation',
    get_admin_notifications: 'admin/get_admin_notifications',
    get_dashboard_result: 'admin/get_dashboard_result',
    get_reservations: 'users/get_reservations'
  };
  restUrl: String = '';
  constructor(private http: HttpClient,public commonservice: CommonService) { 
  	const commonData = this.commonservice.commonData
    this.restUrl = commonData['restUrl']
  }

  getUrl (urlKey) {
  	const uri = this.urls[urlKey]
  	const url = this.restUrl + uri
  	return url
  }

  post(urlKey, params): Observable<any> {
  	const url = this.getUrl(urlKey)
    return this.http.post(url, {params}, httpOptions).pipe(
      map(this.extractData));
  }
  
  upload(urlKey, formData, params): Observable<any> {
    formData.append('params', JSON.stringify(params))
    const url = this.getUrl(urlKey);
    return this.http.post(url, formData, {} ).pipe(
      map(this.extractData));
  }

  private extractData(res: Response) {
    let body = res;
    return body || { };
  }

}