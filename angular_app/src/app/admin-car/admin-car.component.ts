import { Component, OnInit } from '@angular/core';
import { RestService } from '../rest.service';
import { Router } from '@angular/router';
import { MatSnackBar, MatBottomSheet, MatBottomSheetRef } from '@angular/material';

@Component({
	selector: 'app-admin-car',
	templateUrl: './admin-car.component.html',
	styleUrls: ['./admin-car.component.css']
})
export class AdminCarComponent implements OnInit {

	displayedColumns: string[] = ['make_title', 'make_logo', 'model_name', 'fuel_type', 'transmission_type', 'status', 'action'];
	public data = [];
	spinner_color = 'warn';
	spinner_mode = 'indeterminate';
	spinner_value = 20;
	page = 1;
	current = 1;
	totalDocs = 0;
	totalPages = 0;
	startPage = 1;
	endPage = 1;
	deleteId = "";
	delete_btn_loading: Boolean = false;
	pages: any;

	constructor(public rest: RestService, private bottomSheet: MatBottomSheet, public router: Router, public snackBar: MatSnackBar) { }

	deleteConfirmation(templateRef, id) {
		this.deleteId = id;
		let dialogRef = this.bottomSheet.open(templateRef, {
			panelClass: 'delete-confirmation-width'
			// data: { name: this.name, animal: this.animal }
		});
	}
	closeConfirmation() {
		this.bottomSheet.dismiss();
	}
	deleteCar() {
		this.delete_btn_loading = true;
		let params = {
			'carid': this.deleteId,
			'type': 'edit',
			'is_enable': false
		};
		this.rest.post('add_car', JSON.stringify(params)).subscribe((response) => {
			this.snackBar.open(response.message, '', {
				panelClass: 'green-text',
				duration: 2000,
			});
			this.delete_btn_loading = false;
			this.deleteId = "";
			this.get_cars(this.page);
			this.bottomSheet.dismiss();
		});
	}

	get_cars(page) {
		let params = {
			'page': page
		};
		this.rest.post('get_cars', JSON.stringify(params)).subscribe((response) => {
			this.data = response.data.car.docs;
			this.current = response.data.car.page;
			this.totalDocs = response.data.car.totalDocs;
			this.totalPages = response.data.car.totalPages;
			if (this.totalPages <= 5) {
				this.startPage = 1;
				this.endPage = this.totalPages;
			}
			else {
				if (this.current <= 3) {
					this.startPage = 1;
					this.endPage = 5;
				}
				else if (this.current + 2 >= this.totalPages) {
					this.startPage = this.totalPages - 4;
					this.endPage = this.totalPages;
				}
				else {
					this.startPage = this.current - 2;
					this.endPage = this.current + 2;
				}
			}
			this.pages = Array.from(Array((this.endPage + 1) - this.startPage).keys()).map(i => this.startPage + i);
		});
	}
	ngOnInit() {
		this.data = [];
		this.get_cars(this.page);
	}
}
