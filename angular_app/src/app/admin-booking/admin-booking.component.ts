import { Component, OnInit } from '@angular/core';
import { RestService } from '../rest.service';

@Component({
  selector: 'app-admin-booking',
  templateUrl: './admin-booking.component.html',
  styleUrls: ['./admin-booking.component.css']
})
export class AdminBookingComponent implements OnInit {
  total_users=[];
	displayedColumns: string[] = ['name', 'email', 'model', 'terms', 'startdate', 'enddate', 'month_price', 'total_price'];
	public data = [];
	page = 1;
	current = 1;
	totalDocs = 0;
	totalPages = 0;
	startPage = 1;
	endPage = 1;
	pages: any;
  selected_user='';

  constructor(public rest: RestService) { }

  ngOnInit() {
    this.get_all_reservations(this.page)
  }

  get_all_reservations(page) {
		let params = {
      'page': page,
      'user_id': this.selected_user
		};
		this.rest.post('get_reservations', JSON.stringify(params)).subscribe((response) => {
      this.data = response.data.reservation.docs;
      this.total_users = response.data.total_users;
			this.current = response.data.reservation.page;
			this.totalDocs = response.data.reservation.totalDocs;
			this.totalPages = response.data.reservation.totalPages;
			if (this.totalPages <= 5) {
				this.startPage = 1;
				this.endPage = this.totalPages;
			}
			else {
				if (this.current <= 3) {
					this.startPage = 1;
					this.endPage = 5;
				}
				else if (this.current + 2 >= this.totalPages) {
					this.startPage = this.totalPages - 4;
					this.endPage = this.totalPages;
				}
				else {
					this.startPage = this.current - 2;
					this.endPage = this.current + 2;
				}
			}
			this.pages = Array.from(Array((this.endPage + 1) - this.startPage).keys()).map(i => this.startPage + i);
		});
  }

}
