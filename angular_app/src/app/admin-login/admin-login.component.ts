import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { RestService } from '../rest.service';
import { CommonService } from '../common.service';

@Component({
  selector: 'app-admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.css']
})
export class AdminLoginComponent implements OnInit {
  formValues: Object = {};
  public hide: Boolean = true;
  constructor(public rest: RestService, public router: Router, public snackBar: MatSnackBar, public commonservice: CommonService) { }
  ngOnInit() {
    if (localStorage.getItem('isAdminLogged') == "true") {
      this.router.navigateByUrl('admin/dashboard');
    }
  }

  validateEmail(email) {
    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }
  loginSubmit() {
    var error = '';
    // stop here if form is invalid
    if (this.formValues['email'] == undefined || this.formValues['email'] == '') {
      error = 'Enter email';
    }
    else if (!this.validateEmail(this.formValues['email'])) {
      error = 'Enter a valid Email'
    }
    else if (this.formValues['password'] == undefined || this.formValues['password'] == '') {
      error = 'Enter password'
    }
    if (error !== '') {
      this.snackBar.open(error, '', {
        panelClass: 'orange-text',
        duration: 2000,
      });
      return
    }
    let params = {
      'email': this.formValues['email'],
      'password': this.formValues['password']
    }
    let data = JSON.stringify(params);
    this.rest.post('admin_login', data).subscribe((response: { success, message, data }) => {
      if (response.success) {
        this.snackBar.open(response.message, '', {
          duration: 2000,
          panelClass: 'green-text'
        });
        this.commonservice.admin_login = 'true';
        localStorage.setItem('adminUsers', JSON.stringify(response.data.users));
        localStorage.setItem('isAdminLogged', 'true');
        this.router.navigateByUrl('admin/dashboard');
      }
      else {
        this.snackBar.open(response.message, '', {
          duration: 2000,
          panelClass: 'orange-text'
        });
      }
    });
  }
}
