import { Component, OnInit } from '@angular/core';
import { RestService } from '../rest.service';

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.css']
})
export class AdminDashboardComponent implements OnInit {
  user_count=0;
  car_count=0;
  booking_count=0;
  active_user_count:number=0;
  in_active_user_count=0;
  total_amount=0;
  users_count=[];
  public chartType: string = 'doughnut';

  public chartDatasets: Array<any> = [
	  { data: [55,45], label: 'My First dataset' }
  ];
  public chartLabels: Array<any> = ['Active users', 'In active users'];

  public chartColors: Array<any> = [
	  {
		backgroundColor: ['rgba(244, 67, 54, 0.3)', 'rgba(3, 169, 244, 0.3)'],
		hoverBackgroundColor: ['rgba(244, 67, 54, 0.1)', 'rgba(3, 169, 244, 0.1)'],
		borderWidth: 1,
	  }
  ];

  public chartOptions: any = {
	  responsive: true
  };
  public chartClicked(e: any): void { }
  public chartHovered(e: any): void { }


  public barchartType: string = 'bar';

  public barchartDatasets: Array<any> = [
	  { data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], label: 'Cars' }
  ];

  public barchartLabels: Array<any> = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

  public barchartColors: Array<any> = [
  {
	backgroundColor: [
	  'rgba(255, 99, 132, 0.2)',
	  'rgba(54, 162, 235, 0.2)',
	  'rgba(255, 206, 86, 0.2)',
	  'rgba(75, 192, 192, 0.2)',
	  'rgba(153, 102, 255, 0.2)',
	  'rgba(255, 159, 64, 0.2)',
	  'rgba(255, 99, 132, 0.2)',
	  'rgba(54, 162, 235, 0.2)',
	  'rgba(255, 206, 86, 0.2)',
	  'rgba(75, 192, 192, 0.2)',
	  'rgba(153, 102, 255, 0.2)',
	  'rgba(255, 159, 64, 0.2)'
	],
	borderColor: [
	  'rgba(255,99,132,1)',
	  'rgba(54, 162, 235, 1)',
	  'rgba(255, 206, 86, 1)',
	  'rgba(75, 192, 192, 1)',
	  'rgba(153, 102, 255, 1)',
	  'rgba(255, 159, 64, 1)',
	  'rgba(255,99,132,1)',
	  'rgba(54, 162, 235, 1)',
	  'rgba(255, 206, 86, 1)',
	  'rgba(75, 192, 192, 1)',
	  'rgba(153, 102, 255, 1)',
	  'rgba(255, 159, 64, 1)'
	],
	borderWidth: 2,
  }
  ];

  public barchartOptions: any = {
	  responsive: true
  };
  public barchartClicked(e: any): void { }
  public barchartHovered(e: any): void { }
  constructor(public rest: RestService) { }

  ngOnInit() {
	  this.rest.post('get_dashboard_result', {}).subscribe((response) => {
		  if(response.status) {
			  this.user_count = response.data.user_count;
			  this.active_user_count = parseInt(response.data.active_user_count);
			  this.in_active_user_count = parseInt(response.data.in_active_user_count);
			  this.users_count.push(this.active_user_count, this.in_active_user_count);
			  this.chartDatasets = [
				{ data: this.users_count, label: ''}
			  ];
			  this.barchartDatasets = [
				{ data: response.data.yearly_car, label: 'Cars'}
			  ];
			  this.car_count = response.data.car_count;
			  this.total_amount = response.data.total_amount;
			  this.booking_count = response.data.partial_reservation_count;
		  }
	  })
  }
  

}
