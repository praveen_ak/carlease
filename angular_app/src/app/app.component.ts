import { Component, ChangeDetectorRef, OnDestroy, HostListener } from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';
import { CommonService } from './common.service';
import { Router, NavigationEnd, NavigationStart, NavigationCancel } from '@angular/router';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  mobileQuery: MediaQueryList;
  title = 'Carlease Admin';
  public currentUrl;
  public firstsegment;
  public secondsegment;
  public thirdsegment;
  public fourthsegment;
  public fifthsegment;
  public log_status;
  userLoggedin;
  user;
  private _mobileQueryListener: () => void;

  constructor(public router: Router, changeDetectorRef: ChangeDetectorRef, media: MediaMatcher, private commonservice: CommonService, private translate: TranslateService) {
    translate.setDefaultLang('en');
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }
  ngOnInit() {

    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.currentUrl = event.url;

        this.firstsegment = this.currentUrl.split('/')[1];
        this.secondsegment = this.currentUrl.split('/')[2];
        this.thirdsegment = this.currentUrl.split('/')[3];
        this.fourthsegment = this.currentUrl.split('/')[4];
        this.fifthsegment = this.currentUrl.split('/')[5];

        this.commonservice.urlFirstSegment = this.firstsegment;
        this.commonservice.urlSecondSegment = this.secondsegment;
        this.commonservice.urlThirdSegment = this.thirdsegment;
        this.commonservice.urlFourthSegment = this.fourthsegment;
        this.commonservice.urlFifthSegment = this.fifthsegment;
      }
    });
    this.commonservice.admin_login = localStorage.getItem('isAdminLogged');
    this.commonservice.user_login = localStorage.getItem('isUserLogged');
    this.userLoggedin = this.commonservice.user_login;
    if (localStorage.getItem('users')) {
      this.user = JSON.parse(localStorage.getItem('users'));
      this.commonservice.user = JSON.parse(localStorage.getItem('users'));
    }
  }
  ngAfterViewInit() {
    this.router.events.subscribe((event) => {
      this.userLoggedin = this.commonservice.user_login;
      if (localStorage.getItem('users')) {
        this.user = JSON.parse(localStorage.getItem('users'));
      }
    });
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }

	@HostListener('window:scroll', ['$event'])
	onWindowScroll(event) {
    var element = document.getElementsByClassName("top-nav-collapse")[0];
    if (element && window.pageYOffset > 50) {
      element.classList.add("white_header");
    }
    else {
      element.classList.remove("white_header");
    }
  }

  admin_logout() {
    this.commonservice.admin_login = 'false';
    localStorage.setItem('isAdminLogged', 'false');
    this.router.navigateByUrl('admin/login');
  }
  userLoggedOut() {
    this.commonservice.user_login = 'false';
    this.commonservice.user = '';
    this.userLoggedin = 'false';
    localStorage.setItem('isUserLogged', 'false');
    localStorage.setItem('users', '');
    this.router.navigateByUrl('login');
  }
  useLanguage(language: string) {
    console.log(language);
    this.translate.use(language);
  }
}
