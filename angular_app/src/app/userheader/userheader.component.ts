import { Component, OnInit } from '@angular/core';
import { CommonService } from '../common.service';
import { Router, NavigationEnd, NavigationStart, NavigationCancel } from '@angular/router';

@Component({
  selector: 'app-userheader',
  templateUrl: './userheader.component.html',
  styleUrls: ['./userheader.component.css']
})
export class UserheaderComponent implements OnInit {
  user_session;
  firstsegment;
  constructor(public commonservice: CommonService,public router: Router) { }

  ngOnInit() {
    this.user_session = this.commonservice.user;
    this.firstsegment = this.commonservice.getfirst_index();
  }

}
