import { Component, OnInit } from '@angular/core';
import { RestService } from '../rest.service';
import { CommonService } from '../common.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { Observable, Subject } from 'rxjs';
import { Options, LabelType } from 'ng5-slider';
@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  makes;
  term = 'all';
	manualRefresh = new Subject<void>();
	min_price: number = 0;
  max_price: number = 0;
  car_max_price;
	options: Options = {
		floor: 0,
		ceil: 0,
    step: 10,
		translate: (value: number, label: LabelType): string => {
			switch (label) {
				case LabelType.Low:
					return '$' + value;
				case LabelType.High:
					return '$' + value;
				default:
					return '$' + value;
			}
		}
  };
  searchresults;
	page = 1;
	current = 1;
	totalDocs = 0;
	totalPages = 0;
	startPage = 1;
	endPage = 1;
	pages: any;
	start_page: number = 0;
  end_page: number = 0;
  month;
  active_make;
  models = [];
  active_model;
  active_transmission;
  filter: Boolean = false;
  is_loading: Boolean = false;
  constructor(public rest: RestService, public commonservice: CommonService, public router: Router, public snackBar: MatSnackBar) {
    const navigation = this.router.getCurrentNavigation();
    const state = navigation.extras.state as {make_id: string};
    if(state) {
      this.active_make = state.make_id;
      this.get_search_results(this.page);
    }
  }

  ngOnInit() {
    this.rest.post('get_active_make', {}).subscribe((response) => {
      if(response.status) {
        this.makes = response.data.active_make;
      }
    })
		this.rest.post("get_max_price", {}).subscribe((response) => {
			this.max_price = this.car_max_price = response.data.max_price[0].price;
			this.options = {
				floor: 0,
				ceil: this.car_max_price,
				translate: (value: number, label: LabelType): string => {
					switch (label) {
						case LabelType.Low:
							return '$' + value;
						case LabelType.High:
							return '$' + value;
						default:
							return '$' + value;
					}
				}
			};
      this.get_search_results(this.page);
		})
  }
  get_search_results(page) {
    this.is_loading = true;
    let params = {
      'page': page,
      'term': this.term,
      'min_price': this.min_price,
      'max_price': this.max_price,
      'make': this.active_make,
      'model': this.active_model,
      'transmission': this.active_transmission
    }
    if(this.term == '1') {
      this.month = this.term + ' month';
    }
    else {
      this.month = this.term + ' months';
    }
    let data = JSON.stringify(params);
    this.rest.post('get_search_results',data).subscribe((response) => {
      if(response.status) {
        this.is_loading = false;
        this.searchresults = response.data.car_details.docs;

        // pagination
        this.start_page = response.data.car_details.pagingCounter;
        this.end_page = (response.data.car_details.pagingCounter - 1) + this.searchresults.length;
        this.current = response.data.car_details.page;
        this.totalDocs = response.data.car_details.totalDocs;
        this.totalPages = response.data.car_details.totalPages;
        if (this.totalPages <= 5) {
          this.startPage = 1;
          this.endPage = this.totalPages;
        }
        else {
          if (this.current <= 3) {
            this.startPage = 1;
            this.endPage = 5;
          } else if (this.current + 2 >= this.totalPages) {
            this.startPage = this.totalPages - 4;
            this.endPage = this.totalPages;
          } else {
            this.startPage = this.current - 2;
            this.endPage = this.current + 2;
          }
        }
        this.pages = Array.from(Array((this.endPage + 1) - this.startPage).keys()).map(i => this.startPage + i);
      }
    })
  }
  make_change(id) {
    if(this.active_make == id) {
      this.active_make = '';
      this.active_model = '';
    }
    else {
      this.active_make = id;
    }
    let params = {
      'make': this.active_make
    }
    let data = JSON.stringify(params);
    this.rest.post('get_model_by_make', data).subscribe((response) => {
      if(response.status) {
        this.models = response.data.get_models;
      }
      else {
        this.models = [];
      }
    })
    this.get_search_results(this.page)
  }
  model_change(model) {
    if(this.active_model == model) {
      this.active_model = '';
    }
    else {
      this.active_model = model;
    }
    this.get_search_results(this.page)
  }
  transmission_change(transmission) {
    if(this.active_transmission == transmission) {
      this.active_transmission = '';
    }
    else {
      this.active_transmission = transmission;
    }
    this.get_search_results(this.page)
  }
  open_filter() {
    this.filter = !this.filter;
  }
}
