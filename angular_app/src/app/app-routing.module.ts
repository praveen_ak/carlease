import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminauthGuard } from './adminauth.guard';
import { UserauthGuard } from './userauth.guard';

import { AdminLoginComponent } from './admin-login/admin-login.component';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { AdminCarComponent } from './admin-car/admin-car.component';
import { AdminCarUpdateComponent } from './admin-car-update/admin-car-update.component';
import { AdminImageCategoryComponent } from './admin-image-category/admin-image-category.component';
import { AdminUpdateImageCategoryComponent } from './admin-update-image-category/admin-update-image-category.component';
import { AdminMakeComponent } from './admin-make/admin-make.component';
import { AdminMakeUpdateComponent } from './admin-make-update/admin-make-update.component';
import { AdminUsersComponent } from './admin-users/admin-users.component';
import { AdminNotificationComponent } from './admin-notification/admin-notification.component';
import { AdminBookingComponent } from './admin-booking/admin-booking.component';

import { HomepageComponent } from './homepage/homepage.component';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CardetailsComponent } from './cardetails/cardetails.component';
import { SearchComponent } from './search/search.component';
import { EditprofileComponent } from './editprofile/editprofile.component';
import { MyleaseComponent } from './mylease/mylease.component';
const routes: Routes = [
  { path: '', component: HomepageComponent },
  { path: 'register', component: SignupComponent },
  { path: 'login', component: LoginComponent },
  { path: 'dashboard', component: DashboardComponent, canActivate: [UserauthGuard] },
  { path: 'users/edit', component: EditprofileComponent, canActivate: [UserauthGuard] },
  { path: 'lease', component: MyleaseComponent, canActivate: [UserauthGuard] },
  { path: 'car/:id', component: CardetailsComponent },
  { path: 'search', component: SearchComponent},
  { path: 'admin/login', component: AdminLoginComponent },
  { path: 'admin/dashboard', component: AdminDashboardComponent, canActivate: [AdminauthGuard] },
  { path: 'admin/car', component: AdminCarComponent, canActivate: [AdminauthGuard] },
  { path: 'admin/car/add', component: AdminCarUpdateComponent, canActivate: [AdminauthGuard] },
  { path: 'admin/car/edit/:id', component: AdminCarUpdateComponent, canActivate: [AdminauthGuard] },
  { path: 'admin/image_category', component: AdminImageCategoryComponent, canActivate: [AdminauthGuard] },
  { path: 'admin/image_category/add', component: AdminUpdateImageCategoryComponent, canActivate: [AdminauthGuard] },
  { path: 'admin/image_category/edit/:id', component: AdminUpdateImageCategoryComponent, canActivate: [AdminauthGuard] },
  { path: 'admin/make', component: AdminMakeComponent, canActivate: [AdminauthGuard] },
  { path: 'admin/make/add', component: AdminMakeUpdateComponent, canActivate: [AdminauthGuard] },
  { path: 'admin/make/edit/:id', component: AdminMakeUpdateComponent, canActivate: [AdminauthGuard] },
  { path: 'admin/users', component: AdminUsersComponent, canActivate: [AdminauthGuard] },
  { path: 'admin/notifications', component: AdminNotificationComponent, canActivate: [AdminauthGuard] },
  { path: 'admin/bookings', component: AdminBookingComponent, canActivate: [AdminauthGuard] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {scrollPositionRestoration: 'enabled'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
