import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {MatSnackBar} from '@angular/material';
import { RestService } from '../rest.service';
import { CommonService } from '../common.service'
import { MatProgressButtonOptions } from 'mat-progress-buttons';

@Component({
  selector: 'app-admin-make-update',
  templateUrl: './admin-make-update.component.html',
  styleUrls: ['./admin-make-update.component.css']
})
export class AdminMakeUpdateComponent implements OnInit {

  btnOpts: MatProgressButtonOptions = {
		active: false,
		text: 'Submit',
		spinnerSize: 18,
		raised: true,
		stroked: false,
		spinnerColor: 'accent',
		fullWidth: false,
		disabled: false,
		mode: 'indeterminate',
	};
  name="";
  status="active";
  id="";
  returnUrl: string;
  page_title: String;
  showForm:Boolean = true;
  thispageLoad:Boolean = false;
  thisEditpage:Boolean = false;
  selectedFile: File;
  imageSrc;

  constructor(public rest:RestService,public router: Router,public snackBar: MatSnackBar,public commonservice: CommonService) { }

  ngOnInit() {
    this.returnUrl = 'admin/make';
    this.page_title = "Add Make";
    if(this.commonservice.getthird_index()=="edit")
    {
      this.page_title = "Update Make";
      this.thisEditpage=true;
      this.thispageLoad=true;
      let params={
        'id' : this.commonservice.urlFourthSegment
      };
      let data=JSON.stringify(params);
      this.rest.post('get_make_detail',data).subscribe((response) => {
        if(response.status)
        {
          this.showForm=true;
          this.name=response.data.admin_image_category.name;
          this.status=response.data.admin_image_category.status;
          this.imageSrc = response.data.admin_image_category.makephoto;
          this.id=response.data.admin_image_category.id;
        }
        else
        {
          this.showForm=false;
        }
        this.thispageLoad=false;
      });
    }
  }

	makePhotoUpload(event) {
		this.selectedFile = event.target.files[0];
	    if (event.target.files && event.target.files[0]) {
	        const file = event.target.files[0];

	        const reader = new FileReader();
	        reader.onload = e => this.imageSrc = reader.result;

	        reader.readAsDataURL(file);
	    }
  }
  
  update_make()
  {
  	if(this.name=="")
  	{
  	  this.snackBar.open('Enter a make name','',{
  	    duration: 2000,
  	    panelClass: ['orange-text']
  	  });
  	}
  	else if(this.selectedFile==undefined && this.thisEditpage == false)
  	{
  	  this.snackBar.open('Upload make image','',{
  	    duration: 2000,
  	    panelClass: ['orange-text']
  	  });
  	}
  	else
  	{
      this.btnOpts.active = true;
      this.btnOpts.text = "Saving...";
      const uploadData = new FormData();
      if(this.selectedFile!=undefined) 
        uploadData.append('media', this.selectedFile, this.selectedFile.name);
      uploadData.append('name', this.name);
      uploadData.append('status', this.status);
      uploadData.append('is_enable', 'true');
      uploadData.append('id', this.id);
   
      let data=uploadData;
      let params = {
        'type': this.thisEditpage
      }
      this.rest.upload('add_make',data,params).subscribe((response) => {
        if(response.status)
  			{
          this.btnOpts.active = false;
          this.btnOpts.text = "Submit";
  				this.snackBar.open(response.message,'',{
  					duration: 2000,
  					panelClass: ['green-text']
  				});
  				this.router.navigateByUrl(this.returnUrl);
  			}
  			else
  			{
  				this.snackBar.open(response.message,'',{
  					duration: 2000,
  					panelClass: ['orange-text']
  				});
  			}
      });
  	} 
  }

}
