import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminMakeUpdateComponent } from './admin-make-update.component';

describe('AdminMakeUpdateComponent', () => {
  let component: AdminMakeUpdateComponent;
  let fixture: ComponentFixture<AdminMakeUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminMakeUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminMakeUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
