import { Component, OnInit } from '@angular/core';
import { RestService } from '../rest.service';
import { CommonService } from '../common.service';
import { Router, NavigationExtras } from '@angular/router';
import { OwlOptions } from 'ngx-owl-carousel-o';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {
  customOptions: OwlOptions = {
    loop: true,
    mouseDrag: true,
    touchDrag: false,
    pullDrag: true,
    dots: false,
    navSpeed: 700,
    navText: ["<i class='material-icons'>chevron_left</i>", "<i class='material-icons'>chevron_right</i>"],
		margin: 20,
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 2
      },
      740: {
        items: 3
      },
      940: {
        items: 4
      }
    },
    nav: true
  }
  makes;
  cars=[];
  constructor(public rest: RestService, public commonservice: CommonService, public router: Router) { }

  ngOnInit() {
    this.rest.post('get_active_make', {}).subscribe((response) => {
      if(response.status) {
        this.makes = response.data.active_make;
      }
    })
    this.rest.post('get_latest_cars', {}).subscribe((response) => {
      if(response.status) {
        this.cars = response.data.car;
        // console.log(this.cars);
      }
    })
  }
  goto_search(make) {
    const navigationExtras: NavigationExtras = {state: {make_id: make._id}};
    this.router.navigate(['search'], navigationExtras);
  }

}
