import { Component, OnInit } from '@angular/core';
import { RestService } from '../rest.service';
import {MatCalendar, DateAdapter} from '@angular/material';

@Component({
  selector: 'app-admin-notification',
  templateUrl: './admin-notification.component.html',
  styleUrls: ['./admin-notification.component.css']
})
export class AdminNotificationComponent<D> implements OnInit {
	minDate: D = this._dateAdapter.today();
  notifications;
	page = 1;
	current = 1;
	totalDocs= 0;
	totalPages= 0;
	startPage=1;
	endPage=1;
  pages:any;
	_startdate = '';
	_enddate = '';
  constructor(private _dateAdapter: DateAdapter<D>,public rest: RestService) { }

  ngOnInit() {
    this.get_notifications(this.page);
  }
  get_notifications(page) {
    let params = {
      'page': page,
      'start_date': this._startdate,
      'end_date': this._enddate
    }
    let data = JSON.stringify(params);
    this.rest.post('get_admin_notifications',data).subscribe((response) => {
      if(response.status) {
        this.notifications = response.data.notification.docs;
        this.current = response.data.notification.page;
        this.totalDocs = response.data.notification.totalDocs;
        this.totalPages = response.data.notification.totalPages;
        if (this.totalPages <= 5) {
          this.startPage = 1;
          this.endPage = this.totalPages;
        }
        else {
          if (this.current <= 3) {
            this.startPage = 1;
            this.endPage = 5;
          }
          else if (this.current + 2 >= this.totalPages) {
            this.startPage = this.totalPages - 4;
            this.endPage = this.totalPages;
          }
          else {
            this.startPage = this.current - 2;
            this.endPage = this.current + 2;
          }
        }
        if (this.pages && this.pages.length > 1) {
          this.pages = Array.from(Array((this.endPage + 1) - this.startPage).keys()).map(i => this.startPage + i);
        }
      }
    })
  }
}
