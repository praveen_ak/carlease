(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/admin-car-update/admin-car-update.component.css":
/*!*****************************************************************!*\
  !*** ./src/app/admin-car-update/admin-car-update.component.css ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhbmd1bGFyX2FwcC9zcmMvYXBwL2FkbWluLWNhci11cGRhdGUvYWRtaW4tY2FyLXVwZGF0ZS5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/admin-car-update/admin-car-update.component.html":
/*!******************************************************************!*\
  !*** ./src/app/admin-car-update/admin-car-update.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"col-12 p-0 mt-3\" [ngClass]=\"{h100:thispageLoad}\">\n  <div class=\"container col-md-11 mx-auto p-3\" [ngClass]=\"{h100:thispageLoad}\">\n    <div class=\"d-flex justify-content-center align-items-center h-100\" *ngIf=\"thispageLoad\">\n      <mat-progress-spinner [color]=\"spinner_color\" [mode]=\"spinner_mode\" [diameter]=50></mat-progress-spinner>\n    </div>\n    <div class=\"col-12  white z-depth-1\">\n      <div class=\"d-flex align-items-center f-wrap\">\n        <h4 class=\"bold_font w-100 mb-0 col-md-7 col-sm-5 my-3 pl-0\">{{page_title}}</h4>\n      </div>\n      <div class=\"col-12 p-0\">\n        <div class=\"d-flex row mb-3 pb-3\">\n          <mat-horizontal-stepper [linear]=\"false\" #stepper class=\"w-100\" [ngClass]=\"thisEditpage ? '' : 'disable_header'\">\n            <mat-step>\n                <ng-template matStepLabel>General info</ng-template>\n                <div class=\"col-12 row mx-0 my-4 p-0\">\n                    <div class=\"col-sm-6\">\n                        <mat-form-field class=\"example-full-width w-100\" appearance=\"outline\">\n                          <mat-label class=\"font-9\">Pick one Make</mat-label>\n                          <input class=\"font-9\" type=\"text\" placeholder=\"Pick one Make\" aria-label=\"make\" matInput [formControl]=\"myControl\" [matAutocomplete]=\"auto\" [(ngModel)]=\"car_form_value['make']\" autocomplete=\"off\">\n                          <mat-autocomplete autoActiveFirstOption #auto=\"matAutocomplete\">\n                            <mat-option *ngFor=\"let option of filter_make | async\" [value]=\"option\">\n                              {{option}}\n                            </mat-option>\n                          </mat-autocomplete>\n                        </mat-form-field>\n                    </div>\n                    <div class=\"col-sm-6\">\n                        <mat-form-field class=\"example-full-width w-100\" appearance=\"outline\">\n                          <mat-label class=\"font-9\">Enter Model</mat-label>\n                          <input matInput placeholder=\"Enter Model\" autocomplete=\"off\" [(ngModel)]=\"car_form_value['model']\">\n                        </mat-form-field>\n                    </div>\n                    <div class=\"col-sm-6\">\n                        <mat-form-field class=\"example-full-width w-100\" appearance=\"outline\">\n                          <mat-label class=\"font-9\">Pick one Transmission Type</mat-label>\n                          <mat-select class=\"font-9\" placeholder=\"Pick one Transmission Type\" [(ngModel)]=\"car_form_value['transmission_type']\">\n                            <mat-option value=\"automatic\">Automatic</mat-option>\n                            <mat-option value=\"manual\">Manual</mat-option>\n                            <mat-option value=\"semi\">Semi Automatic</mat-option>\n                          </mat-select>\n                        </mat-form-field>\n                    </div>\n                    <div class=\"col-sm-6\">\n                        <mat-form-field class=\"example-full-width w-100\" appearance=\"outline\">\n                          <mat-label class=\"font-9\">Pick one Fuel Type</mat-label>\n                          <mat-select class=\"font-9\" placeholder=\"Pick one Fuel Type\" [(ngModel)]=\"car_form_value['fuel_type']\">\n                            <mat-option value=\"petrol\">Petrol</mat-option>\n                            <mat-option value=\"diesel\">Diesel</mat-option>\n                            <mat-option value=\"electric\">Electric</mat-option>\n                          </mat-select>\n                        </mat-form-field>\n                    </div>\n                    <div class=\"col-sm-6\">\n                        <mat-form-field class=\"example-full-width w-100\" appearance=\"outline\">\n                          <mat-label class=\"font-9\">Pick one Body Type</mat-label>\n                          <mat-select class=\"font-9\" placeholder=\"Pick one Body Type\" [(ngModel)]=\"car_form_value['body_type']\">\n                            <mat-option value=\"hatchback\">Hatchback</mat-option>\n                            <mat-option value=\"sedan\">Sedan</mat-option>\n                            <mat-option value=\"muv/suv\">MUV/SUV</mat-option>\n                            <mat-option value=\"coupe\">Coupe</mat-option>\n                            <mat-option value=\"convertible\">Convertible</mat-option>\n                            <mat-option value=\"wagon\">Wagon</mat-option>\n                            <mat-option value=\"van\">Van</mat-option>\n                            <mat-option value=\"jeep\">Jeep</mat-option>\n                          </mat-select>\n                        </mat-form-field>\n                    </div>\n                    <div class=\"col-sm-6\">\n                        <mat-form-field class=\"example-full-width w-100\" appearance=\"outline\">\n                            <mat-label class=\"font-9\">Enter Seatings</mat-label>\n                            <input matInput placeholder=\"Enter Seatings\" autocomplete=\"off\" [(ngModel)]=\"car_form_value['seat']\" (keypress)=\"numberOnly($event)\">\n                        </mat-form-field>\n                    </div>\n                    <div class=\"col-sm-6\">\n                        <mat-form-field class=\"example-full-width w-100\" appearance=\"outline\">\n                            <mat-label class=\"font-9\">Enter Engine CC</mat-label>\n                            <input matInput placeholder=\"Enter Engine CC\" autocomplete=\"off\" [(ngModel)]=\"car_form_value['engine_cc']\" (keypress)=\"numberOnly($event)\">\n                        </mat-form-field>\n                    </div>\n                    <div class=\"col-sm-6\">\n                        <mat-form-field class=\"example-full-width w-100\" appearance=\"outline\">\n                            <mat-label class=\"font-9\">Enter BHP</mat-label>\n                            <input matInput placeholder=\"Enter BHP\" autocomplete=\"off\" [(ngModel)]=\"car_form_value['bhp']\" (keypress)=\"numberOnly($event)\">\n                        </mat-form-field>\n                    </div>\n                    <div class=\"col-sm-6\">\n                        <mat-form-field class=\"example-full-width w-100\" appearance=\"outline\">\n                            <mat-label class=\"font-9\">Enter Km</mat-label>\n                            <input matInput placeholder=\"Enter Km\" autocomplete=\"off\" [(ngModel)]=\"car_form_value['kilometer']\" (keypress)=\"numberOnly($event)\">\n                        </mat-form-field>\n                    </div>\n                    <div class=\"col-sm-6\">\n                        <mat-form-field class=\"example-full-width w-100\" appearance=\"outline\">\n                            <mat-label class=\"font-9\">Enter Fuel Capacity</mat-label>\n                            <input matInput placeholder=\"Enter Fuel Capacity\" autocomplete=\"off\" [(ngModel)]=\"car_form_value['fuel_capacity']\" (keypress)=\"numberOnly($event)\">\n                        </mat-form-field>\n                    </div>\n                </div>\n                <div class=\"text-right col-12\">\n                    <mat-spinner-button class=\"btn orange darken-3 l-h-normal ml-auto p-3 m-0 rounded\"\n                    (btnClick)=\"goForward('general')\" [options]=\"btnOpts\"></mat-spinner-button>\n                </div>\n            </mat-step>\n            <mat-step>\n                <ng-template matStepLabel>Pricing info</ng-template>\n                <div class=\"col-12 row mx-0 p-0 my-4\">\n                    <div class=\"p-2 w-md-20\" *ngFor='let subscription_name of subscription_names;let i = index'>\n                      <div class=\"card\">\n                          <div class=\"view overlay\">\n                            <img class=\"card-img-top\" src=\"angular_app/carlease/assets/images/leasing.jpg\" alt=\"leasing\">\n                            <a href=\"#!\">\n                              <div class=\"mask rgba-white-slight\"></div>\n                            </a>\n                          </div>\n                          <div class=\"card-body\">\n                            <h4 class=\"card-title bold_font font-1\">{{subscription_name.name}} plan</h4>\n                            <mat-form-field class=\"example-full-width w-100\" appearance=\"outline\">\n                                <mat-label class=\"font-9\">Enter amount for {{subscription_name.name}}</mat-label>\n                                <input matInput placeholder=\"Enter amount for {{subscription_name.name}}\" class=\"font-9\" autocomplete=\"off\" [(ngModel)]=\"subscription_price[i]\">\n                                <span matPrefix class=\"custom_prefix font-9\">$&nbsp;</span>\n                            </mat-form-field>                   \n                          </div>                      \n                      </div>\n                    </div>\n                </div>\n                <div class=\"text-right col-12\">\n                    <button mat-button class=\"btn grey lighten-3 mr-3 l-h-normal ml-auto p-3 m-0 rounded\" (click)=\"goBack()\">Back</button>\n                    <mat-spinner-button class=\"btn orange darken-3 l-h-normal ml-auto p-3 m-0 rounded\"\n                    (btnClick)=\"goForward('pricing')\" [options]=\"btnOpts\"></mat-spinner-button>\n                </div>\n            </mat-step>\n            <mat-step>\n                <ng-template matStepLabel>Upload Photo</ng-template>\n                <div class=\"col-12 row my-4 mx-0 p-0\">\n                    <div class=\"col-12 col-lg-3 col-sm-6 mb-3\" *ngFor=\"let car_image of car_images; let i = index\">\n                      <div class=\"position-relative car_image\">\n                        <span class=\"img-trash position-absolute l-0\"><a class=\"d-inline-block m-2\" (click)=\"delete_photo(car_image)\"><i class=\"material-icons rounded-circle white-text rgba-black-light p-2 font-14\">delete</i></a></span>\n                        <span class=\"img-settings position-absolute r-0 h-100\" [id]=\"'category_' + i\">\n                          <a class=\"d-inline-block my-2\" (click)=\"showcategory('category_' + i)\">\n                            <i class=\"material-icons white rgba-stylish-light p-1 font-1 primary-text-color rounded radius_left\">settings</i>\n                          </a>\n                          <span class=\"categories invisible r-0 rgba-stylish-strong position-absolute h-100 w-100 mw-200 o-y-auto\"> \n                            <mat-radio-group class=\"white-text pt-2\" ngDefaultControl [(ngModel)]=\"car_image.image_category\">\n                                <mat-radio-button *ngFor=\"let category of categories\" class=\"pt-2 px-3 bold_font\" value=\"{{category.name}}\" (change)=\"assignCatgory(category,car_image.id)\">{{category.name}}</mat-radio-button> \n                            </mat-radio-group>\n                          </span>\n                        </span>\n                        <span class=\"position-absolute r-0 b-0 rounded-btn white-text amber lighten-1 py-1 px-2  bold_font font-8 m-2 l-h-15\" *ngIf=\"car_image.is_featured==1\">Cover Photo</span>\n                        <img src=\"{{car_image.car_photo}}\" class=\"w-100 radius_30 h-200 o-cover\">\n                      </div>\n                    </div>\n                    <div [ngClass]=\"[before_carimages.length == 0 ? 'd-none' : 'd-block']\" class=\"col-12 col-lg-3 col-sm-6 mb-3 pointer-events-none\" *ngFor=\"let before_carimage of before_carimages; let i = index\">\n                      <div class=\"position-relative listimage\">\n                          <img src=\"{{before_carimage.car_photo}}\" class=\"w-100 blur_img radius_30 h-200 o-cover\">\n                          <span class=\"position-absolute r-0 p-3 t-0\" *ngIf=\"loadingStatus\">\n                            <mat-spinner class=\"white_spinner\" [color]=\"spinner_color\" [mode]=\"spinner_mode\" [diameter]=\"spinner_value\"></mat-spinner>\n                          </span>\n                      </div>\n                    </div>\n                    <ngx-file-drop class=\"col-lg-3 col-sm-6 text-center font-9 mb-3\" (onFileDrop)=\"dropped($event)\" id=\"upload_photos\">\n                        <ng-template ngx-file-drop-content-tmp let-openFileSelector=\"openFileSelector\">\n                            <span class=\"pointer-events-none d-block grey-text\">Drag files here or </span>\n                            <button type=\"button\" class=\"primary-text-color outline-0 d-inline-block bg-transparent border-0\" (click)=\"openFileSelector()\">Browse Files</button>\n                            <span class=\"d-inline-block grey-text\">to upload.</span>\n                        </ng-template>\n                    </ngx-file-drop>\n                </div>\n                <div class=\"text-right col-12\">\n                    <button mat-button class=\"btn grey lighten-3 mr-3 l-h-normal ml-auto p-3 m-0 rounded\" (click)=\"goBack()\">Back</button>\n                    <button mat-button class=\"btn white-text orange darken-3 l-h-normal ml-auto p-3 m-0 rounded\" (click)=\"goForward('photo')\">Next</button>\n                </div>\n            </mat-step>\n            <mat-step>\n              <ng-template matStepLabel>Color Variants</ng-template>\n              <div class=\"col-12 row my-4 mx-0 p-0\">\n                <table mat-table [dataSource]=\"extra_color_price\" class=\"mat-elevation-z0 w-100 mb-4\" *ngIf=\"extra_color_price?.length > 0\">\n                    <ng-container matColumnDef=\"color_name\">\n                      <th mat-header-cell *matHeaderCellDef class=\"head grey lighten-4 sub-text-color\"> Color code </th>\n                      <td data-title=\"Color code\" mat-cell *matCellDef=\"let element\"\n                        class=\"py-2 align-items-center dark-text-color border-0\">\n                        <p class=\"mb-1 dark-text-color d-flex align-items-center\"><span class=\"border-1 w-20x h-20x mr-2\" [ngStyle]=\"{'background-color': element.color_code}\"></span>{{element.color_code}}</p>\n                      </td>\n                    </ng-container>\n                    <ng-container matColumnDef=\"price\">\n                      <th mat-header-cell *matHeaderCellDef class=\"head grey lighten-4 sub-text-color\"> Amount </th>\n                      <td data-title=\"Amount\" mat-cell *matCellDef=\"let element\"\n                        class=\"py-2 align-items-center dark-text-color border-0\">\n                        <p class=\"mb-1\">{{element.price}}</p>\n                      </td>\n                    </ng-container>\n                    <ng-container matColumnDef=\"action\">\n                      <th mat-header-cell *matHeaderCellDef class=\"head grey lighten-4 sub-text-color\"> Action </th>\n                      <td data-title=\"Action\" class=\"py-2 border-0\" mat-cell *matCellDef=\"let element\">\n                        <button type=\"button\" matTooltip=\"Delete\" matTooltipPosition=\"above\"\n                          class=\"p-0 outline-0 bold_font m-w-auto\" (click)=\"delete_color_price(element)\"\n                          mat-button><i class=\"material-icons primary-text-color font-11\">delete</i></button>\n                      </td>\n                    </ng-container>\n                    <tr mat-header-row class=\"border-top\" *matHeaderRowDef=\"displayedColumns\">\n                      <td>asdf</td>\n                    </tr>\n                    <tr class=\"border-bottom\" mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\n                </table>\n                <div class=\"col-md-6 col-lg-3\">\n                  <h6 class=\"bold_font\">Pick a color</h6>\n                  <color-sketch [color]=\"state\" (onChangeComplete)=\"changeComplete($event)\"></color-sketch>\n                </div>\n                <div class=\"col-md-6 col-lg-4\">\n                  <h6 class=\"bold_font\">Enter extra price for 1 month to specific color</h6>\n                  <mat-form-field class=\"example-full-width w-100\" appearance=\"outline\">\n                      <mat-label class=\"font-9\">Amount</mat-label>\n                      <input matInput placeholder=\"Amount\" autocomplete=\"off\" (keypress)=\"numberOnly($event)\" [(ngModel)]=\"color_price\">\n                  </mat-form-field>\n                  <mat-spinner-button class=\"btn orange darken-3 l-h-normal ml-auto p-3 m-0 rounded\"\n                    (btnClick)=\"add_color_price()\" [options]=\"btnExtraOpts\"></mat-spinner-button>\n                </div>\n              </div>\n              <div class=\"text-right col-12\">\n                  <button mat-button class=\"btn grey lighten-3 mr-3 l-h-normal ml-auto p-3 m-0 rounded\" (click)=\"goBack()\">Back</button>\n                  <button mat-button class=\"btn white-text orange darken-3 l-h-normal ml-auto p-3 m-0 rounded\" (click)=\"goForward()\">Next</button>\n              </div>\n            </mat-step>\n          </mat-horizontal-stepper>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/admin-car-update/admin-car-update.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/admin-car-update/admin-car-update.component.ts ***!
  \****************************************************************/
/*! exports provided: AdminCarUpdateComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminCarUpdateComponent", function() { return AdminCarUpdateComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "../node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "../node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _angular_material_stepper__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/stepper */ "../node_modules/@angular/material/esm5/stepper.es5.js");
/* harmony import */ var _rest_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../rest.service */ "./src/app/rest.service.ts");
/* harmony import */ var _common_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../common.service */ "./src/app/common.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material */ "../node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var ng2_img_max__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ng2-img-max */ "../node_modules/ng2-img-max/dist/ng2-img-max.js");









var extra_price = [];
var AdminCarUpdateComponent = /** @class */ (function () {
    function AdminCarUpdateComponent(rest, commonservice, ng2ImgMax, snackBar) {
        this.rest = rest;
        this.commonservice = commonservice;
        this.ng2ImgMax = ng2ImgMax;
        this.snackBar = snackBar;
        this.btnOpts = {
            active: false,
            text: 'Next',
            spinnerSize: 18,
            raised: true,
            stroked: false,
            spinnerColor: 'accent',
            fullWidth: false,
            disabled: false,
            mode: 'indeterminate',
        };
        this.btnExtraOpts = {
            active: false,
            text: 'Add',
            spinnerSize: 18,
            raised: true,
            stroked: false,
            spinnerColor: 'accent',
            fullWidth: false,
            disabled: false,
            mode: 'indeterminate',
        };
        this.status = "active";
        this.id = "";
        this.thispageLoad = false;
        this.thisEditpage = false;
        this.myControl = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]();
        this.state = '#000';
        this.car_form_value = {};
        this.subscription_price = [];
        this.subscription_names = [];
        this.subscription_prices_name_value = [];
        this.before_carimages = [];
        this.carImages = [];
        this.extra_color_price = [];
        this.spinner_color = 'warn';
        this.spinner_mode = 'indeterminate';
        this.spinner_value = 20;
        this.displayedColumns = ['color_name', 'price', 'action'];
        this.files = [];
    }
    AdminCarUpdateComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.returnUrl = 'admin/make';
        this.page_title = "Add your Car";
        if (this.commonservice.getthird_index() == "edit") {
            this.page_title = "Update your Car";
            this.thisEditpage = true;
            this.thispageLoad = true;
            var params = {
                'id': this.commonservice.urlFourthSegment
            };
            var data = JSON.stringify(params);
            this.rest.post('get_car_detail', data).subscribe(function (response) {
                if (response.status) {
                    _this.car_form_value['make'] = response.data.admin_car.make_title.name;
                    _this.car_form_value['model'] = response.data.admin_car.model;
                    _this.car_form_value['transmission_type'] = response.data.admin_car.transmission_type;
                    _this.car_form_value['fuel_type'] = response.data.admin_car.fuel_type;
                    _this.car_form_value['body_type'] = response.data.admin_car.body_type;
                    _this.car_form_value['seat'] = response.data.admin_car.seat;
                    _this.car_form_value['engine_cc'] = response.data.admin_car.engine_capacity;
                    _this.car_form_value['bhp'] = response.data.admin_car.bhp;
                    _this.car_form_value['kilometer'] = response.data.admin_car.mileage;
                    _this.car_form_value['fuel_capacity'] = response.data.admin_car.fuel_capacity;
                    _this.extra_color_price = response.data.admin_car.extra_price;
                    for (var i = 0; i < response.data.admin_car.price.length; i++) {
                        _this.subscription_price[i] = response.data.admin_car.price[i].price;
                    }
                    _this.status = response.data.admin_car.status;
                    _this.id = response.data.admin_car.id;
                    _this.getCarPhoto();
                }
                else {
                }
                _this.thispageLoad = false;
            });
        }
        this.rest.post('get_active_make', {}).subscribe(function (response) {
            if (response.status) {
                _this.active_make = response.data.active_make;
                _this.make = response.data.make_group;
                _this.filter_make = _this.myControl.valueChanges.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["startWith"])(''), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (value) { return _this._filter(value); }));
            }
            _this.subscription_names = [
                {
                    id: 1,
                    name: '1 month'
                },
                {
                    id: 2,
                    name: '3 months'
                },
                {
                    id: 3,
                    name: '6 months'
                },
                {
                    id: 4,
                    name: '12 months'
                },
                {
                    id: 5,
                    name: '18 months'
                },
            ];
        });
    };
    AdminCarUpdateComponent.prototype._filter = function (value) {
        var filterValue = value.toLowerCase();
        return this.make.filter(function (option) { return option.toLowerCase().indexOf(filterValue) === 0; });
    };
    AdminCarUpdateComponent.prototype.showcategory = function (id) {
        var element = document.getElementById(id);
        element.classList.contains('active') ?
            element.classList.remove('active') :
            element.classList.add('active');
    };
    AdminCarUpdateComponent.prototype.assignCatgory = function (category, image_id) {
        var _this = this;
        var params = {
            "image_category": category.name,
            "id": image_id
        };
        var data = JSON.stringify(params);
        this.rest.post('update_catgory', data).subscribe(function (response) {
            if (response.success) {
                _this.snackBar.open(response.message, '', {
                    duration: 2000,
                    panelClass: ['green-text']
                });
            }
            else {
                _this.snackBar.open(response.message, '', {
                    duration: 2000,
                    panelClass: ['orange-text']
                });
            }
        });
    };
    AdminCarUpdateComponent.prototype.getCarPhoto = function () {
        var _this = this;
        var params = {
            'car_id': this.id
        };
        var data = JSON.stringify(params);
        this.rest.post('get_car_photo', data).subscribe(function (response) {
            _this.car_images = response.data.photoDetails;
            _this.categories = response.data.imagecatgoryDetails;
        });
    };
    AdminCarUpdateComponent.prototype.goBack = function () {
        this.myStepper.previous();
    };
    AdminCarUpdateComponent.prototype.goForward = function (type) {
        var _this = this;
        var is_error;
        var msg;
        if (type == 'general') {
            var make_id = this.searchMake(this.car_form_value['make'], this.active_make);
            if (this.car_form_value['make'] == undefined || this.car_form_value['make'] == '' || make_id == undefined) {
                is_error = true;
                msg = "Pick one make";
            }
            else if (this.car_form_value['model'] == undefined || this.car_form_value['model'] == '') {
                is_error = true;
                msg = "Enter model name";
            }
            else if (this.car_form_value['transmission_type'] == undefined || this.car_form_value['transmission_type'] == '') {
                is_error = true;
                msg = "Pick one transmission type";
            }
            else if (this.car_form_value['fuel_type'] == undefined || this.car_form_value['fuel_type'] == '') {
                is_error = true;
                msg = "Pick one fuel type";
            }
            else if (this.car_form_value['body_type'] == undefined || this.car_form_value['body_type'] == '') {
                is_error = true;
                msg = "Pick one body type";
            }
            else if (this.car_form_value['seat'] == undefined || this.car_form_value['seat'] == '') {
                is_error = true;
                msg = "Enter the seating value";
            }
            else if (this.car_form_value['engine_cc'] == undefined || this.car_form_value['engine_cc'] == '') {
                is_error = true;
                msg = "Enter the Engine CC";
            }
            else if (this.car_form_value['bhp'] == undefined || this.car_form_value['bhp'] == '') {
                is_error = true;
                msg = "Enter the BHP";
            }
            else if (this.car_form_value['kilometer'] == undefined || this.car_form_value['kilometer'] == '') {
                is_error = true;
                msg = "Enter the kilometer";
            }
            else if (this.car_form_value['fuel_capacity'] == undefined || this.car_form_value['fuel_capacity'] == '') {
                is_error = true;
                msg = "Enter the fuel capcity";
            }
            if (is_error) {
                this.snackBar.open(msg, '', {
                    panelClass: 'orange-text',
                    duration: 2000
                });
            }
            else {
                this.btnOpts.active = true;
                this.btnOpts.text = 'Saving...';
                var itype = 'add';
                if (this.id != '') {
                    itype = 'edit';
                }
                var params = {
                    'type': itype,
                    'make_title': make_id,
                    'model': this.car_form_value['model'],
                    'transmission_type': this.car_form_value['transmission_type'],
                    'fuel_type': this.car_form_value['fuel_type'],
                    'body_type': this.car_form_value['body_type'],
                    'seat': this.car_form_value['seat'],
                    'engine_capacity': this.car_form_value['engine_cc'],
                    'bhp': this.car_form_value['bhp'],
                    'mileage': this.car_form_value['kilometer'],
                    'fuel_capacity': this.car_form_value['fuel_capacity'],
                    'status': false,
                    'is_enable': true,
                    'carid': this.id
                };
                var data = JSON.stringify(params);
                this.rest.post('add_car', data).subscribe(function (response) {
                    _this.btnOpts.active = false;
                    _this.btnOpts.text = 'Next';
                    if (response.status) {
                        _this.id = response.data.admin_car.id;
                        _this.snackBar.open(response.message, '', {
                            panelClass: 'green-text',
                            duration: 2000
                        });
                        _this.myStepper.next();
                    }
                    else {
                        _this.snackBar.open(response.message, '', {
                            panelClass: 'orange-text',
                            duration: 2000
                        });
                    }
                });
            }
        }
        else if (type == 'pricing') {
            this.subscription_prices_name_value = [];
            for (var k = 0; k < this.subscription_names.length; k++) {
                this.subscription_prices_name_value.push({ 'duration': this.subscription_names[k].name, 'price': this.subscription_price[k] });
            }
            for (var k = 0; k < this.subscription_names.length; k++) {
                if (this.subscription_price[k] == '' || this.subscription_price[k] == undefined) {
                    is_error = true;
                    msg = "Enter the price";
                }
                else {
                    is_error = false;
                    break;
                }
            }
            if (is_error) {
                this.snackBar.open(msg, '', {
                    panelClass: 'orange-text',
                    duration: 2000
                });
            }
            else {
                this.btnOpts.active = true;
                this.btnOpts.text = 'Saving...';
                if (this.id != '') {
                    itype = 'edit';
                }
                var params = {
                    'price': this.subscription_prices_name_value,
                    'type': itype,
                    'carid': this.id
                };
                var data = JSON.stringify(params);
                this.rest.post('add_car', data).subscribe(function (response) {
                    _this.btnOpts.active = false;
                    _this.btnOpts.text = 'Next';
                    if (response.status) {
                        _this.myStepper.next();
                        _this.snackBar.open(response.message, '', {
                            panelClass: 'green-text',
                            duration: 2000
                        });
                    }
                    else {
                        _this.snackBar.open(response.message, '', {
                            panelClass: 'orange-text',
                            duration: 2000
                        });
                    }
                });
            }
        }
        else if (type == 'photo') {
            if (this.car_images.length == 0) {
                this.snackBar.open("Upload minimum one image", '', {
                    panelClass: 'orange-text',
                    duration: 2000
                });
            }
            else {
                this.myStepper.next();
            }
        }
    };
    AdminCarUpdateComponent.prototype.searchMake = function (nameKey, myArray) {
        for (var i = 0; i < myArray.length; i++) {
            if (myArray[i].name === nameKey) {
                return myArray[i].id;
            }
        }
    };
    AdminCarUpdateComponent.prototype.isFileAllowed = function (fileName) {
        var isFileAllowed = false;
        var not_upload = false;
        var allowedFiles = ['.png', '.jpeg', '.jpg'];
        var regex = /(?:\.([^.]+))?$/;
        var extension = regex.exec(fileName);
        if (undefined !== extension && null !== extension) {
            for (var _i = 0, allowedFiles_1 = allowedFiles; _i < allowedFiles_1.length; _i++) {
                var ext = allowedFiles_1[_i];
                if (ext === extension[0]) {
                    isFileAllowed = true;
                }
            }
        }
        else {
            isFileAllowed = false;
        }
        if (isFileAllowed == false) {
            not_upload = true;
        }
        else {
            not_upload = false;
        }
        return not_upload;
    };
    AdminCarUpdateComponent.prototype.dropped = function (files) {
        var _this = this;
        this.files = files;
        for (var droppedFileIndex = 0; droppedFileIndex < this.files.length; droppedFileIndex++) {
            var droppedFile = this.files[droppedFileIndex];
            if (this.isFileAllowed(droppedFile.fileEntry.name)) {
                this.snackBar.open("Photo error occured", '', {
                    duration: 2000,
                    panelClass: ['orange-text']
                });
                return false;
            }
        }
        for (var _i = 0, files_1 = files; _i < files_1.length; _i++) {
            var droppedFile = files_1[_i];
            // Is it a file?
            if (droppedFile.fileEntry.isFile) {
                var fileEntry = droppedFile.fileEntry;
                fileEntry.file(function (file) {
                    //console.log(droppedFile.relativePath, file);
                    var reader = new FileReader();
                    reader.readAsDataURL(file);
                    reader.onload = function () {
                        document.getElementById('upload_photos').classList.add('disabled');
                        _this.before_carimages.push({ 'car_photo': reader.result });
                        _this.loadingStatus = true;
                    };
                    _this.ng2ImgMax.resizeImage(file, 1500, 500).subscribe(function (result) {
                        var carimage = new File([result], result.name);
                        _this.carImages.push(carimage);
                        if (files.length == _this.carImages.length) {
                            _this.uploadphoto(_this.carImages);
                        }
                    }, function (error) {
                        _this.loadingStatus = false;
                        document.getElementById('upload_photos').classList.remove('disabled');
                        _this.snackBar.open("Photo error occured", '', {
                            duration: 2000,
                            panelClass: ['orange-text']
                        });
                    });
                });
            }
            else {
                var fileEntry = droppedFile.fileEntry;
                console.log(droppedFile.relativePath, fileEntry);
            }
        }
    };
    AdminCarUpdateComponent.prototype.uploadphoto = function (carImages) {
        var _this = this;
        var formData = new FormData();
        for (var i = 0; i < carImages.length; i++) {
            formData.append("media", carImages[i]);
        }
        formData.append("car_id", this.id);
        this.rest.upload('upload_car_photo', formData, {}).subscribe(function (response) {
            document.getElementById('upload_photos').classList.remove('disabled');
            if (response.status) {
                _this.before_carimages = [];
                _this.carImages = [];
                _this.loadingStatus = false;
                _this.car_images = response.data.car_photos;
                _this.snackBar.open(response.message, '', {
                    duration: 2000,
                    panelClass: ['green-text']
                });
            }
            else {
                _this.snackBar.open("Photo not Uploaded", '', {
                    duration: 2000,
                    panelClass: ['orange-text']
                });
            }
        });
    };
    AdminCarUpdateComponent.prototype.delete_photo = function (details) {
        var _this = this;
        var params = {
            "id": details._id,
            "name": details.name,
            "car_id": this.id
        };
        var data = JSON.stringify(params);
        this.rest.post('delete_photo', data).subscribe(function (response) {
            if (response.status) {
                _this.getCarPhoto();
                _this.snackBar.open(response.message, '', {
                    duration: 2000,
                    panelClass: ['green-text']
                });
            }
            else {
                _this.snackBar.open(response.message, '', {
                    duration: 2000,
                    panelClass: ['orange-text']
                });
            }
        });
    };
    AdminCarUpdateComponent.prototype.changeComplete = function (event) {
        this.state = event.color.hex;
    };
    AdminCarUpdateComponent.prototype.numberOnly = function (event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46) {
            return false;
        }
        return true;
    };
    AdminCarUpdateComponent.prototype.add_color_price = function () {
        var _this = this;
        if (this.color_price == '' || this.color_price == undefined) {
            this.snackBar.open("Enter price", "", {
                panelClass: 'orange-text',
                duration: 2000
            });
        }
        else {
            this.btnExtraOpts.active = true;
            this.btnExtraOpts.text = 'Saving...';
            var model_1 = {
                'color_code': this.state,
                'price': this.color_price
            };
            extra_price.push(model_1);
            var params = {
                'extra_price': extra_price,
                'type': 'edit',
                'carid': this.id
            };
            var data = JSON.stringify(params);
            this.rest.post('add_car', data).subscribe(function (response) {
                _this.btnExtraOpts.active = false;
                _this.btnExtraOpts.text = 'Add';
                if (response.status) {
                    _this.extra_color_price.push(model_1);
                    _this.extra_color_price = _this.extra_color_price.slice();
                    _this.state = '#000';
                    _this.color_price = '';
                    _this.snackBar.open(response.message, '', {
                        duration: 2000,
                        panelClass: ['green-text']
                    });
                }
                else {
                    _this.snackBar.open(response.message, '', {
                        duration: 2000,
                        panelClass: ['orange-text']
                    });
                }
            });
        }
    };
    AdminCarUpdateComponent.prototype.delete_color_price = function (element) {
        var _this = this;
        var index = this.extra_color_price.indexOf(element);
        if (index > -1) {
            this.extra_color_price.splice(index, 1);
            var params = {
                'extra_price': this.extra_color_price,
                'type': 'edit',
                'carid': this.id
            };
            var data = JSON.stringify(params);
            this.rest.post('add_car', data).subscribe(function (response) {
                if (response.status) {
                    _this.extra_color_price = _this.extra_color_price.slice();
                    _this.snackBar.open(response.message, '', {
                        duration: 2000,
                        panelClass: ['green-text']
                    });
                }
                else {
                    _this.snackBar.open(response.message, '', {
                        duration: 2000,
                        panelClass: ['orange-text']
                    });
                }
            });
        }
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('stepper'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_material_stepper__WEBPACK_IMPORTED_MODULE_4__["MatStepper"])
    ], AdminCarUpdateComponent.prototype, "myStepper", void 0);
    AdminCarUpdateComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-admin-car-update',
            template: __webpack_require__(/*! ./admin-car-update.component.html */ "./src/app/admin-car-update/admin-car-update.component.html"),
            styles: [__webpack_require__(/*! ./admin-car-update.component.css */ "./src/app/admin-car-update/admin-car-update.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_rest_service__WEBPACK_IMPORTED_MODULE_5__["RestService"], _common_service__WEBPACK_IMPORTED_MODULE_6__["CommonService"], ng2_img_max__WEBPACK_IMPORTED_MODULE_8__["Ng2ImgMaxService"], _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatSnackBar"]])
    ], AdminCarUpdateComponent);
    return AdminCarUpdateComponent;
}());



/***/ }),

/***/ "./src/app/admin-car/admin-car.component.css":
/*!***************************************************!*\
  !*** ./src/app/admin-car/admin-car.component.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhbmd1bGFyX2FwcC9zcmMvYXBwL2FkbWluLWNhci9hZG1pbi1jYXIuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/admin-car/admin-car.component.html":
/*!****************************************************!*\
  !*** ./src/app/admin-car/admin-car.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"col-12 p-0\">\n  <div class=\"container p-0\">\n    <div class=\"row m-0\">\n      <div class=\"col-12\" id=\"no-more-tables\">\n        <div class=\"col-12 p-0 dash-top-section mt-5 mb-3\">\n            <a routerLink=\"/admin/car\" routerLinkActive=\"active\" class=\"linkactive border-1 l-h-28 rounded py-1 px-2 d-inline-block color-inherit mb-2 mr-2 font-9\">Manage cars</a>\n            <a routerLink=\"/admin/make\" routerLinkActive=\"active\" class=\"linkactive border-1 l-h-28 rounded py-1 px-2 d-inline-block color-inherit mb-2 mr-2 font-9\">Manage make</a>\n            <a routerLink=\"/admin/image_category\" routerLinkActive=\"active\" class=\"linkactive border-1 l-h-28 rounded py-1 px-2 d-inline-block color-inherit mb-2 mr-2 font-9\">Manage image Category</a>\n        </div>\n        <div class=\"col-12 px-0 border-1 mb-3 rounded white\">\n          <div class=\"row m-0 col-12 align-items-center p-0\">\n            <div class=\"d-inline-block\">\n              <h5 class=\"dark-text-color m-0 p-3\">Car Management</h5>\n            </div>\n          </div>\n          <table mat-table [dataSource]=\"data\" class=\"mat-elevation-z0 w-100\">\n            <ng-container matColumnDef=\"make_title\">\n              <th mat-header-cell *matHeaderCellDef class=\"head grey lighten-4 sub-text-color\"> Make </th>\n              <td data-title=\"Make\" mat-cell *matCellDef=\"let element\"\n                class=\"py-2 align-items-center dark-text-color border-0\">\n                <p class=\"mb-1 dark-text-color\">{{element.make_title.name}}</p>\n              </td>\n            </ng-container>\n            <ng-container matColumnDef=\"make_logo\">\n              <th mat-header-cell *matHeaderCellDef class=\"head w-70 grey lighten-4 sub-text-color\"> Make logo </th>\n              <td data-title=\"Make logo\" mat-cell *matCellDef=\"let element\"\n                class=\"py-2 align-items-center page_content dark-text-color border-0\">\n                <p class=\"mb-1 dark-text-color\">\n                  <img src=\"{{element.make_title.makephoto}}\" class=\"w-50x\" />\n                </p>\n              </td>\n            </ng-container>\n            <ng-container matColumnDef=\"model_name\">\n              <th mat-header-cell *matHeaderCellDef class=\"head w-70 grey lighten-4 sub-text-color\"> Model name </th>\n              <td data-title=\"Model name\" mat-cell *matCellDef=\"let element\"\n                class=\"py-2 align-items-center page_content dark-text-color border-0\">\n                <p class=\"mb-1 dark-text-color\">{{element.model}}</p>\n              </td>\n            </ng-container>\n            <ng-container matColumnDef=\"fuel_type\">\n              <th mat-header-cell *matHeaderCellDef class=\"head w-70 grey lighten-4 sub-text-color\"> Fuel type </th>\n              <td data-title=\"Fuel type\" mat-cell *matCellDef=\"let element\"\n                class=\"py-2 align-items-center page_content dark-text-color border-0\">\n                <p class=\"mb-1 dark-text-color text-capitalize\">{{element.fuel_type}}</p>\n              </td>\n            </ng-container>\n            <ng-container matColumnDef=\"transmission_type\">\n              <th mat-header-cell *matHeaderCellDef class=\"head w-70 grey lighten-4 sub-text-color\"> Transmission type </th>\n              <td data-title=\"Transmission type\" mat-cell *matCellDef=\"let element\"\n                class=\"py-2 align-items-center page_content dark-text-color border-0\">\n                <p class=\"mb-1 dark-text-color text-capitalize\">{{element.transmission_type}}</p>\n              </td>\n            </ng-container>\n            <ng-container matColumnDef=\"status\">\n              <th mat-header-cell *matHeaderCellDef class=\"head grey lighten-4 sub-text-color\"> Status </th>\n              <td data-title=\"Status\" mat-cell *matCellDef=\"let element\"\n                class=\"py-2 align-items-center dark-text-color border-0\">\n                <p class=\"mb-1 green-text\" *ngIf=\"element.status\">Active</p>\n                <p class=\"mb-1 red-text\" *ngIf=\"!element.status\">In Active</p>\n              </td>\n            </ng-container>\n            <ng-container matColumnDef=\"action\">\n              <th mat-header-cell *matHeaderCellDef class=\"head grey lighten-4 sub-text-color\"> Action </th>\n              <td data-title=\"Action\" class=\"py-2 border-0\" mat-cell *matCellDef=\"let element\">\n                <button type=\"button\" matTooltip=\"Edit\" matTooltipPosition=\"above\"\n                  class=\"p-0 outline-0 bold_font m-w-auto mr-2\" mat-button routerLink=\"edit/{{element._id}}\"><i\n                    class=\"material-icons primary-text-color\">edit</i></button>\n                <button type=\"button\" matTooltip=\"Delete\" matTooltipPosition=\"above\"\n                  class=\"p-0 outline-0 bold_font m-w-auto\" (click)=\"deleteConfirmation(deleteModal,element._id)\"\n                  mat-button><i class=\"material-icons primary-text-color font-11\">delete</i></button>\n              </td>\n            </ng-container>\n            <tr mat-header-row class=\"border-top\" *matHeaderRowDef=\"displayedColumns\">\n              <td>asdf</td>\n            </tr>\n            <tr class=\"bdr-btm\" mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\n          </table>\n          <div class=\"text-center p-3\" *ngIf=\"data?.length === 0\">No records found</div>\n          <nav aria-label=\"Page navigation example\" class=\"mt-5\">\n            <ul *ngIf=\"pages && pages.length > 1\" class=\"pagination justify-content-end f-wrap\">\n              <li class=\"page-item\" [ngClass]=\"{disabled:current === 1}\">\n                <a class=\"page-link\" mdbWavesEffect (click)=\"get_cars(1)\"><i\n                    class=\"material-icons\">first_page</i></a>\n              </li>\n              <li class=\"page-item\" [ngClass]=\"{disabled:current === 1}\">\n                <a class=\"page-link\" mdbWavesEffect (click)=\"get_cars(current - 1)\"><i\n                    class=\"material-icons\">navigate_before</i></a>\n              </li>\n              <li class=\"page-item mt-1\" *ngFor=\"let page of pages\" [ngClass]=\"{active:current === page}\">\n                <a class=\"page-link\" mdbWavesEffect (click)=\"get_cars(page)\">{{page}}</a>\n              </li>\n              <li class=\"page-item\" [ngClass]=\"{disabled:current === totalPages}\">\n                <a class=\"page-link\" mdbWavesEffect (click)=\"get_cars(current + 1)\"><i\n                    class=\"material-icons\">navigate_next</i></a>\n              </li>\n              <li class=\"page-item\" [ngClass]=\"{disabled:current === totalPages}\">\n                <a class=\"page-link\" mdbWavesEffect (click)=\"get_cars(totalPages)\"><i\n                    class=\"material-icons\">last_page</i></a>\n              </li>\n            </ul>\n          </nav>\n          <div class=\"col-12 text-right py-4\">\n            <button mat-button\n              class=\"outline-0 btn rounded py-3 px-4 font-9 mr-3 text-uppercase elegant-color-dark white-text l-h-normal\"\n              routerLink=\"add\">Add More</button>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n<ng-template #deleteModal>\n  <h4 class=\"mt-2\">Delete Car</h4>\n  <p>Are you sure you want to delete this car?</p>\n  <div class=\"d-flex\">\n    <button type=\"button\" class=\"btn rounded grey lighten-2 mr-3 p-3 black-text l-h-normal bold_font\" mat-button\n      (click)=closeConfirmation()>Cancel</button>\n    <button type=\"button\" [ngClass]=\"{disabled:delete_btn_loading}\"\n      class=\"btn rounded py-3 px-4 text-uppercase elegant-color-dark ml-auto white-text bold_font l-h-normal font-9\"\n      mat-button (click)=deleteCar()>\n      <mat-progress-spinner *ngIf=\"delete_btn_loading\" [color]=\"spinner_color\" [mode]=\"spinner_mode\"\n        [diameter]=\"spinner_value\"></mat-progress-spinner>\n      <span *ngIf=\"!delete_btn_loading\">Delete</span>\n    </button>\n  </div>\n</ng-template>"

/***/ }),

/***/ "./src/app/admin-car/admin-car.component.ts":
/*!**************************************************!*\
  !*** ./src/app/admin-car/admin-car.component.ts ***!
  \**************************************************/
/*! exports provided: AdminCarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminCarComponent", function() { return AdminCarComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../rest.service */ "./src/app/rest.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "../node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material */ "../node_modules/@angular/material/esm5/material.es5.js");





var AdminCarComponent = /** @class */ (function () {
    function AdminCarComponent(rest, bottomSheet, router, snackBar) {
        this.rest = rest;
        this.bottomSheet = bottomSheet;
        this.router = router;
        this.snackBar = snackBar;
        this.displayedColumns = ['make_title', 'make_logo', 'model_name', 'fuel_type', 'transmission_type', 'status', 'action'];
        this.data = [];
        this.spinner_color = 'warn';
        this.spinner_mode = 'indeterminate';
        this.spinner_value = 20;
        this.page = 1;
        this.current = 1;
        this.totalDocs = 0;
        this.totalPages = 0;
        this.startPage = 1;
        this.endPage = 1;
        this.deleteId = "";
        this.delete_btn_loading = false;
    }
    AdminCarComponent.prototype.deleteConfirmation = function (templateRef, id) {
        this.deleteId = id;
        var dialogRef = this.bottomSheet.open(templateRef, {
            panelClass: 'delete-confirmation-width'
            // data: { name: this.name, animal: this.animal }
        });
    };
    AdminCarComponent.prototype.closeConfirmation = function () {
        this.bottomSheet.dismiss();
    };
    AdminCarComponent.prototype.deleteCar = function () {
        var _this = this;
        this.delete_btn_loading = true;
        var params = {
            'carid': this.deleteId,
            'type': 'edit',
            'is_enable': false
        };
        this.rest.post('add_car', JSON.stringify(params)).subscribe(function (response) {
            _this.snackBar.open(response.message, '', {
                panelClass: 'green-text',
                duration: 2000,
            });
            _this.delete_btn_loading = false;
            _this.deleteId = "";
            _this.get_cars(_this.page);
            _this.bottomSheet.dismiss();
        });
    };
    AdminCarComponent.prototype.get_cars = function (page) {
        var _this = this;
        var params = {
            'page': page
        };
        this.rest.post('get_cars', JSON.stringify(params)).subscribe(function (response) {
            _this.data = response.data.car.docs;
            _this.current = response.data.car.page;
            _this.totalDocs = response.data.car.totalDocs;
            _this.totalPages = response.data.car.totalPages;
            if (_this.totalPages <= 5) {
                _this.startPage = 1;
                _this.endPage = _this.totalPages;
            }
            else {
                if (_this.current <= 3) {
                    _this.startPage = 1;
                    _this.endPage = 5;
                }
                else if (_this.current + 2 >= _this.totalPages) {
                    _this.startPage = _this.totalPages - 4;
                    _this.endPage = _this.totalPages;
                }
                else {
                    _this.startPage = _this.current - 2;
                    _this.endPage = _this.current + 2;
                }
            }
            _this.pages = Array.from(Array((_this.endPage + 1) - _this.startPage).keys()).map(function (i) { return _this.startPage + i; });
        });
    };
    AdminCarComponent.prototype.ngOnInit = function () {
        this.data = [];
        this.get_cars(this.page);
    };
    AdminCarComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-admin-car',
            template: __webpack_require__(/*! ./admin-car.component.html */ "./src/app/admin-car/admin-car.component.html"),
            styles: [__webpack_require__(/*! ./admin-car.component.css */ "./src/app/admin-car/admin-car.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_rest_service__WEBPACK_IMPORTED_MODULE_2__["RestService"], _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatBottomSheet"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSnackBar"]])
    ], AdminCarComponent);
    return AdminCarComponent;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/admin-dashboard.component.css":
/*!***************************************************************!*\
  !*** ./src/app/admin-dashboard/admin-dashboard.component.css ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhbmd1bGFyX2FwcC9zcmMvYXBwL2FkbWluLWRhc2hib2FyZC9hZG1pbi1kYXNoYm9hcmQuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/admin-dashboard/admin-dashboard.component.html":
/*!****************************************************************!*\
  !*** ./src/app/admin-dashboard/admin-dashboard.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"col-12 p-3 row m-0\">\n\t<div class=\"col-12 p-0 row m-0\">\n\t\t<div class=\"col-md-3 col-12 p-2\">\n\t\t\t<div class=\"col-12 border-1 p-3 white row m-0\">\n\t\t\t\t<h6 class=\"w-100 font-9\">Total Users Count</h6>\n\t\t\t\t<div class=\"row m-0 w-100 align-items-center\">\n\t\t\t\t\t<span class=\"l-h-normal\"><i class=\"material-icons green-text font-2\">timeline</i></span>\n\t\t\t\t\t<span class=\"ml-auto rgba-cyan-slight px-3 py-1 font-9 green-text radius_50\">255</span>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"col-md-3 col-12 p-2\">\n\t\t\t<div class=\"col-12 border-1 p-3 white row m-0\">\n\t\t\t\t<h6 class=\"w-100 font-9\">Total Cars Count</h6>\n\t\t\t\t<div class=\"row m-0 w-100 align-items-center\">\n\t\t\t\t\t<span class=\"l-h-normal\"><i class=\"material-icons red-text font-2\">timeline</i></span>\n\t\t\t\t\t<span class=\"ml-auto rgba-red-slight px-3 py-1 font-9 red-text radius_50\">102</span>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"col-md-3 col-12 p-2\">\n\t\t\t<div class=\"col-12 border-1 p-3 white row m-0\">\n\t\t\t\t<h6 class=\"w-100 font-9\">Total Users Count</h6>\n\t\t\t\t<div class=\"row m-0 w-100 align-items-center\">\n\t\t\t\t\t<span class=\"l-h-normal\"><i class=\"material-icons purple-text font-2\">timeline</i></span>\n\t\t\t\t\t<span class=\"ml-auto rgba-purple-slight px-3 py-1 font-9 purple-text radius_50\">114</span>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"col-md-3 col-12 p-2\">\n\t\t\t<div class=\"col-12 border-1 p-3 white row m-0\">\n\t\t\t\t<h6 class=\"w-100 font-9\">Total Cars Count</h6>\n\t\t\t\t<div class=\"row m-0 w-100 align-items-center\">\n\t\t\t\t\t<span class=\"l-h-normal\"><i class=\"material-icons orange-text font-2\">timeline</i></span>\n\t\t\t\t\t<span class=\"ml-auto rgba-orange-slight px-3 py-1 font-9 orange-text radius_50\">20</span>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t<div class=\"col-12 col-md-6 p-2 my-2\">\n\t\t<div class=\"col-12 white p-4\">\n\t\t\t<h6 class=\"bold_font mb-3\">Users Info</h6>\n\t\t\t<canvas mdbChart\n\t\t\t  [chartType]=\"chartType\"\n\t\t\t  [datasets]=\"chartDatasets\"\n\t\t\t  [labels]=\"chartLabels\"\n\t\t\t  [colors]=\"chartColors\"\n\t\t\t  [options]=\"chartOptions\"\n\t\t\t  [legend]=\"true\"\n\t\t\t  (chartHover)=\"chartHovered($event)\"\n\t\t\t  (chartClick)=\"chartClicked($event)\">\n\t\t\t</canvas>\t\t\t\n\t\t</div>\n\t</div>\n\t<div class=\"col-12 col-md-6 p-2 my-2\">\n\t\t<div class=\"col-12 white p-4\">\n\t\t\t<h6 class=\"bold_font mb-3\">Cars Info</h6>\n\t\t\t\t<canvas mdbChart\n\t\t\t\t  [chartType]=\"barchartType\"\n\t\t\t\t  [datasets]=\"barchartDatasets\"\n\t\t\t\t  [labels]=\"barchartLabels\"\n\t\t\t\t  [colors]=\"barchartColors\"\n\t\t\t\t  [options]=\"barchartOptions\"\n\t\t\t\t  [legend]=\"true\"\n\t\t\t\t  (chartHover)=\"barchartHovered($event)\"\n\t\t\t\t  (chartClick)=\"barchartClicked($event)\">\n\t\t\t\t</canvas>\t\t\t\n\t\t</div>\n\t</div>\n</div>"

/***/ }),

/***/ "./src/app/admin-dashboard/admin-dashboard.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/admin-dashboard/admin-dashboard.component.ts ***!
  \**************************************************************/
/*! exports provided: AdminDashboardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminDashboardComponent", function() { return AdminDashboardComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");


var AdminDashboardComponent = /** @class */ (function () {
    function AdminDashboardComponent() {
        this.chartType = 'doughnut';
        this.chartDatasets = [
            { data: [55], label: 'My First dataset' }
        ];
        this.chartLabels = ['Customer'];
        this.chartColors = [
            {
                backgroundColor: ['rgba(244, 67, 54, 0.3)', 'rgba(3, 169, 244, 0.3)'],
                hoverBackgroundColor: ['rgba(244, 67, 54, 0.1)', 'rgba(3, 169, 244, 0.1)'],
                borderWidth: 1,
            }
        ];
        this.chartOptions = {
            responsive: true
        };
        this.barchartType = 'bar';
        this.barchartDatasets = [
            { data: [65, 59, 80, 81, 56, 55, 40], label: 'Cars' }
        ];
        this.barchartLabels = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun'];
        this.barchartColors = [
            {
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 2,
            }
        ];
        this.barchartOptions = {
            responsive: true
        };
    }
    AdminDashboardComponent.prototype.ngOnInit = function () {
    };
    AdminDashboardComponent.prototype.chartClicked = function (e) { };
    AdminDashboardComponent.prototype.chartHovered = function (e) { };
    AdminDashboardComponent.prototype.barchartClicked = function (e) { };
    AdminDashboardComponent.prototype.barchartHovered = function (e) { };
    AdminDashboardComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-admin-dashboard',
            template: __webpack_require__(/*! ./admin-dashboard.component.html */ "./src/app/admin-dashboard/admin-dashboard.component.html"),
            styles: [__webpack_require__(/*! ./admin-dashboard.component.css */ "./src/app/admin-dashboard/admin-dashboard.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], AdminDashboardComponent);
    return AdminDashboardComponent;
}());



/***/ }),

/***/ "./src/app/admin-image-category/admin-image-category.component.css":
/*!*************************************************************************!*\
  !*** ./src/app/admin-image-category/admin-image-category.component.css ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhbmd1bGFyX2FwcC9zcmMvYXBwL2FkbWluLWltYWdlLWNhdGVnb3J5L2FkbWluLWltYWdlLWNhdGVnb3J5LmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/admin-image-category/admin-image-category.component.html":
/*!**************************************************************************!*\
  !*** ./src/app/admin-image-category/admin-image-category.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"col-12 p-0\">\n  <div class=\"container p-0\">\n    <div class=\"row m-0\">\n      <div class=\"col-12\" id=\"no-more-tables\">\n        <div class=\"col-12 p-0 dash-top-section mt-5 mb-3\">\n            <a routerLink=\"/admin/car\" routerLinkActive=\"active\" class=\"linkactive border-1 l-h-28 rounded py-1 px-2 d-inline-block color-inherit mb-2 mr-2 font-9\">Manage cars</a>\n            <a routerLink=\"/admin/make\" routerLinkActive=\"active\" class=\"linkactive border-1 l-h-28 rounded py-1 px-2 d-inline-block color-inherit mb-2 mr-2 font-9\">Manage make</a>\n            <a routerLink=\"/admin/image_category\" routerLinkActive=\"active\" class=\"linkactive border-1 l-h-28 rounded py-1 px-2 d-inline-block color-inherit mb-2 mr-2 font-9\">Manage image Category</a>\n        </div>\n        <div class=\"col-12 px-0 border-1 mb-3 rounded white\">\n          <div class=\"row m-0 col-12 align-items-center p-0\">\n            <div class=\"d-inline-block\">\n              <h5 class=\"dark-text-color m-0 p-3\">Car Image Category Management</h5>\n            </div>\n          </div>\n          <table mat-table [dataSource]=\"data\" class=\"mat-elevation-z0 w-100\">\n            <ng-container matColumnDef=\"name\">\n              <th mat-header-cell *matHeaderCellDef class=\"head grey lighten-4 sub-text-color\"> Name </th>\n              <td data-title=\"Name\" mat-cell *matCellDef=\"let element\"\n                class=\"py-2 align-items-center dark-text-color border-0\">\n                <p class=\"mb-1 dark-text-color\">{{element.name}}</p>\n              </td>\n            </ng-container>\n            <ng-container matColumnDef=\"status\">\n              <th mat-header-cell *matHeaderCellDef class=\"head grey lighten-4 sub-text-color\"> Status </th>\n              <td data-title=\"Status\" mat-cell *matCellDef=\"let element\"\n                class=\"py-2 align-items-center dark-text-color border-0\">\n                <p class=\"mb-1 text-capitalize\" [ngClass]=\"element.status == 'active' ? 'green-text': 'red-text'\">{{element.status}}</p>\n              </td>\n            </ng-container>\n            <ng-container matColumnDef=\"action\">\n              <th mat-header-cell *matHeaderCellDef class=\"head grey lighten-4 sub-text-color\"> Action </th>\n              <td data-title=\"Action\" class=\"py-2 border-0\" mat-cell *matCellDef=\"let element\">\n                <button type=\"button\" matTooltip=\"Edit\" matTooltipPosition=\"above\"\n                  class=\"p-0 outline-0 bold_font m-w-auto mr-2\" mat-button routerLink=\"edit/{{element._id}}\"><i\n                    class=\"material-icons primary-text-color font-14\">edit</i></button>\n                <button type=\"button\" matTooltip=\"Delete\" matTooltipPosition=\"above\"\n                  class=\"p-0 outline-0 bold_font m-w-auto\" (click)=\"deleteConfirmation(deleteModal,element._id)\"\n                  mat-button><i class=\"material-icons primary-text-color font-14\">delete</i></button>\n              </td>\n            </ng-container>\n            <tr mat-header-row class=\"border-top\" *matHeaderRowDef=\"displayedColumns\">\n              <td>asdf</td>\n            </tr>\n            <tr class=\"border-bottom\" mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\n          </table>\n          <div class=\"text-center p-3\" *ngIf=\"data?.length === 0\">No records found</div>\n          <nav aria-label=\"Page navigation example\" class=\"mt-5\" *ngIf=\"pages && pages.length > 1\">\n            <ul class=\"pagination justify-content-end f-wrap\">\n              <li class=\"page-item\" [ngClass]=\"{disabled:current === 1}\">\n                <a class=\"page-link\" mdbWavesEffect (click)=\"get_static_pages(1)\"><i\n                    class=\"material-icons\">first_page</i></a>\n              </li>\n              <li class=\"page-item\" [ngClass]=\"{disabled:current === 1}\">\n                <a class=\"page-link\" mdbWavesEffect (click)=\"get_static_pages(current - 1)\"><i\n                    class=\"material-icons\">navigate_before</i></a>\n              </li>\n              <li class=\"page-item mt-1\" *ngFor=\"let page of pages\" [ngClass]=\"{active:current === page}\">\n                <a class=\"page-link\" mdbWavesEffect (click)=\"get_static_pages(page)\">{{page}}</a>\n              </li>\n              <li class=\"page-item\" [ngClass]=\"{disabled:current === totalPages}\">\n                <a class=\"page-link\" mdbWavesEffect (click)=\"get_static_pages(current + 1)\"><i\n                    class=\"material-icons\">navigate_next</i></a>\n              </li>\n              <li class=\"page-item\" [ngClass]=\"{disabled:current === totalPages}\">\n                <a class=\"page-link\" mdbWavesEffect (click)=\"get_static_pages(totalPages)\"><i\n                    class=\"material-icons\">last_page</i></a>\n              </li>\n            </ul>\n          </nav>\n          <div class=\"col-12 text-right py-4\">\n            <button mat-button\n              class=\"outline-0 btn rounded py-3 px-4 font-9 mr-3 text-uppercase elegant-color-dark white-text l-h-normal\"\n              routerLink=\"add\">Add More</button>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n<ng-template #deleteModal>\n  <h4 class=\"mt-2\">Delete Category</h4>\n  <p>Are you sure you want to delete this category?</p>\n  <div class=\"d-flex\">\n    <button type=\"button\" class=\"btn rounded grey lighten-2 mr-3 p-3 black-text l-h-normal bold_font\" mat-button\n      (click)=closeConfirmation()>Cancel</button>\n    <button type=\"button\" [ngClass]=\"{disabled:delete_btn_loading}\"\n      class=\"btn rounded py-3 px-4 text-uppercase elegant-color-dark ml-auto white-text bold_font l-h-normal font-9\"\n      mat-button (click)=deleteCategory()>\n      <mat-progress-spinner *ngIf=\"delete_btn_loading\" [color]=\"spinner_color\" [mode]=\"spinner_mode\"\n        [diameter]=\"spinner_value\"></mat-progress-spinner>\n      <span *ngIf=\"!delete_btn_loading\">Delete</span>\n    </button>\n  </div>\n</ng-template>"

/***/ }),

/***/ "./src/app/admin-image-category/admin-image-category.component.ts":
/*!************************************************************************!*\
  !*** ./src/app/admin-image-category/admin-image-category.component.ts ***!
  \************************************************************************/
/*! exports provided: AdminImageCategoryComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminImageCategoryComponent", function() { return AdminImageCategoryComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../rest.service */ "./src/app/rest.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "../node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material */ "../node_modules/@angular/material/esm5/material.es5.js");





var AdminImageCategoryComponent = /** @class */ (function () {
    function AdminImageCategoryComponent(rest, bottomSheet, router, snackBar) {
        this.rest = rest;
        this.bottomSheet = bottomSheet;
        this.router = router;
        this.snackBar = snackBar;
        this.displayedColumns = ['name', 'status', 'action'];
        this.data = [];
        this.spinner_color = 'warn';
        this.spinner_mode = 'indeterminate';
        this.spinner_value = 20;
        this.page = 1;
        this.current = 1;
        this.totalDocs = 0;
        this.totalPages = 0;
        this.startPage = 1;
        this.endPage = 1;
        this.deleteId = "";
        this.delete_btn_loading = false;
    }
    AdminImageCategoryComponent.prototype.deleteConfirmation = function (templateRef, id) {
        this.deleteId = id;
        var dialogRef = this.bottomSheet.open(templateRef, {
            panelClass: 'delete-confirmation-width'
            // data: { name: this.name, animal: this.animal }
        });
    };
    AdminImageCategoryComponent.prototype.closeConfirmation = function () {
        this.bottomSheet.dismiss();
    };
    AdminImageCategoryComponent.prototype.deleteCategory = function () {
        var _this = this;
        this.delete_btn_loading = true;
        var params = {
            'id': this.deleteId
        };
        this.rest.post('delete_image_category', JSON.stringify(params)).subscribe(function (response) {
            _this.snackBar.open(response.message, '', {
                duration: 2000,
            });
            _this.delete_btn_loading = false;
            _this.deleteId = "";
            _this.get_car_image_category(_this.page);
            _this.bottomSheet.dismiss();
        });
    };
    AdminImageCategoryComponent.prototype.get_car_image_category = function (page) {
        var _this = this;
        var params = {
            'page': page
        };
        this.rest.post('get_car_image_category', JSON.stringify(params)).subscribe(function (response) {
            _this.data = response.data.car.docs;
            _this.current = response.data.car.page;
            _this.totalDocs = response.data.car.totalDocs;
            _this.totalPages = response.data.car.totalPages;
            if (_this.totalPages <= 5) {
                _this.startPage = 1;
                _this.endPage = _this.totalPages;
            }
            else {
                if (_this.current <= 3) {
                    _this.startPage = 1;
                    _this.endPage = 5;
                }
                else if (_this.current + 2 >= _this.totalPages) {
                    _this.startPage = _this.totalPages - 4;
                    _this.endPage = _this.totalPages;
                }
                else {
                    _this.startPage = _this.current - 2;
                    _this.endPage = _this.current + 2;
                }
            }
            if (_this.pages && _this.pages.length > 1) {
                _this.pages = Array.from(Array((_this.endPage + 1) - _this.startPage).keys()).map(function (i) { return _this.startPage + i; });
            }
        });
    };
    AdminImageCategoryComponent.prototype.ngOnInit = function () {
        this.data = [];
        this.get_car_image_category(this.page);
    };
    AdminImageCategoryComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-admin-image-category',
            template: __webpack_require__(/*! ./admin-image-category.component.html */ "./src/app/admin-image-category/admin-image-category.component.html"),
            styles: [__webpack_require__(/*! ./admin-image-category.component.css */ "./src/app/admin-image-category/admin-image-category.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_rest_service__WEBPACK_IMPORTED_MODULE_2__["RestService"], _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatBottomSheet"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSnackBar"]])
    ], AdminImageCategoryComponent);
    return AdminImageCategoryComponent;
}());



/***/ }),

/***/ "./src/app/admin-login/admin-login.component.css":
/*!*******************************************************!*\
  !*** ./src/app/admin-login/admin-login.component.css ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhbmd1bGFyX2FwcC9zcmMvYXBwL2FkbWluLWxvZ2luL2FkbWluLWxvZ2luLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/admin-login/admin-login.component.html":
/*!********************************************************!*\
  !*** ./src/app/admin-login/admin-login.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section class=\"d-flex justify-content-center p-10 align-items-center h-100\">\n  <div class=\"user_login\">\n    <div class=\"account_section px-3 pt-3 pb-3\">\n      <h4 class=\"text-uppercase mt-3 mb-4 black-text text-center bold_font\">Admin Login</h4> \n        <div class=\"\">\t\t\t\n          <mat-form-field appearance=\"outline\" class=\"w-100\"> \n            <mat-label>Email</mat-label> \t\t \t\t\t\n            <input type=\"text\" matInput [(ngModel)]=\"formValues['email']\" placeholder=\"Email\" autocomplete=\"off\" name=\"\" class=\"\" (keydown.enter)=\"loginSubmit()\" />\n          </mat-form-field>\n          <mat-form-field appearance=\"outline\" class=\"w-100\"> \n            <mat-label>Password</mat-label> \t\n            <input matInput placeholder=\"Password\" [(ngModel)]=\"formValues['password']\" name=\"\" class=\"\" [type]=\"hide ? 'password' : 'text'\" (keydown.enter)=\"loginSubmit()\" />\n            <i class=\"material-icons pointer primary-text-color\" matSuffix (click)=\"hide = !hide\">{{hide ? 'visibility_off' : 'visibility'}}</i>\n          </mat-form-field>\n          <div class=\"col-12 p-3 text-center\">\n            <button type=\"button\" mat-button class=\"btn orange darken-3 d-inline-flex px-4 py-2 font-8 text-white m-0 rounded\" (click)=\"loginSubmit()\">Submit</button>\n          </div>\n        </div>\n    </div>\n  </div>\n</section>"

/***/ }),

/***/ "./src/app/admin-login/admin-login.component.ts":
/*!******************************************************!*\
  !*** ./src/app/admin-login/admin-login.component.ts ***!
  \******************************************************/
/*! exports provided: AdminLoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminLoginComponent", function() { return AdminLoginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "../node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "../node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _rest_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../rest.service */ "./src/app/rest.service.ts");
/* harmony import */ var _common_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../common.service */ "./src/app/common.service.ts");






var AdminLoginComponent = /** @class */ (function () {
    function AdminLoginComponent(rest, router, snackBar, commonservice) {
        this.rest = rest;
        this.router = router;
        this.snackBar = snackBar;
        this.commonservice = commonservice;
        this.formValues = {};
        this.hide = true;
    }
    AdminLoginComponent.prototype.ngOnInit = function () {
        if (localStorage.getItem('isAdminLogged') == "true") {
            this.router.navigateByUrl('admin/dashboard');
        }
    };
    AdminLoginComponent.prototype.validateEmail = function (email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    };
    AdminLoginComponent.prototype.loginSubmit = function () {
        var _this = this;
        var error = '';
        // stop here if form is invalid
        if (this.formValues['email'] == undefined || this.formValues['email'] == '') {
            error = 'Enter email';
        }
        else if (!this.validateEmail(this.formValues['email'])) {
            error = 'Enter a valid Email';
        }
        else if (this.formValues['password'] == undefined || this.formValues['password'] == '') {
            error = 'Enter password';
        }
        if (error !== '') {
            this.snackBar.open(error, '', {
                panelClass: 'orange-text',
                duration: 2000,
            });
            return;
        }
        var params = {
            'email': this.formValues['email'],
            'password': this.formValues['password']
        };
        var data = JSON.stringify(params);
        this.rest.post('admin_login', data).subscribe(function (response) {
            if (response.success) {
                _this.snackBar.open(response.message, '', {
                    duration: 2000,
                    panelClass: 'green-text'
                });
                _this.commonservice.admin_login = 'true';
                localStorage.setItem('adminUsers', JSON.stringify(response.data.users));
                localStorage.setItem('isAdminLogged', 'true');
                _this.router.navigateByUrl('admin/dashboard');
            }
            else {
                _this.snackBar.open(response.message, '', {
                    duration: 2000,
                    panelClass: 'orange-text'
                });
            }
        });
    };
    AdminLoginComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-admin-login',
            template: __webpack_require__(/*! ./admin-login.component.html */ "./src/app/admin-login/admin-login.component.html"),
            styles: [__webpack_require__(/*! ./admin-login.component.css */ "./src/app/admin-login/admin-login.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_rest_service__WEBPACK_IMPORTED_MODULE_4__["RestService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSnackBar"], _common_service__WEBPACK_IMPORTED_MODULE_5__["CommonService"]])
    ], AdminLoginComponent);
    return AdminLoginComponent;
}());



/***/ }),

/***/ "./src/app/admin-make-update/admin-make-update.component.css":
/*!*******************************************************************!*\
  !*** ./src/app/admin-make-update/admin-make-update.component.css ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhbmd1bGFyX2FwcC9zcmMvYXBwL2FkbWluLW1ha2UtdXBkYXRlL2FkbWluLW1ha2UtdXBkYXRlLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/admin-make-update/admin-make-update.component.html":
/*!********************************************************************!*\
  !*** ./src/app/admin-make-update/admin-make-update.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"col-12 p-0 mt-3\" [ngClass]=\"{h100:thispageLoad}\">\n  <div class=\"container col-md-6 mx-auto p-3\" [ngClass]=\"{h100:thispageLoad}\">\n    <div class=\"d-flex justify-content-center align-items-center h-100\" *ngIf=\"thispageLoad\">\n      <mat-progress-spinner [color]=\"spinner_color\" [mode]=\"spinner_mode\" [diameter]=50></mat-progress-spinner>\n    </div>\n    <div class=\"col-12  white z-depth-1 p-0 text-center pb-5\" *ngIf=\"!showForm\">\n      <div class=\"no_data_image\"></div>\n      <h4>Oops! Record is missing.</h4>\n      <small>\n        <p>This record must be delete or Unavailable</p>\n      </small>\n      <button type=\"button\" class=\"btn rounded black-text grey lighten-4 p-3 mr-3\" routerLink=\"/admin/image_category\"\n        mat-button>Go Back</button>\n    </div>\n    <div class=\"col-12  white z-depth-1\" *ngIf=\"showForm\">\n      <div class=\"d-flex align-items-center f-wrap\">\n        <h4 class=\"bold_font w-100 mb-0 col-md-7 col-sm-5 my-3 pl-0\">{{page_title}}</h4>\n      </div>\n      <div class=\"col-12 p-0\">\n        <div class=\"d-flex row mb-3 pb-3\">\n          <div class=\"col-12\">\n            <mat-form-field appearance=\"outline\" class=\"w-100\">\n              <mat-label>Name</mat-label>\n              <input type=\"text\" matInput [(ngModel)]=\"name\" placeholder=\"Name\" autocomplete=\"off\" name=\"\" class=\"\" />\n            </mat-form-field>\n          </div>\n          <div class=\"col-12\">\n            <div class=\"upload-box-content\">\n              <div class=\"drop-container contain-bg d-flex align-items-center justify-content-center mb-4 h-200 border-1 rounded\" [style.background-image]=\"'url('+imageSrc+')'\">\n                <p class=\"\" [ngClass]=\"[imageSrc != null ? 'position-absolute p-3 t-0 r-0' : 'mb-0 text-center px-3']\">\n                  <label class=\"upload-button mb-0 primary-text-color font-9 pointer\"><input type=\"file\"\n                      (change)=\"makePhotoUpload($event)\" accept=\"image/*\" class=\"position-absolute w-50x z-index_1 opacity-0\"> Browse </label> to upload.\n                </p>\n              </div>\n            </div>\n          </div>\n          <div class=\"col-12\">\n            <mat-form-field appearance=\"outline\" class=\"w-100\">\n              <mat-label>Status</mat-label>\n              <mat-select [(value)]=\"status\" class=\"\">\n                <mat-option value=\"active\">Active</mat-option>\n                <mat-option value=\"inactive\">In active</mat-option>\n              </mat-select>\n            </mat-form-field>\n          </div>\n          <div class=\"col-sm-12 mt-3 d-flex\">\n            <button type=\"button\" class=\"btn rounded black-text grey lighten-4 l-h-normal p-3 mr-3\"\n              routerLink=\"/admin/make\" mat-button>Cancel</button>\n            <mat-spinner-button class=\"btn orange darken-3 l-h-normal ml-auto p-3 m-0 rounded\"\n              (btnClick)=\"update_make()\" [options]=\"btnOpts\"></mat-spinner-button>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/admin-make-update/admin-make-update.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/admin-make-update/admin-make-update.component.ts ***!
  \******************************************************************/
/*! exports provided: AdminMakeUpdateComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminMakeUpdateComponent", function() { return AdminMakeUpdateComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "../node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "../node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _rest_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../rest.service */ "./src/app/rest.service.ts");
/* harmony import */ var _common_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../common.service */ "./src/app/common.service.ts");






var AdminMakeUpdateComponent = /** @class */ (function () {
    function AdminMakeUpdateComponent(rest, router, snackBar, commonservice) {
        this.rest = rest;
        this.router = router;
        this.snackBar = snackBar;
        this.commonservice = commonservice;
        this.btnOpts = {
            active: false,
            text: 'Submit',
            spinnerSize: 18,
            raised: true,
            stroked: false,
            spinnerColor: 'accent',
            fullWidth: false,
            disabled: false,
            mode: 'indeterminate',
        };
        this.name = "";
        this.status = "active";
        this.id = "";
        this.showForm = true;
        this.thispageLoad = false;
        this.thisEditpage = false;
    }
    AdminMakeUpdateComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.returnUrl = 'admin/make';
        this.page_title = "Add Make";
        if (this.commonservice.getthird_index() == "edit") {
            this.page_title = "Update Make";
            this.thisEditpage = true;
            this.thispageLoad = true;
            var params = {
                'id': this.commonservice.urlFourthSegment
            };
            var data = JSON.stringify(params);
            this.rest.post('get_make_detail', data).subscribe(function (response) {
                if (response.status) {
                    _this.showForm = true;
                    _this.name = response.data.admin_image_category.name;
                    _this.status = response.data.admin_image_category.status;
                    _this.imageSrc = response.data.admin_image_category.makephoto;
                    _this.id = response.data.admin_image_category.id;
                }
                else {
                    _this.showForm = false;
                }
                _this.thispageLoad = false;
            });
        }
    };
    AdminMakeUpdateComponent.prototype.makePhotoUpload = function (event) {
        var _this = this;
        this.selectedFile = event.target.files[0];
        if (event.target.files && event.target.files[0]) {
            var file = event.target.files[0];
            var reader_1 = new FileReader();
            reader_1.onload = function (e) { return _this.imageSrc = reader_1.result; };
            reader_1.readAsDataURL(file);
        }
    };
    AdminMakeUpdateComponent.prototype.update_make = function () {
        var _this = this;
        if (this.name == "") {
            this.snackBar.open('Enter a make name', '', {
                duration: 2000,
                panelClass: ['orange-text']
            });
        }
        else if (this.selectedFile == undefined && this.thisEditpage == false) {
            this.snackBar.open('Upload make image', '', {
                duration: 2000,
                panelClass: ['orange-text']
            });
        }
        else {
            this.btnOpts.active = true;
            this.btnOpts.text = "Saving...";
            var uploadData = new FormData();
            if (this.selectedFile != undefined)
                uploadData.append('media', this.selectedFile, this.selectedFile.name);
            uploadData.append('name', this.name);
            uploadData.append('status', this.status);
            uploadData.append('is_enable', 'true');
            uploadData.append('id', this.id);
            var data = uploadData;
            var params = {
                'type': this.thisEditpage
            };
            this.rest.upload('add_make', data, params).subscribe(function (response) {
                if (response.status) {
                    _this.btnOpts.active = false;
                    _this.btnOpts.text = "Submit";
                    _this.snackBar.open(response.message, '', {
                        duration: 2000,
                        panelClass: ['green-text']
                    });
                    _this.router.navigateByUrl(_this.returnUrl);
                }
                else {
                    _this.snackBar.open(response.message, '', {
                        duration: 2000,
                        panelClass: ['orange-text']
                    });
                }
            });
        }
    };
    AdminMakeUpdateComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-admin-make-update',
            template: __webpack_require__(/*! ./admin-make-update.component.html */ "./src/app/admin-make-update/admin-make-update.component.html"),
            styles: [__webpack_require__(/*! ./admin-make-update.component.css */ "./src/app/admin-make-update/admin-make-update.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_rest_service__WEBPACK_IMPORTED_MODULE_4__["RestService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSnackBar"], _common_service__WEBPACK_IMPORTED_MODULE_5__["CommonService"]])
    ], AdminMakeUpdateComponent);
    return AdminMakeUpdateComponent;
}());



/***/ }),

/***/ "./src/app/admin-make/admin-make.component.css":
/*!*****************************************************!*\
  !*** ./src/app/admin-make/admin-make.component.css ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhbmd1bGFyX2FwcC9zcmMvYXBwL2FkbWluLW1ha2UvYWRtaW4tbWFrZS5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/admin-make/admin-make.component.html":
/*!******************************************************!*\
  !*** ./src/app/admin-make/admin-make.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"col-12 p-0\">\n    <div class=\"container p-0\">\n      <div class=\"row m-0\">\n        <div class=\"col-12\" id=\"no-more-tables\">\n          <div class=\"col-12 p-0 dash-top-section mt-5 mb-3\">\n              <a routerLink=\"/admin/car\" routerLinkActive=\"active\" class=\"linkactive border-1 l-h-28 rounded py-1 px-2 d-inline-block color-inherit mb-2 mr-2 font-9\">Manage cars</a>\n              <a routerLink=\"/admin/make\" routerLinkActive=\"active\" class=\"linkactive border-1 l-h-28 rounded py-1 px-2 d-inline-block color-inherit mb-2 mr-2 font-9\">Manage make</a>\n              <a routerLink=\"/admin/image_category\" routerLinkActive=\"active\" class=\"linkactive border-1 l-h-28 rounded py-1 px-2 d-inline-block color-inherit mb-2 mr-2 font-9\">Manage image Category</a>\n          </div>\n          <div class=\"col-12 px-0 border-1 mb-3 rounded white\">\n            <div class=\"row m-0 col-12 align-items-center p-0\">\n              <div class=\"d-inline-block\">\n                <h5 class=\"dark-text-color m-0 p-3\">Car Make Management</h5>\n              </div>\n            </div>\n            <table mat-table [dataSource]=\"data\" class=\"mat-elevation-z0 w-100\">\n              <ng-container matColumnDef=\"name\">\n                <th mat-header-cell *matHeaderCellDef class=\"head grey lighten-4 sub-text-color\"> Name </th>\n                <td data-title=\"Name\" mat-cell *matCellDef=\"let element\"\n                  class=\"py-2 align-items-center dark-text-color border-0\">\n                  <p class=\"mb-1 dark-text-color\">{{element.name}}</p>\n                </td>\n              </ng-container>\n              <ng-container matColumnDef=\"photo\">\n                <th mat-header-cell *matHeaderCellDef class=\"head grey lighten-4 sub-text-color\"> Photo </th>\n                <td data-title=\"Photo\" mat-cell *matCellDef=\"let element\"\n                  class=\"py-2 align-items-center dark-text-color border-0\">\n                  <p class=\"mb-1 dark-text-color\"><img src=\"{{element.makephoto}}\" class=\"w-30x\" /></p>\n                </td>\n              </ng-container>\n              <ng-container matColumnDef=\"status\">\n                <th mat-header-cell *matHeaderCellDef class=\"head grey lighten-4 sub-text-color\"> Status </th>\n                <td data-title=\"Status\" mat-cell *matCellDef=\"let element\"\n                  class=\"py-2 align-items-center dark-text-color border-0\">\n                  <p class=\"mb-1 text-capitalize\" [ngClass]=\"element.status == 'active' ? 'green-text': 'red-text'\">{{element.status}}</p>\n                </td>\n              </ng-container>\n              <ng-container matColumnDef=\"action\">\n                <th mat-header-cell *matHeaderCellDef class=\"head grey lighten-4 sub-text-color\"> Action </th>\n                <td data-title=\"Action\" class=\"py-2 border-0\" mat-cell *matCellDef=\"let element\">\n                  <button type=\"button\" matTooltip=\"Edit\" matTooltipPosition=\"above\"\n                    class=\"p-0 outline-0 bold_font m-w-auto mr-2\" mat-button routerLink=\"edit/{{element._id}}\"><i\n                      class=\"material-icons primary-text-color font-14\">edit</i></button>\n                  <button type=\"button\" matTooltip=\"Delete\" matTooltipPosition=\"above\"\n                    class=\"p-0 outline-0 bold_font m-w-auto\" (click)=\"deleteConfirmation(deleteModal,element._id)\"\n                    mat-button><i class=\"material-icons primary-text-color font-14\">delete</i></button>\n                </td>\n              </ng-container>\n              <tr mat-header-row class=\"border-top\" *matHeaderRowDef=\"displayedColumns\">\n                <td>asdf</td>\n              </tr>\n              <tr class=\"border-bottom\" mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\n            </table>\n            <div class=\"text-center p-3\" *ngIf=\"data?.length === 0\">No records found</div>\n            <nav aria-label=\"Page navigation example\" class=\"mt-5\" *ngIf=\"pages && pages.length > 1\">\n              <ul class=\"pagination justify-content-end f-wrap\">\n                <li class=\"page-item\" [ngClass]=\"{disabled:current === 1}\">\n                  <a class=\"page-link\" mdbWavesEffect (click)=\"get_static_pages(1)\"><i\n                      class=\"material-icons\">first_page</i></a>\n                </li>\n                <li class=\"page-item\" [ngClass]=\"{disabled:current === 1}\">\n                  <a class=\"page-link\" mdbWavesEffect (click)=\"get_static_pages(current - 1)\"><i\n                      class=\"material-icons\">navigate_before</i></a>\n                </li>\n                <li class=\"page-item mt-1\" *ngFor=\"let page of pages\" [ngClass]=\"{active:current === page}\">\n                  <a class=\"page-link\" mdbWavesEffect (click)=\"get_static_pages(page)\">{{page}}</a>\n                </li>\n                <li class=\"page-item\" [ngClass]=\"{disabled:current === totalPages}\">\n                  <a class=\"page-link\" mdbWavesEffect (click)=\"get_static_pages(current + 1)\"><i\n                      class=\"material-icons\">navigate_next</i></a>\n                </li>\n                <li class=\"page-item\" [ngClass]=\"{disabled:current === totalPages}\">\n                  <a class=\"page-link\" mdbWavesEffect (click)=\"get_static_pages(totalPages)\"><i\n                      class=\"material-icons\">last_page</i></a>\n                </li>\n              </ul>\n            </nav>\n            <div class=\"col-12 text-right py-4\">\n              <button mat-button\n                class=\"outline-0 btn rounded py-3 px-4 font-9 mr-3 text-uppercase elegant-color-dark white-text l-h-normal\"\n                routerLink=\"add\">Add More</button>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n  <ng-template #deleteModal>\n    <h4 class=\"mt-2\">Delete Make</h4>\n    <p>Are you sure you want to delete this make?</p>\n    <div class=\"d-flex\">\n      <button type=\"button\" class=\"btn rounded grey lighten-2 mr-3 p-3 black-text l-h-normal bold_font\" mat-button\n        (click)=closeConfirmation()>Cancel</button>\n      <button type=\"button\" [ngClass]=\"{disabled:delete_btn_loading}\"\n        class=\"btn rounded py-3 px-4 text-uppercase elegant-color-dark ml-auto white-text bold_font l-h-normal font-9\"\n        mat-button (click)=deleteMake()>\n        <mat-progress-spinner *ngIf=\"delete_btn_loading\" [color]=\"spinner_color\" [mode]=\"spinner_mode\"\n          [diameter]=\"spinner_value\"></mat-progress-spinner>\n        <span *ngIf=\"!delete_btn_loading\">Delete</span>\n      </button>\n    </div>\n  </ng-template>"

/***/ }),

/***/ "./src/app/admin-make/admin-make.component.ts":
/*!****************************************************!*\
  !*** ./src/app/admin-make/admin-make.component.ts ***!
  \****************************************************/
/*! exports provided: AdminMakeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminMakeComponent", function() { return AdminMakeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../rest.service */ "./src/app/rest.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "../node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material */ "../node_modules/@angular/material/esm5/material.es5.js");





var AdminMakeComponent = /** @class */ (function () {
    function AdminMakeComponent(rest, bottomSheet, router, snackBar) {
        this.rest = rest;
        this.bottomSheet = bottomSheet;
        this.router = router;
        this.snackBar = snackBar;
        this.displayedColumns = ['name', 'photo', 'status', 'action'];
        this.data = [];
        this.spinner_color = 'warn';
        this.spinner_mode = 'indeterminate';
        this.spinner_value = 20;
        this.page = 1;
        this.current = 1;
        this.totalDocs = 0;
        this.totalPages = 0;
        this.startPage = 1;
        this.endPage = 1;
        this.deleteId = "";
        this.delete_btn_loading = false;
    }
    AdminMakeComponent.prototype.deleteConfirmation = function (templateRef, id) {
        this.deleteId = id;
        var dialogRef = this.bottomSheet.open(templateRef, {
            panelClass: 'delete-confirmation-width'
            // data: { name: this.name, animal: this.animal }
        });
    };
    AdminMakeComponent.prototype.closeConfirmation = function () {
        this.bottomSheet.dismiss();
    };
    AdminMakeComponent.prototype.deleteMake = function () {
        var _this = this;
        this.delete_btn_loading = true;
        var params = {
            'id': this.deleteId
        };
        this.rest.post('delete_car_make', JSON.stringify(params)).subscribe(function (response) {
            _this.snackBar.open(response.message, '', {
                duration: 2000,
            });
            _this.delete_btn_loading = false;
            _this.deleteId = "";
            _this.get_car_make(_this.page);
            _this.bottomSheet.dismiss();
        });
    };
    AdminMakeComponent.prototype.get_car_make = function (page) {
        var _this = this;
        var params = {
            'page': page
        };
        this.rest.post('get_car_make', JSON.stringify(params)).subscribe(function (response) {
            _this.data = response.data.car.docs;
            _this.current = response.data.car.page;
            _this.totalDocs = response.data.car.totalDocs;
            _this.totalPages = response.data.car.totalPages;
            if (_this.totalPages <= 5) {
                _this.startPage = 1;
                _this.endPage = _this.totalPages;
            }
            else {
                if (_this.current <= 3) {
                    _this.startPage = 1;
                    _this.endPage = 5;
                }
                else if (_this.current + 2 >= _this.totalPages) {
                    _this.startPage = _this.totalPages - 4;
                    _this.endPage = _this.totalPages;
                }
                else {
                    _this.startPage = _this.current - 2;
                    _this.endPage = _this.current + 2;
                }
            }
            if (_this.pages && _this.pages.length > 1) {
                _this.pages = Array.from(Array((_this.endPage + 1) - _this.startPage).keys()).map(function (i) { return _this.startPage + i; });
            }
        });
    };
    AdminMakeComponent.prototype.ngOnInit = function () {
        this.data = [];
        this.get_car_make(this.page);
    };
    AdminMakeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-admin-make',
            template: __webpack_require__(/*! ./admin-make.component.html */ "./src/app/admin-make/admin-make.component.html"),
            styles: [__webpack_require__(/*! ./admin-make.component.css */ "./src/app/admin-make/admin-make.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_rest_service__WEBPACK_IMPORTED_MODULE_2__["RestService"], _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatBottomSheet"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSnackBar"]])
    ], AdminMakeComponent);
    return AdminMakeComponent;
}());



/***/ }),

/***/ "./src/app/admin-sidebar/admin-sidebar.component.css":
/*!***********************************************************!*\
  !*** ./src/app/admin-sidebar/admin-sidebar.component.css ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhbmd1bGFyX2FwcC9zcmMvYXBwL2FkbWluLXNpZGViYXIvYWRtaW4tc2lkZWJhci5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/admin-sidebar/admin-sidebar.component.html":
/*!************************************************************!*\
  !*** ./src/app/admin-sidebar/admin-sidebar.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"white py-3\">\n  <div class=\"list-group list-group-flush\">\n      <a routerLink=\"admin/dashboard\" [ngClass]=\"[secondSegment == 'dashboard' ? 'primary-text-color' : '']\" class=\"list-group-item rounded d-flex border-0 align-items-center list-group-item-action waves-effect font-9\">\n          <i class=\"material-icons mr-3 font-12\">dashboard</i>Dashboard\n      </a>\n      <a routerLink=\"admin/car\" [ngClass]=\"[secondSegment == 'car' ? 'primary-text-color' : '']\" class=\"list-group-item rounded d-flex border-0 align-items-center list-group-item-action waves-effect font-9\">\n          <i class=\"material-icons mr-3 font-12\">directions_car</i>Car info\n      </a>\n      <a routerLink=\"admin/users\" [ngClass]=\"[secondSegment == 'users' ? 'primary-text-color' : '']\" class=\"list-group-item rounded d-flex border-0 align-items-center list-group-item-action waves-effect font-9\">\n          <i class=\"material-icons mr-3 font-12\">group</i>Users\n      </a>\n      <!-- <a routerLink=\"admin/static-pages\" [ngClass]=\"[secondSegment == 'static-pages' ? 'primary-text-color' : '']\" class=\"list-group-item rounded d-flex border-0 align-items-center list-group-item-action waves-effect font-9\">\n          <i class=\"material-icons mr-3 font-12\">settings</i>Static Pages\n      </a> -->\n  </div>\n</div> "

/***/ }),

/***/ "./src/app/admin-sidebar/admin-sidebar.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/admin-sidebar/admin-sidebar.component.ts ***!
  \**********************************************************/
/*! exports provided: AdminSidebarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminSidebarComponent", function() { return AdminSidebarComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "../node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _common_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../common.service */ "./src/app/common.service.ts");




var AdminSidebarComponent = /** @class */ (function () {
    function AdminSidebarComponent(router, commonservice) {
        var _this = this;
        this.router = router;
        this.commonservice = commonservice;
        this.navigationSubscription = this.router.events.subscribe(function (e) {
            // If it is a NavigationEnd event re-initalise the component
            if (e instanceof _angular_router__WEBPACK_IMPORTED_MODULE_2__["NavigationEnd"]) {
                _this.ngOnInit();
            }
        });
    }
    AdminSidebarComponent.prototype.ngOnInit = function () {
        this.secondSegment = this.commonservice.getsecond_index();
    };
    AdminSidebarComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-admin-sidebar',
            template: __webpack_require__(/*! ./admin-sidebar.component.html */ "./src/app/admin-sidebar/admin-sidebar.component.html"),
            styles: [__webpack_require__(/*! ./admin-sidebar.component.css */ "./src/app/admin-sidebar/admin-sidebar.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _common_service__WEBPACK_IMPORTED_MODULE_3__["CommonService"]])
    ], AdminSidebarComponent);
    return AdminSidebarComponent;
}());



/***/ }),

/***/ "./src/app/admin-update-image-category/admin-update-image-category.component.css":
/*!***************************************************************************************!*\
  !*** ./src/app/admin-update-image-category/admin-update-image-category.component.css ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhbmd1bGFyX2FwcC9zcmMvYXBwL2FkbWluLXVwZGF0ZS1pbWFnZS1jYXRlZ29yeS9hZG1pbi11cGRhdGUtaW1hZ2UtY2F0ZWdvcnkuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/admin-update-image-category/admin-update-image-category.component.html":
/*!****************************************************************************************!*\
  !*** ./src/app/admin-update-image-category/admin-update-image-category.component.html ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"col-12 p-0 mt-3\" [ngClass]=\"{h100:thispageLoad}\">\n  <div class=\"container col-md-9 mx-auto p-3\" [ngClass]=\"{h100:thispageLoad}\">\n    <div class=\"d-flex justify-content-center align-items-center h-100\" *ngIf=\"thispageLoad\">\n        <mat-progress-spinner [color]=\"spinner_color\" [mode]=\"spinner_mode\" [diameter]=50></mat-progress-spinner>\n    </div>\n    <div class=\"col-12  white z-depth-1 p-0 text-center pb-5\" *ngIf=\"!showForm\">\n      <div class=\"no_data_image\"></div>\n      <h4>Oops! Record is missing.</h4>\n      <small><p>This record must be delete or Unavailable</p></small>\n      <button type=\"button\" class=\"btn rounded black-text grey lighten-4 p-3 mr-3\" routerLink=\"/admin/image_category\" mat-button>Go Back</button>\n    </div>\n    <div class=\"col-12  white z-depth-1\" *ngIf=\"showForm\">\n      <div class=\"d-flex align-items-center f-wrap\">\n        <h4 class=\"bold_font w-100 mb-0 col-md-7 col-sm-5 my-3 pl-0\">{{page_title}}</h4>\n      </div>\n      <div class=\"col-12 p-0\">\n        <div class=\"d-flex row mb-3 pb-3\">\n          <div class=\"col-sm-6\">\n              <mat-form-field appearance=\"outline\" class=\"w-100\"> \n                <mat-label>Name</mat-label> \t\t \t\t\t\n                <input type=\"text\" matInput [(ngModel)]=\"name\" placeholder=\"Name\" autocomplete=\"off\" name=\"\" class=\"\" />\n              </mat-form-field>\n          </div>\n          <div class=\"col-sm-6\">\n            <mat-form-field appearance=\"outline\" class=\"w-100\"> \n              <mat-label>Status</mat-label> \t\n              <mat-select [(value)]=\"status\" class=\"\">\n                <mat-option value=\"active\">Active</mat-option>\n                <mat-option value=\"inactive\">In active</mat-option>\n              </mat-select>\n            </mat-form-field>\n          </div>\n          <div class=\"col-sm-12 mt-3 d-flex\">\n              <button type=\"button\" class=\"btn rounded black-text grey lighten-4 l-h-normal p-3 mr-3\" routerLink=\"/admin/image_category\" mat-button>Cancel</button>\n              <mat-spinner-button class=\"btn orange darken-3 l-h-normal ml-auto p-3 m-0 rounded\" (btnClick)=\"update_image_category()\" [options]=\"btnOpts\"></mat-spinner-button>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/admin-update-image-category/admin-update-image-category.component.ts":
/*!**************************************************************************************!*\
  !*** ./src/app/admin-update-image-category/admin-update-image-category.component.ts ***!
  \**************************************************************************************/
/*! exports provided: AdminUpdateImageCategoryComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminUpdateImageCategoryComponent", function() { return AdminUpdateImageCategoryComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "../node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "../node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _rest_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../rest.service */ "./src/app/rest.service.ts");
/* harmony import */ var _common_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../common.service */ "./src/app/common.service.ts");






var AdminUpdateImageCategoryComponent = /** @class */ (function () {
    function AdminUpdateImageCategoryComponent(rest, router, snackBar, commonservice) {
        this.rest = rest;
        this.router = router;
        this.snackBar = snackBar;
        this.commonservice = commonservice;
        this.btnOpts = {
            active: false,
            text: 'Submit',
            spinnerSize: 18,
            raised: true,
            stroked: false,
            spinnerColor: 'accent',
            fullWidth: false,
            disabled: false,
            mode: 'indeterminate',
        };
        this.name = "";
        this.status = "active";
        this.id = "";
        this.showForm = true;
        this.thispageLoad = false;
        this.thisEditpage = false;
    }
    AdminUpdateImageCategoryComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.returnUrl = 'admin/image_category';
        this.page_title = "Add Image Category";
        if (this.commonservice.getthird_index() == "edit") {
            this.page_title = "Update Image Category";
            this.thisEditpage = true;
            this.thispageLoad = true;
            var params = {
                'id': this.commonservice.urlFourthSegment
            };
            var data = JSON.stringify(params);
            this.rest.post('get_image_category_detail', data).subscribe(function (response) {
                if (response.status) {
                    _this.showForm = true;
                    _this.name = response.data.admin_image_category.name;
                    _this.status = response.data.admin_image_category.status;
                    _this.id = response.data.admin_image_category.id;
                }
                else {
                    _this.showForm = false;
                }
                _this.thispageLoad = false;
            });
        }
    };
    AdminUpdateImageCategoryComponent.prototype.update_image_category = function () {
        var _this = this;
        if (this.name == "") {
            this.snackBar.open('Enter a image category name', '', {
                duration: 2000,
                panelClass: ['orange-text']
            });
        }
        else {
            this.btnOpts.active = true;
            this.btnOpts.text = "Saving...";
            var params = {
                'id': this.id,
                'name': this.name,
                'status': this.status,
                'type': this.thisEditpage
            };
            var data = JSON.stringify(params);
            this.rest.post('add_image_category', data).subscribe(function (response) {
                if (response.status) {
                    _this.btnOpts.active = false;
                    _this.btnOpts.text = "Submit";
                    _this.snackBar.open(response.message, '', {
                        duration: 2000,
                        panelClass: ['green-text']
                    });
                    _this.router.navigateByUrl(_this.returnUrl);
                }
                else {
                    _this.snackBar.open(response.message, '', {
                        duration: 2000,
                        panelClass: ['orange-text']
                    });
                }
            });
        }
    };
    AdminUpdateImageCategoryComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-admin-update-image-category',
            template: __webpack_require__(/*! ./admin-update-image-category.component.html */ "./src/app/admin-update-image-category/admin-update-image-category.component.html"),
            styles: [__webpack_require__(/*! ./admin-update-image-category.component.css */ "./src/app/admin-update-image-category/admin-update-image-category.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_rest_service__WEBPACK_IMPORTED_MODULE_4__["RestService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSnackBar"], _common_service__WEBPACK_IMPORTED_MODULE_5__["CommonService"]])
    ], AdminUpdateImageCategoryComponent);
    return AdminUpdateImageCategoryComponent;
}());



/***/ }),

/***/ "./src/app/admin-users/admin-users.component.css":
/*!*******************************************************!*\
  !*** ./src/app/admin-users/admin-users.component.css ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhbmd1bGFyX2FwcC9zcmMvYXBwL2FkbWluLXVzZXJzL2FkbWluLXVzZXJzLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/admin-users/admin-users.component.html":
/*!********************************************************!*\
  !*** ./src/app/admin-users/admin-users.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"col-12 p-0 mt-5\">\n    <div class=\"container p-0\">\n      <div class=\"row m-0\">\n        <div class=\"col-12\" id=\"no-more-tables\">\n          <div class=\"col-12 px-0 border-1 mb-3 rounded white\">\n            <div class=\"row m-0 col-12 align-items-center p-0\">\n              <div class=\"d-inline-block\">\n                <h5 class=\"dark-text-color m-0 p-3\">User Management</h5>\n              </div>\n            </div>\n            <table mat-table [dataSource]=\"data\" class=\"mat-elevation-z0 w-100\">\n              <ng-container matColumnDef=\"name\">\n                <th mat-header-cell *matHeaderCellDef class=\"head grey lighten-4 sub-text-color\"> Fullname </th>\n                <td data-title=\"Fullname\" mat-cell *matCellDef=\"let element\"\n                  class=\"py-2 align-items-center dark-text-color border-0\">\n                  <p class=\"mb-1 dark-text-color\">{{element.fullName}}</p>\n                </td>\n              </ng-container>\n              <ng-container matColumnDef=\"email\">\n                <th mat-header-cell *matHeaderCellDef class=\"head w-70 grey lighten-4 sub-text-color\"> Email </th>\n                <td data-title=\"Email\" mat-cell *matCellDef=\"let element\"\n                  class=\"py-2 align-items-center page_content dark-text-color border-0\">\n                  <p class=\"mb-1 dark-text-color\">{{element.email}}</p>\n                </td>\n              </ng-container>\n              <ng-container matColumnDef=\"licence\">\n                <th mat-header-cell *matHeaderCellDef class=\"head w-70 grey lighten-4 sub-text-color\"> Licence </th>\n                <td data-title=\"Licence\" mat-cell *matCellDef=\"let element\"\n                  class=\"py-2 align-items-center page_content dark-text-color border-0\">\n                  <p class=\"mb-1 dark-text-color\">\n                      <img src=\"{{element.licencephoto}}\" class=\"w-50x\" />\n                  </p>\n                </td>\n              </ng-container>\n              <ng-container matColumnDef=\"national\">\n                <th mat-header-cell *matHeaderCellDef class=\"head w-70 grey lighten-4 sub-text-color\"> National ID </th>\n                <td data-title=\"National ID\" mat-cell *matCellDef=\"let element\"\n                  class=\"py-2 align-items-center page_content dark-text-color border-0\">\n                  <p class=\"mb-1 dark-text-color\">\n                      <img src=\"{{element.nationalphoto}}\" class=\"w-50x\" />\n                  </p>\n                </td>\n              </ng-container>\n              <ng-container matColumnDef=\"active\">\n                <th mat-header-cell *matHeaderCellDef class=\"head w-70 grey lighten-4 sub-text-color\"> Active </th>\n                <td data-title=\"Model name\" mat-cell *matCellDef=\"let element\"\n                  class=\"py-2 align-items-center page_content dark-text-color border-0\">\n                  <p class=\"mb-1 dark-text-color\">\n                      <mat-slide-toggle class=\"my-2\" [(ngModel)]=\"element.is_active\" (change)=\"change_active(element?.id, element?.is_active)\"></mat-slide-toggle>\n                  </p>\n                </td>\n              </ng-container>\n              <ng-container matColumnDef=\"action\">\n                <th mat-header-cell *matHeaderCellDef class=\"head grey lighten-4 sub-text-color\"> Action </th>\n                <td data-title=\"Action\" class=\"py-2 border-0\" mat-cell *matCellDef=\"let element\">\n                  <button type=\"button\" matTooltip=\"Delete\" matTooltipPosition=\"above\"\n                    class=\"p-0 outline-0 bold_font m-w-auto\" (click)=\"deleteConfirmation(deleteModal,element._id)\"\n                    mat-button><i class=\"material-icons primary-text-color font-11\">delete</i></button>\n                </td>\n              </ng-container>\n              <tr mat-header-row class=\"border-top\" *matHeaderRowDef=\"displayedColumns\">\n                <td>asdf</td>\n              </tr>\n              <tr class=\"bdr-btm\" mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\n            </table>\n            <div class=\"text-center p-3\" *ngIf=\"data?.length === 0\">No records found</div>\n            <nav aria-label=\"Page navigation example\" class=\"mt-5\">\n              <ul *ngIf=\"pages && pages.length > 1\" class=\"pagination justify-content-end f-wrap\">\n                <li class=\"page-item\" [ngClass]=\"{disabled:current === 1}\">\n                  <a class=\"page-link\" mdbWavesEffect (click)=\"get_users(1)\"><i\n                      class=\"material-icons\">first_page</i></a>\n                </li>\n                <li class=\"page-item\" [ngClass]=\"{disabled:current === 1}\">\n                  <a class=\"page-link\" mdbWavesEffect (click)=\"get_users(current - 1)\"><i\n                      class=\"material-icons\">navigate_before</i></a>\n                </li>\n                <li class=\"page-item mt-1\" *ngFor=\"let page of pages\" [ngClass]=\"{active:current === page}\">\n                  <a class=\"page-link\" mdbWavesEffect (click)=\"get_users(page)\">{{page}}</a>\n                </li>\n                <li class=\"page-item\" [ngClass]=\"{disabled:current === totalPages}\">\n                  <a class=\"page-link\" mdbWavesEffect (click)=\"get_users(current + 1)\"><i\n                      class=\"material-icons\">navigate_next</i></a>\n                </li>\n                <li class=\"page-item\" [ngClass]=\"{disabled:current === totalPages}\">\n                  <a class=\"page-link\" mdbWavesEffect (click)=\"get_users(totalPages)\"><i\n                      class=\"material-icons\">last_page</i></a>\n                </li>\n              </ul>\n            </nav>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n  <ng-template #deleteModal>\n    <h4 class=\"mt-2\">Delete User</h4>\n    <p>Are you sure you want to delete this User?</p>\n    <div class=\"d-flex\">\n      <button type=\"button\" class=\"btn rounded grey lighten-2 mr-3 p-3 black-text l-h-normal bold_font\" mat-button\n        (click)=closeConfirmation()>Cancel</button>\n      <button type=\"button\" [ngClass]=\"{disabled:delete_btn_loading}\"\n        class=\"btn rounded py-3 px-4 text-uppercase elegant-color-dark ml-auto white-text bold_font l-h-normal font-9\"\n        mat-button (click)=deleteUser()>\n        <mat-progress-spinner *ngIf=\"delete_btn_loading\" [color]=\"spinner_color\" [mode]=\"spinner_mode\"\n          [diameter]=\"spinner_value\"></mat-progress-spinner>\n        <span *ngIf=\"!delete_btn_loading\">Delete</span>\n      </button>\n    </div>\n  </ng-template>"

/***/ }),

/***/ "./src/app/admin-users/admin-users.component.ts":
/*!******************************************************!*\
  !*** ./src/app/admin-users/admin-users.component.ts ***!
  \******************************************************/
/*! exports provided: AdminUsersComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminUsersComponent", function() { return AdminUsersComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../rest.service */ "./src/app/rest.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "../node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material */ "../node_modules/@angular/material/esm5/material.es5.js");





var AdminUsersComponent = /** @class */ (function () {
    function AdminUsersComponent(rest, bottomSheet, router, snackBar) {
        this.rest = rest;
        this.bottomSheet = bottomSheet;
        this.router = router;
        this.snackBar = snackBar;
        this.displayedColumns = ['name', 'email', 'licence', 'national', 'active', 'action'];
        this.data = [];
        this.spinner_color = 'warn';
        this.spinner_mode = 'indeterminate';
        this.spinner_value = 20;
        this.page = 1;
        this.current = 1;
        this.totalDocs = 0;
        this.totalPages = 0;
        this.startPage = 1;
        this.endPage = 1;
        this.deleteId = "";
        this.delete_btn_loading = false;
    }
    AdminUsersComponent.prototype.deleteConfirmation = function (templateRef, id) {
        this.deleteId = id;
        var dialogRef = this.bottomSheet.open(templateRef, {
            panelClass: 'delete-confirmation-width'
            // data: { name: this.name, animal: this.animal }
        });
    };
    AdminUsersComponent.prototype.closeConfirmation = function () {
        this.bottomSheet.dismiss();
    };
    AdminUsersComponent.prototype.deleteUser = function () {
        var _this = this;
        this.delete_btn_loading = true;
        var params = {
            'id': this.deleteId,
            'status': false
        };
        var data = JSON.stringify(params);
        this.rest.post('update_user', data).subscribe(function (response) {
            if (response.status) {
                _this.snackBar.open(response.message, '', {
                    duration: 2000,
                    panelClass: 'green-text'
                });
            }
            else {
                _this.snackBar.open(response.message, '', {
                    duration: 2000,
                    panelClass: 'orange-text'
                });
            }
            _this.delete_btn_loading = false;
            _this.deleteId = "";
            _this.get_users(_this.page);
            _this.bottomSheet.dismiss();
        });
    };
    AdminUsersComponent.prototype.get_users = function (page) {
        var _this = this;
        var params = {
            'page': page
        };
        this.rest.post('get_users', JSON.stringify(params)).subscribe(function (response) {
            _this.data = response.data.user.docs;
            _this.current = response.data.user.page;
            _this.totalDocs = response.data.user.totalDocs;
            _this.totalPages = response.data.user.totalPages;
            if (_this.totalPages <= 5) {
                _this.startPage = 1;
                _this.endPage = _this.totalPages;
            }
            else {
                if (_this.current <= 3) {
                    _this.startPage = 1;
                    _this.endPage = 5;
                }
                else if (_this.current + 2 >= _this.totalPages) {
                    _this.startPage = _this.totalPages - 4;
                    _this.endPage = _this.totalPages;
                }
                else {
                    _this.startPage = _this.current - 2;
                    _this.endPage = _this.current + 2;
                }
            }
            _this.pages = Array.from(Array((_this.endPage + 1) - _this.startPage).keys()).map(function (i) { return _this.startPage + i; });
        });
    };
    AdminUsersComponent.prototype.ngOnInit = function () {
        this.data = [];
        this.get_users(this.page);
    };
    AdminUsersComponent.prototype.change_active = function (user_id, status) {
        var _this = this;
        var params = {
            'id': user_id,
            'is_active': status
        };
        var data = JSON.stringify(params);
        this.rest.post('update_user', data).subscribe(function (response) {
            if (response.status) {
                _this.snackBar.open(response.message, '', {
                    duration: 2000,
                    panelClass: 'green-text'
                });
            }
            else {
                _this.snackBar.open(response.message, '', {
                    duration: 2000,
                    panelClass: 'orange-text'
                });
            }
        });
    };
    AdminUsersComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-admin-users',
            template: __webpack_require__(/*! ./admin-users.component.html */ "./src/app/admin-users/admin-users.component.html"),
            styles: [__webpack_require__(/*! ./admin-users.component.css */ "./src/app/admin-users/admin-users.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_rest_service__WEBPACK_IMPORTED_MODULE_2__["RestService"], _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatBottomSheet"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSnackBar"]])
    ], AdminUsersComponent);
    return AdminUsersComponent;
}());



/***/ }),

/***/ "./src/app/adminauth.guard.ts":
/*!************************************!*\
  !*** ./src/app/adminauth.guard.ts ***!
  \************************************/
/*! exports provided: AdminauthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminauthGuard", function() { return AdminauthGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "../node_modules/@angular/router/fesm5/router.js");



var AdminauthGuard = /** @class */ (function () {
    function AdminauthGuard(router) {
        this.router = router;
    }
    AdminauthGuard.prototype.canActivate = function (next, state) {
        var url = state.url;
        return this.adminLogin(url);
    };
    AdminauthGuard.prototype.adminLogin = function (url) {
        var logStatus = this.isAdminLogged();
        if (!logStatus) {
            this.router.navigate(['admin/login']);
            return false;
        }
        else if (logStatus) {
            return true;
        }
    };
    AdminauthGuard.prototype.isAdminLogged = function () {
        var status = false;
        if (localStorage.getItem('isAdminLogged') == "true") {
            status = true;
        }
        else {
            status = false;
        }
        return status;
    };
    AdminauthGuard = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], AdminauthGuard);
    return AdminauthGuard;
}());



/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "../node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _adminauth_guard__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./adminauth.guard */ "./src/app/adminauth.guard.ts");
/* harmony import */ var _userauth_guard__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./userauth.guard */ "./src/app/userauth.guard.ts");
/* harmony import */ var _admin_login_admin_login_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./admin-login/admin-login.component */ "./src/app/admin-login/admin-login.component.ts");
/* harmony import */ var _admin_dashboard_admin_dashboard_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./admin-dashboard/admin-dashboard.component */ "./src/app/admin-dashboard/admin-dashboard.component.ts");
/* harmony import */ var _admin_car_admin_car_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./admin-car/admin-car.component */ "./src/app/admin-car/admin-car.component.ts");
/* harmony import */ var _admin_car_update_admin_car_update_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./admin-car-update/admin-car-update.component */ "./src/app/admin-car-update/admin-car-update.component.ts");
/* harmony import */ var _admin_image_category_admin_image_category_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./admin-image-category/admin-image-category.component */ "./src/app/admin-image-category/admin-image-category.component.ts");
/* harmony import */ var _admin_update_image_category_admin_update_image_category_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./admin-update-image-category/admin-update-image-category.component */ "./src/app/admin-update-image-category/admin-update-image-category.component.ts");
/* harmony import */ var _admin_make_admin_make_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./admin-make/admin-make.component */ "./src/app/admin-make/admin-make.component.ts");
/* harmony import */ var _admin_make_update_admin_make_update_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./admin-make-update/admin-make-update.component */ "./src/app/admin-make-update/admin-make-update.component.ts");
/* harmony import */ var _admin_users_admin_users_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./admin-users/admin-users.component */ "./src/app/admin-users/admin-users.component.ts");
/* harmony import */ var _homepage_homepage_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./homepage/homepage.component */ "./src/app/homepage/homepage.component.ts");
/* harmony import */ var _signup_signup_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./signup/signup.component */ "./src/app/signup/signup.component.ts");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./dashboard/dashboard.component */ "./src/app/dashboard/dashboard.component.ts");
/* harmony import */ var _cardetails_cardetails_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./cardetails/cardetails.component */ "./src/app/cardetails/cardetails.component.ts");
/* harmony import */ var _search_search_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./search/search.component */ "./src/app/search/search.component.ts");




















var routes = [
    { path: '', component: _homepage_homepage_component__WEBPACK_IMPORTED_MODULE_14__["HomepageComponent"] },
    { path: 'register', component: _signup_signup_component__WEBPACK_IMPORTED_MODULE_15__["SignupComponent"] },
    { path: 'login', component: _login_login_component__WEBPACK_IMPORTED_MODULE_16__["LoginComponent"] },
    { path: 'dashboard', component: _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_17__["DashboardComponent"], canActivate: [_userauth_guard__WEBPACK_IMPORTED_MODULE_4__["UserauthGuard"]] },
    { path: 'car/:id', component: _cardetails_cardetails_component__WEBPACK_IMPORTED_MODULE_18__["CardetailsComponent"] },
    { path: 'search', component: _search_search_component__WEBPACK_IMPORTED_MODULE_19__["SearchComponent"] },
    { path: 'admin/login', component: _admin_login_admin_login_component__WEBPACK_IMPORTED_MODULE_5__["AdminLoginComponent"] },
    { path: 'admin/dashboard', component: _admin_dashboard_admin_dashboard_component__WEBPACK_IMPORTED_MODULE_6__["AdminDashboardComponent"], canActivate: [_adminauth_guard__WEBPACK_IMPORTED_MODULE_3__["AdminauthGuard"]] },
    { path: 'admin/car', component: _admin_car_admin_car_component__WEBPACK_IMPORTED_MODULE_7__["AdminCarComponent"], canActivate: [_adminauth_guard__WEBPACK_IMPORTED_MODULE_3__["AdminauthGuard"]] },
    { path: 'admin/car/add', component: _admin_car_update_admin_car_update_component__WEBPACK_IMPORTED_MODULE_8__["AdminCarUpdateComponent"], canActivate: [_adminauth_guard__WEBPACK_IMPORTED_MODULE_3__["AdminauthGuard"]] },
    { path: 'admin/car/edit/:id', component: _admin_car_update_admin_car_update_component__WEBPACK_IMPORTED_MODULE_8__["AdminCarUpdateComponent"], canActivate: [_adminauth_guard__WEBPACK_IMPORTED_MODULE_3__["AdminauthGuard"]] },
    { path: 'admin/image_category', component: _admin_image_category_admin_image_category_component__WEBPACK_IMPORTED_MODULE_9__["AdminImageCategoryComponent"], canActivate: [_adminauth_guard__WEBPACK_IMPORTED_MODULE_3__["AdminauthGuard"]] },
    { path: 'admin/image_category/add', component: _admin_update_image_category_admin_update_image_category_component__WEBPACK_IMPORTED_MODULE_10__["AdminUpdateImageCategoryComponent"], canActivate: [_adminauth_guard__WEBPACK_IMPORTED_MODULE_3__["AdminauthGuard"]] },
    { path: 'admin/image_category/edit/:id', component: _admin_update_image_category_admin_update_image_category_component__WEBPACK_IMPORTED_MODULE_10__["AdminUpdateImageCategoryComponent"], canActivate: [_adminauth_guard__WEBPACK_IMPORTED_MODULE_3__["AdminauthGuard"]] },
    { path: 'admin/make', component: _admin_make_admin_make_component__WEBPACK_IMPORTED_MODULE_11__["AdminMakeComponent"], canActivate: [_adminauth_guard__WEBPACK_IMPORTED_MODULE_3__["AdminauthGuard"]] },
    { path: 'admin/make/add', component: _admin_make_update_admin_make_update_component__WEBPACK_IMPORTED_MODULE_12__["AdminMakeUpdateComponent"], canActivate: [_adminauth_guard__WEBPACK_IMPORTED_MODULE_3__["AdminauthGuard"]] },
    { path: 'admin/make/edit/:id', component: _admin_make_update_admin_make_update_component__WEBPACK_IMPORTED_MODULE_12__["AdminMakeUpdateComponent"], canActivate: [_adminauth_guard__WEBPACK_IMPORTED_MODULE_3__["AdminauthGuard"]] },
    { path: 'admin/users', component: _admin_users_admin_users_component__WEBPACK_IMPORTED_MODULE_13__["AdminUsersComponent"], canActivate: [_adminauth_guard__WEBPACK_IMPORTED_MODULE_3__["AdminauthGuard"]] }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"firstsegment != 'admin'\" [ngClass]=\"[firstsegment == 'reset-password' ? 'h-100' : '', firstsegment == 'forgot-password' ? 'h-100': '']\">\n    <div class=\"list-right fixed-top\"></div>\n    <div class=\"h-100vh\" *ngIf=\"firstsegment != 'reset-password'\">\n        <mat-toolbar class=\"bg-transparent t-0 z-index-3 top-nav-collapse px-md-3 w-100\" [ngClass]=\"[router.url == '/' ? 'position-absolute top_header z-depth-0' : 'header position-fixed z-depth-1',router.url == '/login' ? 'white' : '',router.url == '/register' ? 'white' : '']\">\n            <mat-toolbar-row>\n                <span><a class=\"navbar-brand bold_font font-14 primary-text-color\" routerLink=\"/\">Car Lease</a></span>\n                <button mat-icon-button class=\"text-black d-md-none ml-auto\" (click)=\"sidenav.toggle()\">\n                    <mat-icon>menu</mat-icon>\n                </button>\n                <span class=\"menu-spacer\"></span>\n                <div class=\"d-md-block d-none ml-auto\">\n                    <!-- The following menu items will be hidden on both SM and XS screen sizes -->\n                    <a mat-button routerLinkActive=\"active\" routerLink=\"register\" class=\"nav-link font-8 lh-60\" [ngClass]=\"router.url == '/' ? 'text-white' : 'text-muted'\"  *ngIf=\"userLoggedin == 'false' || userLoggedin == null\">Sign up</a> \n                    <a mat-button routerLinkActive=\"active\" routerLink=\"login\" class=\"nav-link font-8 lh-60\" [ngClass]=\"router.url == '/' ? 'text-white' : 'text-muted'\" *ngIf=\"userLoggedin == 'false' || userLoggedin == null\">Log in</a> \n                    <span class=\"nav-item py-0 d-inline-block position-relative\" *ngIf=\"userLoggedin == 'true'\" mdbDropdown>\n                        <a class=\"nav-link font-1 py-3 font-1 text-left\" mat-button mdbDropdownToggle>\n                        <img src=\"{{user.original_profile}}\" id=\"user_profile_image\" class=\"rounded-circle w-30x o-cover\"></a>\n                        <div class=\"dropdown-menu dropdown-primary r-0 l-auto p-0\">\n                            <a class=\"dropdown-item py-2 px-3 font-9\" routerLink=\"dashboard\">Dashboard</a>\n                            <a class=\"dropdown-item py-2 px-3 font-9\" (click)=\"userLoggedOut()\">Logout</a>\n                        </div>\n                    </span>\n                </div>\n            </mat-toolbar-row>\n        </mat-toolbar>\n        \n        <mat-sidenav-container class=\"z-index-auto\" [ngClass]=\"[firstsegment == 'reset-password' ? 'h-100' : '', firstsegment == 'forgot-password' ? 'h-100': '']\">\n            <mat-sidenav #sidenav class=\"position-fixed w-100 mw-250\">\n                <mat-nav-list>\n                    <a mat-list-item routerLinkActive=\"active\" (click)=\"sidenav.toggle()\" routerLink=\"register\" class=\"nav-link font-1 lh-60\"  *ngIf=\"userLoggedin == 'false' || userLoggedin == null\">Sign up</a> \n                    <a mat-list-item routerLinkActive=\"active\" (click)=\"sidenav.toggle()\" routerLink=\"login\" class=\"nav-link font-1 lh-60\"  *ngIf=\"userLoggedin == 'false' || userLoggedin == null\">Log in</a> \n                    <span class=\"nav-item py-0 d-block\" *ngIf=\"userLoggedin == 'true'\">\n                        <span class=\"d-block p-2 rgba-stylish-slight text-capitalize break-word\">\n                            <img src=\"{{user.original_profile}}\" id=\"user_profile_image\" class=\"mr-2 rounded-circle w-30x o-cover\">\n                            <span>{{user.fullName}}</span>\n                        </span>\n                        <mat-list class=\"p-0\">\n                            <a mat-list-item routerLinkActive=\"active\" (click)=\"sidenav.toggle()\" routerLink=\"/dashboard\" class=\"font-9\">Dashboard</a>\n                            <a mat-list-item (click)=\"userLoggedOut()\" class=\"font-9\"><span>Logout</span></a>\n                        </mat-list>\n                    </span>\n                </mat-nav-list>\n            </mat-sidenav>\n            <mat-sidenav-content class=\"white z-index-auto\">                \n                <app-listprogress *ngIf=\"firstsegment == 'listyourspace' && thirdsegment != 'edit' && thirdsegment != undefined\"></app-listprogress>   \n                <router-outlet></router-outlet>\n                <app-footer [ngClass]=\"[firstsegment == 'rooms' ? 'showfooter' : '', firstsegment == 'contact' ? 'showfooter': '', firstsegment == 'reservation' ? 'showfooter': '']\" *ngIf=\"firstsegment != 'listyourspace'\"></app-footer>\n            </mat-sidenav-content>\n        </mat-sidenav-container>\n    </div> \n</div>\n<div *ngIf=\"firstsegment == 'admin'\" class=\"h-100\">\n  <div class=\"example-container h-100\" [class.example-is-mobile]=\"mobileQuery.matches\">\n      <mat-toolbar color=\"primary\" *ngIf=\"commonservice.admin_login=='true'\"  [class.sidebar_opened]=\"snav.opened\" class=\"example-toolbar orange darken-3\">\n          <h1 class=\"example-app-name bold_font\">{{ title }}</h1>\n          <button mat-icon-button class=\"outline-0\" #snavbtn (click)=\"snav.toggle()\"><mat-icon>menu</mat-icon></button>\n          <div class=\"ml-auto mr-3 row m-0\">\n              <a class=\"white-text d-inline-flex px-2\" routerLink=\"admin/profile\"><i class=\"material-icons cursor-pointer\">account_circle</i></a>\n              <a class=\"white-text d-inline-flex px-2\" (click)=\"admin_logout()\"><i class=\"material-icons cursor-pointer\">power_settings_new</i></a>\n          </div>\n      </mat-toolbar>\n      \n      <mat-sidenav-container class=\"example-sidenav-container h-100\" [ngClass]=\"[secondsegment == 'login' ? 'white' : '']\" [style.marginTop.px]=\"mobileQuery.matches ? 56 : 0\">\n          <mat-sidenav #snav [opened]=\"mobileQuery.matches ? 'false' : 'true'\" class=\"admin-sidenav\" [ngClass]=\"[secondsegment == 'login' ? 'd-none' : '']\" [mode]=\"mobileQuery.matches ? 'over' : 'side'\" [fixedInViewport]=\"mobileQuery.matches\" fixedTopGap=\"56\">\n            <app-admin-sidebar></app-admin-sidebar>\n          </mat-sidenav>\n          <mat-sidenav-content [ngClass]=\"[secondsegment == 'login' ? 'ml-0' : '']\">\n              <router-outlet></router-outlet>\n          </mat-sidenav-content>\n      </mat-sidenav-container>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example-container {\n  display: flex;\n  flex-direction: column;\n  position: absolute;\n  top: 0;\n  bottom: 0;\n  left: 0;\n  right: 0; }\n\n.admin-sidenav {\n  width: 230px; }\n\n.example-is-mobile .example-toolbar {\n  position: fixed;\n  /* Make sure the toolbar will stay on top of the content as it scrolls past. */\n  z-index: 2; }\n\n.example-toolbar {\n  transition: all 200ms ease-in-out 0.2s; }\n\nh1.example-app-name {\n  margin-left: 8px; }\n\n.example-sidenav-container {\n  /* When the sidenav is not fixed, stretch the sidenav container to fill the available space. This\n    causes `<mat-sidenav-content>` to act as our scrolling element for desktop layouts. */\n  flex: 1; }\n\n.example-is-mobile .example-sidenav-container {\n  /* When the sidenav is fixed, don't constrain the height of the sidenav container. This allows the\n    `<body>` to be our scrolling element for mobile layouts. */\n  flex: 1 0 auto; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2FiZHVsL3Byb2plY3RzL2NhcmxlYXNlL2FuZ3VsYXJfYXBwL3NyYy9hcHAvYXBwLmNvbXBvbmVudC5zY3NzIiwiYW5ndWxhcl9hcHAvc3JjL2FwcC9hcHAuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxhQUFhO0VBQ2Isc0JBQXNCO0VBQ3RCLGtCQUFrQjtFQUNsQixNQUFNO0VBQ04sU0FBUztFQUNULE9BQU87RUFDUCxRQUFRLEVBQUE7O0FBRVo7RUFFSSxZQUFXLEVBQUE7O0FBRWY7RUFDSSxlQUFlO0VBQ2YsOEVBQUE7RUFDQSxVQUFVLEVBQUE7O0FBRWQ7RUFLSSxzQ0FBc0MsRUFBQTs7QUFFMUM7RUFDSSxnQkFBZ0IsRUFBQTs7QUFHcEI7RUFDQTt5RkNGeUY7RURJckYsT0FBTyxFQUFBOztBQUdYO0VBQ0E7OERDSDhEO0VESzFELGNBQWMsRUFBQSIsImZpbGUiOiJhbmd1bGFyX2FwcC9zcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5leGFtcGxlLWNvbnRhaW5lciB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6IDA7XG4gICAgYm90dG9tOiAwO1xuICAgIGxlZnQ6IDA7XG4gICAgcmlnaHQ6IDA7XG4gIH1cbi5hZG1pbi1zaWRlbmF2XG57XG4gICAgd2lkdGg6MjMwcHg7XG59XG4uZXhhbXBsZS1pcy1tb2JpbGUgLmV4YW1wbGUtdG9vbGJhciB7XG4gICAgcG9zaXRpb246IGZpeGVkO1xuICAgIC8qIE1ha2Ugc3VyZSB0aGUgdG9vbGJhciB3aWxsIHN0YXkgb24gdG9wIG9mIHRoZSBjb250ZW50IGFzIGl0IHNjcm9sbHMgcGFzdC4gKi9cbiAgICB6LWluZGV4OiAyO1xufVxuLmV4YW1wbGUtdG9vbGJhciBcbntcbiAgICAtd2Via2l0LXRyYW5zaXRpb246IGFsbCAyMDBtcyBlYXNlLWluLW91dCAwLjJzO1xuICAgIC1tb3otdHJhbnNpdGlvbjogYWxsIDIwMG1zIGVhc2UtaW4tb3V0IDAuMnM7XG4gICAgLW8tdHJhbnNpdGlvbjogYWxsIDIwMG1zIGVhc2UtaW4tb3V0IDAuMnM7XG4gICAgdHJhbnNpdGlvbjogYWxsIDIwMG1zIGVhc2UtaW4tb3V0IDAuMnM7XG59XG5oMS5leGFtcGxlLWFwcC1uYW1lIHtcbiAgICBtYXJnaW4tbGVmdDogOHB4O1xufVxuXG4uZXhhbXBsZS1zaWRlbmF2LWNvbnRhaW5lciB7XG4vKiBXaGVuIHRoZSBzaWRlbmF2IGlzIG5vdCBmaXhlZCwgc3RyZXRjaCB0aGUgc2lkZW5hdiBjb250YWluZXIgdG8gZmlsbCB0aGUgYXZhaWxhYmxlIHNwYWNlLiBUaGlzXG4gICAgY2F1c2VzIGA8bWF0LXNpZGVuYXYtY29udGVudD5gIHRvIGFjdCBhcyBvdXIgc2Nyb2xsaW5nIGVsZW1lbnQgZm9yIGRlc2t0b3AgbGF5b3V0cy4gKi9cbiAgICBmbGV4OiAxO1xufVxuXG4uZXhhbXBsZS1pcy1tb2JpbGUgLmV4YW1wbGUtc2lkZW5hdi1jb250YWluZXIge1xuLyogV2hlbiB0aGUgc2lkZW5hdiBpcyBmaXhlZCwgZG9uJ3QgY29uc3RyYWluIHRoZSBoZWlnaHQgb2YgdGhlIHNpZGVuYXYgY29udGFpbmVyLiBUaGlzIGFsbG93cyB0aGVcbiAgICBgPGJvZHk+YCB0byBiZSBvdXIgc2Nyb2xsaW5nIGVsZW1lbnQgZm9yIG1vYmlsZSBsYXlvdXRzLiAqL1xuICAgIGZsZXg6IDEgMCBhdXRvO1xufSIsIi5leGFtcGxlLWNvbnRhaW5lciB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiAwO1xuICBib3R0b206IDA7XG4gIGxlZnQ6IDA7XG4gIHJpZ2h0OiAwOyB9XG5cbi5hZG1pbi1zaWRlbmF2IHtcbiAgd2lkdGg6IDIzMHB4OyB9XG5cbi5leGFtcGxlLWlzLW1vYmlsZSAuZXhhbXBsZS10b29sYmFyIHtcbiAgcG9zaXRpb246IGZpeGVkO1xuICAvKiBNYWtlIHN1cmUgdGhlIHRvb2xiYXIgd2lsbCBzdGF5IG9uIHRvcCBvZiB0aGUgY29udGVudCBhcyBpdCBzY3JvbGxzIHBhc3QuICovXG4gIHotaW5kZXg6IDI7IH1cblxuLmV4YW1wbGUtdG9vbGJhciB7XG4gIC13ZWJraXQtdHJhbnNpdGlvbjogYWxsIDIwMG1zIGVhc2UtaW4tb3V0IDAuMnM7XG4gIC1tb3otdHJhbnNpdGlvbjogYWxsIDIwMG1zIGVhc2UtaW4tb3V0IDAuMnM7XG4gIC1vLXRyYW5zaXRpb246IGFsbCAyMDBtcyBlYXNlLWluLW91dCAwLjJzO1xuICB0cmFuc2l0aW9uOiBhbGwgMjAwbXMgZWFzZS1pbi1vdXQgMC4yczsgfVxuXG5oMS5leGFtcGxlLWFwcC1uYW1lIHtcbiAgbWFyZ2luLWxlZnQ6IDhweDsgfVxuXG4uZXhhbXBsZS1zaWRlbmF2LWNvbnRhaW5lciB7XG4gIC8qIFdoZW4gdGhlIHNpZGVuYXYgaXMgbm90IGZpeGVkLCBzdHJldGNoIHRoZSBzaWRlbmF2IGNvbnRhaW5lciB0byBmaWxsIHRoZSBhdmFpbGFibGUgc3BhY2UuIFRoaXNcbiAgICBjYXVzZXMgYDxtYXQtc2lkZW5hdi1jb250ZW50PmAgdG8gYWN0IGFzIG91ciBzY3JvbGxpbmcgZWxlbWVudCBmb3IgZGVza3RvcCBsYXlvdXRzLiAqL1xuICBmbGV4OiAxOyB9XG5cbi5leGFtcGxlLWlzLW1vYmlsZSAuZXhhbXBsZS1zaWRlbmF2LWNvbnRhaW5lciB7XG4gIC8qIFdoZW4gdGhlIHNpZGVuYXYgaXMgZml4ZWQsIGRvbid0IGNvbnN0cmFpbiB0aGUgaGVpZ2h0IG9mIHRoZSBzaWRlbmF2IGNvbnRhaW5lci4gVGhpcyBhbGxvd3MgdGhlXG4gICAgYDxib2R5PmAgdG8gYmUgb3VyIHNjcm9sbGluZyBlbGVtZW50IGZvciBtb2JpbGUgbGF5b3V0cy4gKi9cbiAgZmxleDogMSAwIGF1dG87IH1cbiJdfQ== */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/cdk/layout */ "../node_modules/@angular/cdk/esm5/layout.es5.js");
/* harmony import */ var _common_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./common.service */ "./src/app/common.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "../node_modules/@angular/router/fesm5/router.js");





var AppComponent = /** @class */ (function () {
    function AppComponent(router, changeDetectorRef, media, commonservice) {
        this.router = router;
        this.commonservice = commonservice;
        this.title = 'Carlease Admin';
        this.mobileQuery = media.matchMedia('(max-width: 600px)');
        this._mobileQueryListener = function () { return changeDetectorRef.detectChanges(); };
        this.mobileQuery.addListener(this._mobileQueryListener);
    }
    AppComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.router.events.subscribe(function (event) {
            if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_4__["NavigationEnd"]) {
                _this.currentUrl = event.url;
                _this.firstsegment = _this.currentUrl.split('/')[1];
                _this.secondsegment = _this.currentUrl.split('/')[2];
                _this.thirdsegment = _this.currentUrl.split('/')[3];
                _this.fourthsegment = _this.currentUrl.split('/')[4];
                _this.fifthsegment = _this.currentUrl.split('/')[5];
                _this.commonservice.urlFirstSegment = _this.firstsegment;
                _this.commonservice.urlSecondSegment = _this.secondsegment;
                _this.commonservice.urlThirdSegment = _this.thirdsegment;
                _this.commonservice.urlFourthSegment = _this.fourthsegment;
                _this.commonservice.urlFifthSegment = _this.fifthsegment;
            }
        });
        this.commonservice.admin_login = localStorage.getItem('isAdminLogged');
        this.commonservice.user_login = localStorage.getItem('isUserLogged');
        this.userLoggedin = this.commonservice.user_login;
        if (localStorage.getItem('users')) {
            this.user = JSON.parse(localStorage.getItem('users'));
            this.commonservice.user = JSON.parse(localStorage.getItem('users'));
        }
    };
    AppComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.router.events.subscribe(function (event) {
            _this.userLoggedin = _this.commonservice.user_login;
            if (localStorage.getItem('users')) {
                _this.user = JSON.parse(localStorage.getItem('users'));
            }
        });
    };
    AppComponent.prototype.ngOnDestroy = function () {
        this.mobileQuery.removeListener(this._mobileQueryListener);
    };
    AppComponent.prototype.onWindowScroll = function (event) {
        var element = document.getElementsByClassName("top-nav-collapse")[0];
        if (element && window.pageYOffset > 50) {
            element.classList.add("white_header");
        }
        else {
            element.classList.remove("white_header");
        }
    };
    AppComponent.prototype.admin_logout = function () {
        this.commonservice.admin_login = 'false';
        localStorage.setItem('isAdminLogged', 'false');
        this.router.navigateByUrl('admin/login');
    };
    AppComponent.prototype.userLoggedOut = function () {
        this.commonservice.user_login = 'false';
        this.commonservice.user = '';
        this.userLoggedin = 'false';
        localStorage.setItem('isUserLogged', 'false');
        localStorage.setItem('users', '');
        this.router.navigateByUrl('login');
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('window:scroll', ['$event']),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
    ], AppComponent.prototype, "onWindowScroll", null);
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"], _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_2__["MediaMatcher"], _common_service__WEBPACK_IMPORTED_MODULE_3__["CommonService"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "../node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/http */ "../node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "../node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "../node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/platform-browser/animations */ "../node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _cust_ext_browser_xhr__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./cust-ext-browser-xhr */ "./src/app/cust-ext-browser-xhr.ts");
/* harmony import */ var _common_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./common.service */ "./src/app/common.service.ts");
/* harmony import */ var angular_bootstrap_md__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! angular-bootstrap-md */ "../node_modules/angular-bootstrap-md/fesm5/angular-bootstrap-md.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/material */ "../node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var mat_progress_buttons__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! mat-progress-buttons */ "../node_modules/mat-progress-buttons/esm5/mat-progress-buttons.es5.js");
/* harmony import */ var ngx_file_drop__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ngx-file-drop */ "../node_modules/ngx-file-drop/fesm5/ngx-file-drop.js");
/* harmony import */ var ngx_color_sketch__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ngx-color/sketch */ "../node_modules/ngx-color/sketch/fesm5/ngx-color-sketch.js");
/* harmony import */ var ng2_img_max__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ng2-img-max */ "../node_modules/ng2-img-max/dist/ng2-img-max.js");
/* harmony import */ var ngx_owl_carousel_o__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ngx-owl-carousel-o */ "../node_modules/ngx-owl-carousel-o/fesm5/ngx-owl-carousel-o.js");
/* harmony import */ var _admin_login_admin_login_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./admin-login/admin-login.component */ "./src/app/admin-login/admin-login.component.ts");
/* harmony import */ var _admin_dashboard_admin_dashboard_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./admin-dashboard/admin-dashboard.component */ "./src/app/admin-dashboard/admin-dashboard.component.ts");
/* harmony import */ var _admin_sidebar_admin_sidebar_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./admin-sidebar/admin-sidebar.component */ "./src/app/admin-sidebar/admin-sidebar.component.ts");
/* harmony import */ var _admin_car_admin_car_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./admin-car/admin-car.component */ "./src/app/admin-car/admin-car.component.ts");
/* harmony import */ var _admin_car_update_admin_car_update_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./admin-car-update/admin-car-update.component */ "./src/app/admin-car-update/admin-car-update.component.ts");
/* harmony import */ var _admin_image_category_admin_image_category_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./admin-image-category/admin-image-category.component */ "./src/app/admin-image-category/admin-image-category.component.ts");
/* harmony import */ var _admin_update_image_category_admin_update_image_category_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./admin-update-image-category/admin-update-image-category.component */ "./src/app/admin-update-image-category/admin-update-image-category.component.ts");
/* harmony import */ var _admin_make_admin_make_component__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./admin-make/admin-make.component */ "./src/app/admin-make/admin-make.component.ts");
/* harmony import */ var _admin_make_update_admin_make_update_component__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./admin-make-update/admin-make-update.component */ "./src/app/admin-make-update/admin-make-update.component.ts");
/* harmony import */ var _homepage_homepage_component__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./homepage/homepage.component */ "./src/app/homepage/homepage.component.ts");
/* harmony import */ var _signup_signup_component__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ./signup/signup.component */ "./src/app/signup/signup.component.ts");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! ./dashboard/dashboard.component */ "./src/app/dashboard/dashboard.component.ts");
/* harmony import */ var _userheader_userheader_component__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! ./userheader/userheader.component */ "./src/app/userheader/userheader.component.ts");
/* harmony import */ var _admin_users_admin_users_component__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! ./admin-users/admin-users.component */ "./src/app/admin-users/admin-users.component.ts");
/* harmony import */ var _cardetails_cardetails_component__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! ./cardetails/cardetails.component */ "./src/app/cardetails/cardetails.component.ts");
/* harmony import */ var _search_search_component__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! ./search/search.component */ "./src/app/search/search.component.ts");



































var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"],
                _admin_login_admin_login_component__WEBPACK_IMPORTED_MODULE_18__["AdminLoginComponent"],
                _admin_dashboard_admin_dashboard_component__WEBPACK_IMPORTED_MODULE_19__["AdminDashboardComponent"],
                _admin_sidebar_admin_sidebar_component__WEBPACK_IMPORTED_MODULE_20__["AdminSidebarComponent"],
                _admin_car_admin_car_component__WEBPACK_IMPORTED_MODULE_21__["AdminCarComponent"],
                _admin_car_update_admin_car_update_component__WEBPACK_IMPORTED_MODULE_22__["AdminCarUpdateComponent"],
                _admin_image_category_admin_image_category_component__WEBPACK_IMPORTED_MODULE_23__["AdminImageCategoryComponent"],
                _admin_update_image_category_admin_update_image_category_component__WEBPACK_IMPORTED_MODULE_24__["AdminUpdateImageCategoryComponent"],
                _admin_make_admin_make_component__WEBPACK_IMPORTED_MODULE_25__["AdminMakeComponent"],
                _admin_make_update_admin_make_update_component__WEBPACK_IMPORTED_MODULE_26__["AdminMakeUpdateComponent"],
                _homepage_homepage_component__WEBPACK_IMPORTED_MODULE_27__["HomepageComponent"],
                _signup_signup_component__WEBPACK_IMPORTED_MODULE_28__["SignupComponent"],
                _login_login_component__WEBPACK_IMPORTED_MODULE_29__["LoginComponent"],
                _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_30__["DashboardComponent"],
                _userheader_userheader_component__WEBPACK_IMPORTED_MODULE_31__["UserheaderComponent"],
                _admin_users_admin_users_component__WEBPACK_IMPORTED_MODULE_32__["AdminUsersComponent"],
                _cardetails_cardetails_component__WEBPACK_IMPORTED_MODULE_33__["CardetailsComponent"],
                _search_search_component__WEBPACK_IMPORTED_MODULE_34__["SearchComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_6__["AppRoutingModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientJsonpModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_8__["BrowserAnimationsModule"],
                angular_bootstrap_md__WEBPACK_IMPORTED_MODULE_11__["MDBBootstrapModule"].forRoot(),
                _angular_material__WEBPACK_IMPORTED_MODULE_12__["MatFormFieldModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_12__["MatSnackBarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_12__["MatDialogModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_12__["MatSidenavModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_12__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_12__["MatToolbarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_12__["MatToolbarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_12__["MatListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_12__["MatSelectModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_12__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_12__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_12__["MatProgressSpinnerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_12__["MatBottomSheetModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_12__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_12__["MatAutocompleteModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_12__["MatStepperModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ReactiveFormsModule"],
                mat_progress_buttons__WEBPACK_IMPORTED_MODULE_13__["MatProgressButtonsModule"],
                ngx_file_drop__WEBPACK_IMPORTED_MODULE_14__["NgxFileDropModule"],
                ngx_color_sketch__WEBPACK_IMPORTED_MODULE_15__["ColorSketchModule"],
                ng2_img_max__WEBPACK_IMPORTED_MODULE_16__["Ng2ImgMaxModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_12__["MatRadioModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_12__["MatSlideToggleModule"],
                ngx_owl_carousel_o__WEBPACK_IMPORTED_MODULE_17__["CarouselModule"]
            ],
            schemas: [_angular_core__WEBPACK_IMPORTED_MODULE_2__["NO_ERRORS_SCHEMA"]],
            providers: [
                _common_service__WEBPACK_IMPORTED_MODULE_10__["CommonService"],
                { provide: _angular_http__WEBPACK_IMPORTED_MODULE_3__["BrowserXhr"], useFactory: _cust_ext_browser_xhr__WEBPACK_IMPORTED_MODULE_9__["CustExtBrowserXhr"] }
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/cardetails/cardetails.component.css":
/*!*****************************************************!*\
  !*** ./src/app/cardetails/cardetails.component.css ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhbmd1bGFyX2FwcC9zcmMvYXBwL2NhcmRldGFpbHMvY2FyZGV0YWlscy5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/cardetails/cardetails.component.html":
/*!******************************************************!*\
  !*** ./src/app/cardetails/cardetails.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  cardetails works!\n</p>\n"

/***/ }),

/***/ "./src/app/cardetails/cardetails.component.ts":
/*!****************************************************!*\
  !*** ./src/app/cardetails/cardetails.component.ts ***!
  \****************************************************/
/*! exports provided: CardetailsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CardetailsComponent", function() { return CardetailsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");


var CardetailsComponent = /** @class */ (function () {
    function CardetailsComponent() {
    }
    CardetailsComponent.prototype.ngOnInit = function () {
    };
    CardetailsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-cardetails',
            template: __webpack_require__(/*! ./cardetails.component.html */ "./src/app/cardetails/cardetails.component.html"),
            styles: [__webpack_require__(/*! ./cardetails.component.css */ "./src/app/cardetails/cardetails.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], CardetailsComponent);
    return CardetailsComponent;
}());



/***/ }),

/***/ "./src/app/common.service.ts":
/*!***********************************!*\
  !*** ./src/app/common.service.ts ***!
  \***********************************/
/*! exports provided: CommonService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CommonService", function() { return CommonService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");


var CommonService = /** @class */ (function () {
    function CommonService() {
        var commonData = {};
        var commonDataElement = document.getElementById('commonData');
        if (commonDataElement && commonDataElement.innerHTML) {
            commonData = JSON.parse(commonDataElement.innerHTML);
        }
        this.commonData = commonData;
    }
    CommonService.prototype.getfirst_index = function () {
        return this.urlFirstSegment;
    };
    CommonService.prototype.getsecond_index = function () {
        return this.urlSecondSegment;
    };
    CommonService.prototype.getthird_index = function () {
        return this.urlThirdSegment;
    };
    CommonService.prototype.getfourth_index = function () {
        return this.urlFourthSegment;
    };
    CommonService.prototype.getfifth_index = function () {
        return this.urlFifthSegment;
    };
    CommonService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], CommonService);
    return CommonService;
}());



/***/ }),

/***/ "./src/app/cust-ext-browser-xhr.ts":
/*!*****************************************!*\
  !*** ./src/app/cust-ext-browser-xhr.ts ***!
  \*****************************************/
/*! exports provided: CustExtBrowserXhr */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustExtBrowserXhr", function() { return CustExtBrowserXhr; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "../node_modules/@angular/http/fesm5/http.js");



var CustExtBrowserXhr = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](CustExtBrowserXhr, _super);
    function CustExtBrowserXhr() {
        return _super.call(this) || this;
    }
    CustExtBrowserXhr.prototype.build = function () {
        var xhr = _super.prototype.build.call(this);
        xhr.withCredentials = true; // this is all the magic we need for now
        return (xhr);
    };
    CustExtBrowserXhr = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
        /**
         * @author AhsanAyaz
         * We're extending the BrowserXhr to support CORS
         */
        ,
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], CustExtBrowserXhr);
    return CustExtBrowserXhr;
}(_angular_http__WEBPACK_IMPORTED_MODULE_2__["BrowserXhr"]));



/***/ }),

/***/ "./src/app/dashboard/dashboard.component.css":
/*!***************************************************!*\
  !*** ./src/app/dashboard/dashboard.component.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhbmd1bGFyX2FwcC9zcmMvYXBwL2Rhc2hib2FyZC9kYXNoYm9hcmQuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/dashboard/dashboard.component.html":
/*!****************************************************!*\
  !*** ./src/app/dashboard/dashboard.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-userheader></app-userheader>\n<div class=\"col-12 p-0 my-4\">\n\t<div class=\"container p-0 d-flex f-wrap\">\n    <div class=\"col-12\">\n      <div class=\"p-4 white radius_10 z-depth-1\">\n        <h4 class=\"bold_font mb-3 primary-text-color\">Join us on journey</h4>\n        <p class=\"grey-text col-9 p-0 font-9\">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>\n        <button class=\"btn orange darken-3 l-h-normal p-3 m-0 rounded text-white font-8\" routerLink=\"/search\" mat-button>Make your journey</button>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/dashboard/dashboard.component.ts":
/*!**************************************************!*\
  !*** ./src/app/dashboard/dashboard.component.ts ***!
  \**************************************************/
/*! exports provided: DashboardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardComponent", function() { return DashboardComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");


var DashboardComponent = /** @class */ (function () {
    function DashboardComponent() {
    }
    DashboardComponent.prototype.ngOnInit = function () {
    };
    DashboardComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-dashboard',
            template: __webpack_require__(/*! ./dashboard.component.html */ "./src/app/dashboard/dashboard.component.html"),
            styles: [__webpack_require__(/*! ./dashboard.component.css */ "./src/app/dashboard/dashboard.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], DashboardComponent);
    return DashboardComponent;
}());



/***/ }),

/***/ "./src/app/homepage/homepage.component.css":
/*!*************************************************!*\
  !*** ./src/app/homepage/homepage.component.css ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhbmd1bGFyX2FwcC9zcmMvYXBwL2hvbWVwYWdlL2hvbWVwYWdlLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/homepage/homepage.component.html":
/*!**************************************************!*\
  !*** ./src/app/homepage/homepage.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"col-12 p-0 row align-items-center m-0 primary-bg h-md-650 mb-5\">\n    <div class=\"col-md-6 d-flex py-5 py-md-0 mb-4 mb-md-0 align-items-center banner_car white\">\n      <img class=\"card-img-top pr-md-5 py-5 py-md-0\" src=\"angular_app/carlease/assets/images/car_banner.png\" alt=\"Car\" />\n    </div>\n    <div class=\"col-md-6\">\n      <div class=\"mw-500 px-4\">\n        <h3 class=\"bold_font text-white\">All New Series with Car Lease</h3>\n        <p class=\"text-white font-9 mb-4\">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>\n        <button class=\"btn white l-h-normal py-3 mb-4 mb-md-0 px-4 m-0 radius_30 primary-text-color font-9\" routerLink=\"/search\" mat-button>Find your Perfect Lease</button>\n      </div>\n    </div>\n</div>\n<div class=\"col-12 py-3 px-0\">\n  <div class=\"container\">\n    <h2 class=\"mb-5 bold_font text-center position-relative section_title pb-3\">How it Works</h2>\n    <div class=\"col-12 row m-0 mb-5 p-0\">\n      <div class=\"col-md-4 p-0\">\n        <div class=\"view p-3 text-center\">\n          <span class=\"d-flex mw-150 border-1 mb-4 rounded-circle border-dash mx-auto\"><img src=\"angular_app/carlease/assets/images/car.svg\" class=\"w-100 h-150 o-contain mw-70x mx-auto\" /></span>\n          <h4 class=\"bold_font font-1 text-uppercase mb-3\">Select Your Car</h4>\n          <p class=\"font-9 mb-3 text-muted\">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>\n        </div>\n      </div>\n      <div class=\"col-md-4 p-0\">\n        <div class=\"view p-3 text-center\">\n          <span class=\"d-flex mw-150 border-1 mb-4 rounded-circle border-dash mx-auto\"><img src=\"angular_app/carlease/assets/images/terms.svg\" class=\"w-100 h-150 o-contain mw-70x mx-auto\" /></span>\n          <h4 class=\"bold_font font-1 text-uppercase mb-3\">Choose Your Lease Terms</h4>\n          <p class=\"font-9 mb-3 text-muted\">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>\n        </div>\n      </div>\n      <div class=\"col-md-4 p-0\">\n        <div class=\"view p-3 text-center\">\n          <span class=\"d-flex mw-150 border-1 mb-4 rounded-circle border-dash mx-auto\"><img src=\"angular_app/carlease/assets/images/truck.svg\" class=\"w-100 h-150 o-contain mw-70x mx-auto\" /></span>\n          <h4 class=\"bold_font font-1 text-uppercase mb-3\">Schedule Delivery</h4>\n          <p class=\"font-9 mb-3 text-muted\">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>\n        </div>\n      </div>\n      <button class=\"btn orange darken-3 mx-auto l-h-normal py-3 px-4 mx-0 mb-4 radius_30 text-white font-9\" routerLink=\"/search\" mat-button>Let's start your jouney</button>\n    </div>\n    <h2 class=\"mb-5 bold_font text-center position-relative section_title pb-3\">Latest Cars</h2>\n    <div class=\"col-12 p-0 mb-5\">\n        <owl-carousel-o [options]=\"customOptions\">\n            <ng-container *ngFor=\"let car of cars\">\n                <ng-template carouselSlide [id]=\"car.id\">\n                  <div class=\"border-1\">\n                    <img [src]=\"car.photo_id.car_photo\" [alt]=\"car.model\" class=\"w-100 h-200 o-contain\">\n                    <p class=\"font-1 bold_font p-3 m-0\">{{car.model}}</p>\n                  </div>\n                </ng-template>\n              </ng-container>  \n        </owl-carousel-o>\n    </div>\n    <h2 class=\"mb-5 bold_font text-center position-relative section_title pb-3\">Find your Next Lease</h2>\n    <div class=\"col-12 row m-0 mb-5 p-0\">\n      <div class=\"col-md-2 text-center\" *ngFor=\"let make of makes; let i = index\">\n        <img [src]=\"make.makephoto\" class=\"w-100 mw-100x h-100x o-contain mb-3 greyscale\" />\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/homepage/homepage.component.ts":
/*!************************************************!*\
  !*** ./src/app/homepage/homepage.component.ts ***!
  \************************************************/
/*! exports provided: HomepageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomepageComponent", function() { return HomepageComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../rest.service */ "./src/app/rest.service.ts");
/* harmony import */ var _common_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../common.service */ "./src/app/common.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "../node_modules/@angular/router/fesm5/router.js");





var HomepageComponent = /** @class */ (function () {
    function HomepageComponent(rest, commonservice, router) {
        this.rest = rest;
        this.commonservice = commonservice;
        this.router = router;
        this.customOptions = {
            loop: true,
            mouseDrag: true,
            touchDrag: false,
            pullDrag: true,
            dots: false,
            navSpeed: 700,
            navText: ["<i class='material-icons'>chevron_left</i>", "<i class='material-icons'>chevron_right</i>"],
            margin: 20,
            responsive: {
                0: {
                    items: 1
                },
                400: {
                    items: 2
                },
                740: {
                    items: 3
                },
                940: {
                    items: 4
                }
            },
            nav: true
        };
        this.cars = [];
    }
    HomepageComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.rest.post('get_active_make', {}).subscribe(function (response) {
            if (response.status) {
                _this.makes = response.data.active_make;
            }
        });
        this.rest.post('get_latest_cars', {}).subscribe(function (response) {
            if (response.status) {
                _this.cars = response.data.car;
                console.log(_this.cars);
            }
        });
    };
    HomepageComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-homepage',
            template: __webpack_require__(/*! ./homepage.component.html */ "./src/app/homepage/homepage.component.html"),
            styles: [__webpack_require__(/*! ./homepage.component.css */ "./src/app/homepage/homepage.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_rest_service__WEBPACK_IMPORTED_MODULE_2__["RestService"], _common_service__WEBPACK_IMPORTED_MODULE_3__["CommonService"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
    ], HomepageComponent);
    return HomepageComponent;
}());



/***/ }),

/***/ "./src/app/login/login.component.css":
/*!*******************************************!*\
  !*** ./src/app/login/login.component.css ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhbmd1bGFyX2FwcC9zcmMvYXBwL2xvZ2luL2xvZ2luLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/login/login.component.html":
/*!********************************************!*\
  !*** ./src/app/login/login.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"col-12 px-0 pt-5\">\n  <div class=\"container\">\n    <div class=\"row mx-0 my-5\">\n      <div class=\"col-md-6 p-0\">\n          <div class=\"mdb-color radius_top_left h-100 radius_10 darken-3 p-5\">\n            <h4 class=\"text-white bold_font text-uppercase mb-4\">Information</h4>\n            <p class=\"text-white font-9\">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries</p>\n            <p class=\"text-white font-9\">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>\n            <button class=\"btn orange darken-3 l-h-normal p-3 m-0 rounded text-white font-9\" routerLink=\"/register\" mat-button>Don't have an Account</button>\n          </div>\n      </div>\n      <div class=\"col-md-6 p-0\">\n          <div class=\"white p-4 radius_10 radius_bottom_right z-depth-1 h-100\">\n            <h4 class=\"text-black-50 bold_font text-uppercase px-3 mb-4\">Login Form</h4>\n            <div class=\"col-12 p-0 row m-0\">\n              <div class=\"col-12\">\n                  <mat-form-field appearance=\"outline\" class=\"w-100\"> \n                    <mat-label class=\"font-9\">Email</mat-label> \t\t \t\t\t\n                    <input type=\"text\" matInput [(ngModel)]=\"formValues['email']\" placeholder=\"Email\" autocomplete=\"off\" name=\"\" class=\"font-9\" (keydown.enter)=\"loginSubmit()\" />\n                  </mat-form-field>\n              </div>\n              <div class=\"col-12\">\n                  <mat-form-field appearance=\"outline\" class=\"w-100\"> \n                    <mat-label class=\"font-9\">Password</mat-label> \t\t \t\t\t\n                    <input type=\"text\" matInput [(ngModel)]=\"formValues['pwd']\" placeholder=\"Password\" autocomplete=\"off\" name=\"\" class=\"font-9\" (keydown.enter)=\"loginSubmit()\" [type]=\"pwd_hide ? 'password' : 'text'\"/>\n                    <i class=\"material-icons pointer primary-text-color\" matSuffix (click)=\"pwd_hide = !pwd_hide\">{{pwd_hide ? 'visibility_off' : 'visibility'}}</i>\n                  </mat-form-field>\n              </div>\n              <div class=\"col-12 text-center\">\n                  <mat-spinner-button class=\"btn orange darken-3 l-h-normal ml-auto p-3 m-0 rounded\"\n                  (btnClick)=\"loginSubmit()\" [options]=\"btnOpts\"></mat-spinner-button>\n              </div>\n            </div>\n\n          </div>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/login/login.component.ts":
/*!******************************************!*\
  !*** ./src/app/login/login.component.ts ***!
  \******************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../rest.service */ "./src/app/rest.service.ts");
/* harmony import */ var _common_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../common.service */ "./src/app/common.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material */ "../node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "../node_modules/@angular/router/fesm5/router.js");






var LoginComponent = /** @class */ (function () {
    function LoginComponent(rest, commonservice, snackBar, router) {
        this.rest = rest;
        this.commonservice = commonservice;
        this.snackBar = snackBar;
        this.router = router;
        this.btnOpts = {
            active: false,
            text: 'Submit',
            spinnerSize: 18,
            raised: true,
            stroked: false,
            spinnerColor: 'accent',
            fullWidth: false,
            disabled: false,
            mode: 'indeterminate',
        };
        this.formValues = {};
        this.pwd_hide = true;
    }
    LoginComponent.prototype.ngOnInit = function () {
        this.userLoggedin = this.commonservice.user_login;
        if (this.userLoggedin == 'true') {
            this.router.navigateByUrl('dashboard');
        }
    };
    LoginComponent.prototype.validateEmail = function (email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    };
    LoginComponent.prototype.loginSubmit = function () {
        var _this = this;
        if (this.formValues['email'] == undefined || this.formValues['email'] == '') {
            this.snackBar.open('Enter a valid email', '', {
                duration: 2000,
                panelClass: ['orange-text']
            });
        }
        else if (!this.validateEmail(this.formValues['email'])) {
            this.snackBar.open('Invalid email', '', {
                duration: 2000,
                panelClass: ['orange-text']
            });
        }
        else if (this.formValues['pwd'] == undefined || this.formValues['pwd'] == '') {
            this.snackBar.open('Enter Password', '', {
                duration: 2000,
                panelClass: ['orange-text']
            });
        }
        else {
            var params = {
                'email': this.formValues['email'],
                'password': this.formValues['pwd']
            };
            var data = JSON.stringify(params);
            this.btnOpts.active = true;
            this.rest.post('user_login', data).subscribe(function (response) {
                if (response.status) {
                    _this.snackBar.open(response.message, '', {
                        duration: 2000,
                        panelClass: ['green-text']
                    });
                    _this.commonservice.user_login = 'true';
                    _this.commonservice.user = response.data.user;
                    localStorage.setItem('users', JSON.stringify(response.data.user));
                    localStorage.setItem('isUserLogged', 'true');
                    _this.router.navigateByUrl('dashboard');
                }
                else {
                    _this.snackBar.open(response.message, '', {
                        duration: 2000,
                        panelClass: ['orange-text']
                    });
                }
                _this.btnOpts.active = false;
            });
        }
    };
    LoginComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.css */ "./src/app/login/login.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_rest_service__WEBPACK_IMPORTED_MODULE_2__["RestService"], _common_service__WEBPACK_IMPORTED_MODULE_3__["CommonService"], _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSnackBar"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/rest.service.ts":
/*!*********************************!*\
  !*** ./src/app/rest.service.ts ***!
  \*********************************/
/*! exports provided: RestService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RestService", function() { return RestService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "../node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "../node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _common_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./common.service */ "./src/app/common.service.ts");





var httpOptions = {
    headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
        'Content-Type': 'application/json',
    })
};
var RestService = /** @class */ (function () {
    function RestService(http, commonservice) {
        this.http = http;
        this.commonservice = commonservice;
        this.urls = {
            admin_login: 'admin/check_login',
            get_cars: 'admin/get_cars',
            get_car_image_category: 'admin/get_car_image_category',
            get_image_category_detail: 'admin/get_image_category_detail',
            add_image_category: 'admin/update_image_category',
            delete_image_category: 'admin/delete_image_category',
            get_car_make: 'admin/get_car_make',
            delete_car_make: 'admin/delete_car_make',
            add_make: 'admin/update_make',
            get_make_detail: 'admin/get_make_detail',
            get_active_make: 'admin/get_active_make',
            add_car: 'admin/add_car',
            upload_car_photo: 'admin/upload_car_photo',
            delete_photo: 'admin/delete_photo',
            get_car_photo: 'admin/get_car_photo',
            update_catgory: 'admin/update_catgory',
            get_car_detail: 'admin/get_car_detail',
            delete_car: 'admin/delete_car',
            user_register: 'users/user_register',
            user_login: 'users/user_login',
            get_users: 'users/get_users',
            update_user: 'users/update_user',
            get_latest_cars: 'admin/get_latest_cars'
        };
        this.restUrl = '';
        var commonData = this.commonservice.commonData;
        this.restUrl = commonData['restUrl'];
    }
    RestService.prototype.getUrl = function (urlKey) {
        var uri = this.urls[urlKey];
        var url = this.restUrl + uri;
        return url;
    };
    RestService.prototype.post = function (urlKey, params) {
        var url = this.getUrl(urlKey);
        return this.http.post(url, { params: params }, httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(this.extractData));
    };
    RestService.prototype.upload = function (urlKey, formData, params) {
        formData.append('params', JSON.stringify(params));
        var url = this.getUrl(urlKey);
        return this.http.post(url, formData, {}).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(this.extractData));
    };
    RestService.prototype.extractData = function (res) {
        var body = res;
        return body || {};
    };
    RestService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _common_service__WEBPACK_IMPORTED_MODULE_4__["CommonService"]])
    ], RestService);
    return RestService;
}());



/***/ }),

/***/ "./src/app/search/search.component.css":
/*!*********************************************!*\
  !*** ./src/app/search/search.component.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhbmd1bGFyX2FwcC9zcmMvYXBwL3NlYXJjaC9zZWFyY2guY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/search/search.component.html":
/*!**********************************************!*\
  !*** ./src/app/search/search.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  search works!\n</p>\n"

/***/ }),

/***/ "./src/app/search/search.component.ts":
/*!********************************************!*\
  !*** ./src/app/search/search.component.ts ***!
  \********************************************/
/*! exports provided: SearchComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchComponent", function() { return SearchComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");


var SearchComponent = /** @class */ (function () {
    function SearchComponent() {
    }
    SearchComponent.prototype.ngOnInit = function () {
    };
    SearchComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-search',
            template: __webpack_require__(/*! ./search.component.html */ "./src/app/search/search.component.html"),
            styles: [__webpack_require__(/*! ./search.component.css */ "./src/app/search/search.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], SearchComponent);
    return SearchComponent;
}());



/***/ }),

/***/ "./src/app/signup/signup.component.css":
/*!*********************************************!*\
  !*** ./src/app/signup/signup.component.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhbmd1bGFyX2FwcC9zcmMvYXBwL3NpZ251cC9zaWdudXAuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/signup/signup.component.html":
/*!**********************************************!*\
  !*** ./src/app/signup/signup.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"col-12 px-0 pt-5\">\n  <div class=\"container\">\n    <div class=\"row mx-0 my-5\">\n      <div class=\"col-md-6 p-0\">\n          <div class=\"mdb-color radius_top_left h-100 radius_10 darken-3 p-5\">\n            <h4 class=\"text-white bold_font text-uppercase mb-4\">Information</h4>\n            <p class=\"text-white font-9\">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries</p>\n            <p class=\"text-white font-9\">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>\n            <button class=\"btn orange darken-3 l-h-normal p-3 m-0 rounded text-white font-9\" routerLink=\"/login\" mat-button>I have an Account</button>\n          </div>\n      </div>\n      <div class=\"col-md-6 p-0\">\n          <div class=\"white p-4 radius_10 radius_bottom_right z-depth-1\">\n            <h4 class=\"text-black-50 bold_font text-uppercase px-3 mb-4\">Register Form</h4>\n            <div class=\"col-12 p-0 row m-0\">\n              <div class=\"col-md-6\">\n                  <mat-form-field appearance=\"outline\" class=\"w-100\"> \n                    <mat-label class=\"font-9\">First Name</mat-label> \t\t \t\t\t\n                    <input type=\"text\" matInput [(ngModel)]=\"formValues['fname']\" placeholder=\"First Name\" autocomplete=\"off\" name=\"\" class=\"font-9\" (keydown.enter)=\"registerSubmit()\" />\n                  </mat-form-field>\n              </div>\n              <div class=\"col-md-6\">\n                  <mat-form-field appearance=\"outline\" class=\"w-100\"> \n                    <mat-label class=\"font-9\">Last Name</mat-label> \t\t \t\t\t\n                    <input type=\"text\" matInput [(ngModel)]=\"formValues['lname']\" placeholder=\"Last Name\" autocomplete=\"off\" name=\"\" class=\"font-9\" (keydown.enter)=\"registerSubmit()\" />\n                  </mat-form-field>\n              </div>\n              <div class=\"col-12\">\n                  <mat-form-field appearance=\"outline\" class=\"w-100\"> \n                    <mat-label class=\"font-9\">Email</mat-label> \t\t \t\t\t\n                    <input type=\"text\" matInput [(ngModel)]=\"formValues['email']\" placeholder=\"Email\" autocomplete=\"off\" name=\"\" class=\"font-9\" (keydown.enter)=\"registerSubmit()\" />\n                  </mat-form-field>\n              </div>\n              <div class=\"col-md-6\">\n                  <div class=\"col-12 p-0\">\n                    <div class=\"upload-box-content\">\n                      <div class=\"drop-container contain-bg d-flex align-items-center justify-content-center mb-4 h-150 border-1 rounded\" [style.background-image]=\"'url('+imageLicenceSrc+')'\">\n                        <p class=\"font-9\" [ngClass]=\"[imageLicenceSrc != null ? 'position-absolute p-3 t-0 r-0' : 'mb-0 text-center px-3']\">\n                          <label class=\"upload-button mb-0 primary-text-color pointer\"><input type=\"file\"\n                              (change)=\"makeLicenceUpload($event)\" accept=\"image/*\" class=\"position-absolute w-50x z-index_1 opacity-0\"> Browse </label> license to upload.\n                        </p>\n                      </div>\n                    </div>\n                  </div>\n              </div>\n              <div class=\"col-md-6\">\n                  <div class=\"col-12 p-0\">\n                    <div class=\"upload-box-content\">\n                      <div class=\"drop-container contain-bg d-flex align-items-center justify-content-center mb-4 h-150 border-1 rounded\" [style.background-image]=\"'url('+imageNationalSrc+')'\">\n                        <p class=\"font-9\" [ngClass]=\"[imageNationalSrc != null ? 'position-absolute p-3 t-0 r-0' : 'mb-0 text-center px-3']\">\n                          <label class=\"upload-button mb-0 primary-text-color pointer\"><input type=\"file\"\n                              (change)=\"makeNationalUpload($event)\" accept=\"image/*\" class=\"position-absolute w-50x z-index_1 opacity-0\"> Browse </label> national ID to upload.\n                        </p>\n                      </div>\n                    </div>\n                  </div>\n              </div>\n              <div class=\"col-md-6\">\n                  <mat-form-field appearance=\"outline\" class=\"w-100\"> \n                    <mat-label class=\"font-9\">Password</mat-label> \t\t \t\t\t\n                    <input type=\"text\" matInput [(ngModel)]=\"formValues['pwd']\" placeholder=\"Password\" autocomplete=\"off\" name=\"\" class=\"font-9\" (keydown.enter)=\"registerSubmit()\" [type]=\"pwd_hide ? 'password' : 'text'\"/>\n                    <i class=\"material-icons pointer primary-text-color\" matSuffix (click)=\"pwd_hide = !pwd_hide\">{{pwd_hide ? 'visibility_off' : 'visibility'}}</i>\n                  </mat-form-field>\n              </div>\n              <div class=\"col-md-6\">\n                  <mat-form-field appearance=\"outline\" class=\"w-100\"> \n                    <mat-label class=\"font-9\">Confirm Password</mat-label> \t\t \t\t\t\n                    <input type=\"text\" matInput [(ngModel)]=\"formValues['cpwd']\" placeholder=\"Confirm Password\" autocomplete=\"off\" name=\"\" class=\"font-9\" (keydown.enter)=\"registerSubmit()\" [type]=\"cpwd_hide ? 'password' : 'text'\"/>\n                    <i class=\"material-icons pointer primary-text-color\" matSuffix (click)=\"cpwd_hide = !cpwd_hide\">{{cpwd_hide ? 'visibility_off' : 'visibility'}}</i>\n                  </mat-form-field>\n              </div>\n              <div class=\"col-12 text-center\">\n                  <mat-spinner-button class=\"btn orange darken-3 l-h-normal ml-auto p-3 m-0 rounded\"\n                  (btnClick)=\"registerSubmit()\" [options]=\"btnOpts\"></mat-spinner-button>\n              </div>\n            </div>\n\n          </div>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/signup/signup.component.ts":
/*!********************************************!*\
  !*** ./src/app/signup/signup.component.ts ***!
  \********************************************/
/*! exports provided: SignupComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignupComponent", function() { return SignupComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../rest.service */ "./src/app/rest.service.ts");
/* harmony import */ var _common_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../common.service */ "./src/app/common.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material */ "../node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "../node_modules/@angular/router/fesm5/router.js");






var SignupComponent = /** @class */ (function () {
    function SignupComponent(rest, commonservice, snackBar, router) {
        this.rest = rest;
        this.commonservice = commonservice;
        this.snackBar = snackBar;
        this.router = router;
        this.btnOpts = {
            active: false,
            text: 'Submit',
            spinnerSize: 18,
            raised: true,
            stroked: false,
            spinnerColor: 'accent',
            fullWidth: false,
            disabled: false,
            mode: 'indeterminate',
        };
        this.formValues = {};
        this.pwd_hide = true;
        this.cpwd_hide = true;
    }
    SignupComponent.prototype.ngOnInit = function () {
        this.userLoggedin = this.commonservice.user_login;
        if (this.userLoggedin == 'true') {
            this.router.navigateByUrl('dashboard');
        }
    };
    SignupComponent.prototype.makeLicenceUpload = function (event) {
        var _this = this;
        this.selected_licence_File = event.target.files[0];
        if (this.isFileAllowed(this.selected_licence_File.name)) {
            this.selected_licence_File = undefined;
            this.snackBar.open("Photo error occured", '', {
                duration: 2000,
                panelClass: ['orange-text']
            });
            return false;
        }
        else {
            if (event.target.files && event.target.files[0]) {
                var file = event.target.files[0];
                var reader_1 = new FileReader();
                reader_1.onload = function (e) { return _this.imageLicenceSrc = reader_1.result; };
                reader_1.readAsDataURL(file);
            }
        }
    };
    SignupComponent.prototype.makeNationalUpload = function (event) {
        var _this = this;
        this.selected_national_File = event.target.files[0];
        if (this.isFileAllowed(this.selected_national_File.name)) {
            this.selected_national_File = undefined;
            this.snackBar.open("Photo error occured", '', {
                duration: 2000,
                panelClass: ['orange-text']
            });
            return false;
        }
        else {
            if (event.target.files && event.target.files[0]) {
                var file = event.target.files[0];
                var reader_2 = new FileReader();
                reader_2.onload = function (e) { return _this.imageNationalSrc = reader_2.result; };
                reader_2.readAsDataURL(file);
            }
        }
    };
    SignupComponent.prototype.isFileAllowed = function (fileName) {
        var isFileAllowed = false;
        var not_upload = false;
        var allowedFiles = ['.png', '.jpeg', '.jpg'];
        var regex = /(?:\.([^.]+))?$/;
        var extension = regex.exec(fileName);
        if (undefined !== extension && null !== extension) {
            for (var _i = 0, allowedFiles_1 = allowedFiles; _i < allowedFiles_1.length; _i++) {
                var ext = allowedFiles_1[_i];
                if (ext === extension[0]) {
                    isFileAllowed = true;
                }
            }
        }
        else {
            isFileAllowed = false;
        }
        if (isFileAllowed == false) {
            not_upload = true;
        }
        else {
            not_upload = false;
        }
        return not_upload;
    };
    SignupComponent.prototype.validateEmail = function (email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    };
    SignupComponent.prototype.registerSubmit = function () {
        var _this = this;
        if (this.formValues['fname'] == undefined || this.formValues['fname'] == '') {
            this.snackBar.open('Enter First Name', '', {
                panelClass: "orange-text",
                duration: 2000,
            });
        }
        else if (this.formValues['lname'] == undefined || this.formValues['lname'] == '') {
            this.snackBar.open('Enter Last Name', '', {
                panelClass: "orange-text",
                duration: 2000,
            });
        }
        else if (this.formValues['email'] == undefined || this.formValues['email'] == '') {
            this.snackBar.open('Enter a valid email', '', {
                panelClass: "orange-text",
                duration: 2000,
            });
        }
        else if (!this.validateEmail(this.formValues['email'])) {
            this.snackBar.open('Invalid email', '', {
                panelClass: "orange-text",
                duration: 2000,
            });
        }
        else if (this.selected_licence_File == undefined) {
            this.snackBar.open('Upload licence image', '', {
                duration: 2000,
                panelClass: ['orange-text']
            });
        }
        else if (this.selected_national_File == undefined) {
            this.snackBar.open('Upload National ID image', '', {
                duration: 2000,
                panelClass: ['orange-text']
            });
        }
        else if (this.formValues['pwd'] == undefined || this.formValues['pwd'] == '') {
            this.snackBar.open('Enter Password', '', {
                panelClass: "orange-text",
                duration: 2000,
            });
        }
        else if (this.formValues['cpwd'] == undefined || this.formValues['cpwd'] == '') {
            this.snackBar.open('Enter Confirm Password', '', {
                panelClass: "orange-text",
                duration: 2000,
            });
        }
        else if (this.formValues['cpwd'] != this.formValues['pwd']) {
            this.snackBar.open('Password is mismatch', '', {
                panelClass: "orange-text",
                duration: 2000,
            });
        }
        else {
            this.btnOpts.active = true;
            this.btnOpts.text = "Saving...";
            var uploadData = new FormData();
            uploadData.append('licence_media', this.selected_licence_File, this.selected_licence_File.name);
            uploadData.append('national_media', this.selected_national_File, this.selected_national_File.name);
            uploadData.append('fname', this.formValues['fname']);
            uploadData.append('lname', this.formValues['lname']);
            uploadData.append('email', this.formValues['email']);
            uploadData.append('password', this.formValues['pwd']);
            var data = uploadData;
            var params = {};
            this.rest.upload('user_register', data, params).subscribe(function (response) {
                if (response.status) {
                    _this.snackBar.open(response.message, '', {
                        panelClass: "green-text",
                        duration: 2000,
                    });
                    _this.commonservice.user_login = 'true';
                    _this.commonservice.user = response.data.user;
                    localStorage.setItem('users', JSON.stringify(response.data.user));
                    localStorage.setItem('isUserLogged', 'true');
                    _this.router.navigateByUrl('dashboard');
                }
                else {
                    _this.snackBar.open(response.message, '', {
                        panelClass: "orange-text",
                        duration: 2000,
                    });
                }
                _this.btnOpts.active = false;
                _this.btnOpts.text = "Submit";
            });
        }
    };
    SignupComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-signup',
            template: __webpack_require__(/*! ./signup.component.html */ "./src/app/signup/signup.component.html"),
            styles: [__webpack_require__(/*! ./signup.component.css */ "./src/app/signup/signup.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_rest_service__WEBPACK_IMPORTED_MODULE_2__["RestService"], _common_service__WEBPACK_IMPORTED_MODULE_3__["CommonService"], _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSnackBar"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]])
    ], SignupComponent);
    return SignupComponent;
}());



/***/ }),

/***/ "./src/app/userauth.guard.ts":
/*!***********************************!*\
  !*** ./src/app/userauth.guard.ts ***!
  \***********************************/
/*! exports provided: UserauthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserauthGuard", function() { return UserauthGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "../node_modules/@angular/router/fesm5/router.js");



var UserauthGuard = /** @class */ (function () {
    function UserauthGuard(router) {
        this.router = router;
    }
    UserauthGuard.prototype.canActivate = function (next, state) {
        var url = state.url;
        return this.userLogin(url);
    };
    UserauthGuard.prototype.userLogin = function (url) {
        var logStatus = this.isUserLogged();
        if (!logStatus) {
            this.router.navigate(['login']);
            return false;
        }
        else if (logStatus) {
            return true;
        }
    };
    UserauthGuard.prototype.isUserLogged = function () {
        var status = false;
        if (localStorage.getItem('isUserLogged') == "true") {
            status = true;
        }
        else {
            status = false;
        }
        return status;
    };
    UserauthGuard = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], UserauthGuard);
    return UserauthGuard;
}());



/***/ }),

/***/ "./src/app/userheader/userheader.component.css":
/*!*****************************************************!*\
  !*** ./src/app/userheader/userheader.component.css ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhbmd1bGFyX2FwcC9zcmMvYXBwL3VzZXJoZWFkZXIvdXNlcmhlYWRlci5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/userheader/userheader.component.html":
/*!******************************************************!*\
  !*** ./src/app/userheader/userheader.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"col-12 p-0\">\n  <div class=\"user_banner px-3 row m-0\">\n    <div class=\"container p-0\">\n      <div class=\"col-12 p-0 pt-5\">\n        <h3 class=\"light_font py-5 my-5 text-white\"><span class=\"text-uppercase\">Dashboard</span> <br /> <span class=\"font-5 grey-text\">Welcome to Car Lease <span class=\"text-capitalize text-white\">{{user_session.fullName}}</span></span></h3>\n        <!-- <p class=\"text-white font-8\"></p> -->\n      </div>\n      <ul class=\"col-12 d-flex p-0 m-0 f-wrap\">\n        <li class=\"d-inline-block\"><a class=\"pb-2 mr-4 primary_link text-muted d-inline-block\" routerLink=\"/dashboard\" routerLinkActive=\"active\">Dashboard</a></li>\n        <li class=\"d-inline-block\"><a class=\"pb-2 mr-4 primary_link text-muted d-inline-block\" routerLink=\"/profile\" routerLinkActive=\"active\">Profile</a></li>\n      </ul>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/userheader/userheader.component.ts":
/*!****************************************************!*\
  !*** ./src/app/userheader/userheader.component.ts ***!
  \****************************************************/
/*! exports provided: UserheaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserheaderComponent", function() { return UserheaderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _common_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../common.service */ "./src/app/common.service.ts");



var UserheaderComponent = /** @class */ (function () {
    function UserheaderComponent(commonservice) {
        this.commonservice = commonservice;
    }
    UserheaderComponent.prototype.ngOnInit = function () {
        this.user_session = this.commonservice.user;
    };
    UserheaderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-userheader',
            template: __webpack_require__(/*! ./userheader.component.html */ "./src/app/userheader/userheader.component.html"),
            styles: [__webpack_require__(/*! ./userheader.component.css */ "./src/app/userheader/userheader.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_common_service__WEBPACK_IMPORTED_MODULE_2__["CommonService"]])
    ], UserheaderComponent);
    return UserheaderComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! hammerjs */ "../node_modules/hammerjs/hammer.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "../node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");





if (_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_3__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/abdul/projects/carlease/angular_app/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map