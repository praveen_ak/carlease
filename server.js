/* For env file function */
const dotenv = require('dotenv');
dotenv.config();

if (!process.env.HTTP_HOST) {
  console.log('Please configure env file.')
  console.log('Copy .env.default to .env and configure the values.')
  return
}

const http = require('http');
/**
 * remove the command line for https purpose
 */
// const https = require('https');
const app = require('./node_app/app');

const httpHost = process.env.HTTP_HOST || 'localhost';
const httpPort = process.env.HTTP_PORT || 8000;

/**
 * remove the command line for https purpose
 */
// const fs = require('fs');
// const options = {
//   key: fs.readFileSync(process.env.HTTPS_KEY),
//   cert: fs.readFileSync(process.env.HTTPS_CERT)
// };
// const server = https.createServer(options,app);

const server = http.createServer(app);

server.listen(httpPort, httpHost);
console.log('Server listening on '+ httpHost + ':' + httpPort)